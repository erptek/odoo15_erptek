# -*- coding: utf-8 -*-

from odoo.exceptions import ValidationError
from odoo import models, fields

class MRPCUTPRODUCTIONCREATEWIZARD(models.TransientModel):
	_name = 'mrp.cut.production.create.wizard'

	ratio = fields.Char(string="Tỷ lệ")
	layer = fields.Char(string="Số lớp")
	start_at = fields.Integer(string="Bắt đầu từ bàn", default=0)


	def create_table(self):
		active_id = self.env.context.get('active_id')
		active_record = self.env['namco.mrp.cut.production'].browse(active_id)
		products = active_record.line_ids.mapped('product_id')
		ratio = self.ratio
		ratio_list = []
		layer = self.layer
		if "," in self.ratio:
			ratio.replace(" ","")
			ratio_list = [(a.split('.')[0],a.split('.')[1]) for a in ratio.split(',')]
		else:
			ratio_list = [(a.split('.')[0],a.split('.')[1]) for a in ratio.split(' ')]
		if "," in self.layer:
			layer.replace(" ", "")
			layer_list = layer.split(",")
		else:
			layer_list = layer.split(" ")
		sequence = 1
		if not self.start_at:
			table_line = self.env['namco.mrp.cut.table.production'].search([('parent_id','=',active_id)],order="sequence desc",limit=1)
			if table_line:
				sequence = table_line.sequence + 1
		else:
			sequence = self.start_at
		for layer_qty in layer_list:
			if not layer_qty.isnumeric():
				raise ValidationError(_("Số lớp không đúng định dạng."))
			qty = int(layer_qty)
			line_list = []
			for ratio in ratio_list:
				if not ratio[1].isnumeric():
					raise ValidationError(_("Tỉ lệ không đúng định dạng."))
				product_ids = products.filtered(lambda p: p.product_size == ratio[0])
				if product_ids:
					product_id = product_ids[0]
					line_list.append((0,0,{
						'product_id': product_id.id,
						'ratio': int(ratio[1]),
						'cut_quantity': qty*int(ratio[1]),
						'real_cut_quantity': qty*int(ratio[1]),
						}))
				else:
					raise ValidationError(_("Không tìm thấy Size %s trong lệnh cắt.") % ratio[0])
			if line_list:
				self.env['namco.mrp.cut.table.production'].create({
					'name': str(sequence),
					'sequence': sequence,
					'ratio': self.ratio.replace('.',':').replace(',',', ').replace(' ','  '),
					'layer_qty': qty,
					'parent_id': active_id,
					'table_line_ids': line_list,
					})
				sequence += 1
		return True