# -*- coding: utf-8 -*-
from datetime import date

from dateutil.relativedelta import *
from odoo.exceptions import ValidationError
from odoo import models, fields, _
from odoo.osv import expression
import base64
import xlrd

class MRPCUTPRODUCTIONIMPORTWIZARD(models.TransientModel):
	_name = 'mrp.cut.production.import.wizard'

	file = fields.Binary(string="Planning Document", required=True)
	filename = fields.Char('File Name')


	def action_confirm(self):
		def get_bg_color(book, sheet, row, col):
			xfx = sheet.cell_xf_index(row, col)
			xf = book.xf_list[xfx]
			bgx = xf.background.pattern_colour_index
			pattern_colour = book.colour_map[bgx]
			return pattern_colour
		def format_number(s):
			try:
				result = int(s)
				return str(result)
			except ValueError:
				return str(s)
		def check_number(s):
			try:
				result = int(s)
				return True
			except ValueError:
				return False
		wb = xlrd.open_workbook(file_contents=base64.decodestring(self.file), formatting_info=True)
		data = []
		for sheet in wb.sheets():
			attribute_size_id = self.env['product.attribute'].search([('name','=','Size')], limit=1).id
			attribute_color_id = self.env['product.attribute'].search([('name','=','Color')], limit=1).id
			p_template = sheet.cell(0,0).value
			p_color = ''
			oc_list = []
			oc_string = sheet.cell(0,1).value.replace(" ","")
			size_list = []
			oc_split = oc_string.split("+")
			oc_origin = oc_split[0][:-1*len(oc_split[0].split(".")[-1])]
			n_cols = 0
			fabric_dict = {}
			fabric_row_list = []
			fabric_col = 0
			for r in range(4,sheet.nrows):
				if str(sheet.cell(r,2).value).upper() == "TỔNG OC":
					total_row = r
					break
			for oc in oc_split:
				if oc == oc_split[0]:
					if self.env['namco.order.code'].search([('name','=',oc)], limit=1):
						oc_list.append(self.env['namco.order.code'].search([('name','=',oc)], limit=1).id)
					else:
						oc_list.append(self.env['namco.order.code'].create({'name': oc}).id)
				else:
					if self.env['namco.order.code'].search([('name','=',oc_origin + oc)], limit=1):
						oc_list.append(self.env['namco.order.code'].search([('name','=',oc_origin + oc)], limit=1).id)
					else:
						oc_list.append(self.env['namco.order.code'].create({'name': oc_origin + oc}).id)
			for i in range(5,sheet.ncols):
				if sheet.cell(3,i).value == 'TOTAL':
					n_cols = i
			for i in range(2,n_cols):
				if sheet.cell(0,i).value and sheet.cell(0,i).value != 'TH':
					p_color = sheet.cell(0,i).value
			for i in range(5,sheet.ncols):
				if sheet.cell(3,i).value == 'TOTAL':
					break
				if not sheet.cell(3,i).value:
					continue
				if sheet.cell(total_row,i).value:
					if self.env['product.attribute.value'].search([('attribute_id','=',attribute_size_id),('name','=',format_number(sheet.cell(3,i).value))],limit=1):
						size_list.append(self.env['product.attribute.value'].search([('attribute_id','=',attribute_size_id),('name','=',format_number(sheet.cell(3,i).value))],limit=1).id)
					else:
						size_list.append(self.env['product.attribute.value'].create({
							'name': format_number(sheet.cell(3,i).value),
							'attribute_id': attribute_size_id
							}).id)
			if self.env['product.attribute.value'].search([('attribute_id','=',attribute_color_id),('name','=',p_color)],limit=1):
				p_color_id = self.env['product.attribute.value'].search([('attribute_id','=',attribute_color_id),('name','=',p_color)],limit=1).id
			else:
				p_color_id = self.env['product.attribute.value'].create({
					'name': p_color,
					'attribute_id': attribute_color_id
					}).id
			if self.env['product.template'].search([('name','=',p_template)], limit=1):
				p_template_id = self.env['product.template'].search([('name','=',p_template)], limit=1)
				color_set = []
				size_set = []
				if p_template_id.attribute_line_ids.filtered(lambda l: l.attribute_id.id == attribute_color_id):
					attribute_line = p_template_id.attribute_line_ids.filtered(lambda l: l.attribute_id.id == attribute_color_id)[0]
					if p_color_id not in attribute_line.value_ids.ids:
						color_set = [1, attribute_line.id, {'value_ids': [[6, False, attribute_line.value_ids.ids + [p_color_id]]]}]
					
				else:
					color_set = [0,0,{'attribute_id': attribute_color_id, 'value_ids': [[6, False, [p_color_id]]]}]
				if p_template_id.attribute_line_ids.filtered(lambda l: l.attribute_id.id == attribute_size_id):
					attribute_line_size = p_template_id.attribute_line_ids.filtered(lambda l: l.attribute_id.id == attribute_size_id)[0]
					size_set_ids = attribute_line_size.value_ids.ids
					for size_id in size_list:
						if size_id not in size_set_ids:
							size_set_ids.append(size_id)
					size_set = [1, attribute_line_size.id, {'value_ids': [[6, False,size_set_ids]]}]
				else:
					size_set = [0,0,{'attribute_id': attribute_size_id, 'value_ids': [[6, False, size_list]]}]
				if color_set and size_set:
					p_template_id.update({
						'attribute_line_ids': [color_set,size_set]
						})
				elif color_set and not size_set:
					p_template_id.update({
						'attribute_line_ids': [color_set]
						})
				elif not color_set and size_set:
					p_template_id.update({
						'attribute_line_ids': [size_set]
						})
			else:
				p_template_id = self.env['product.template'].create({
					'name': p_template,
					'attribute_line_ids': [[0,0,{'attribute_id': attribute_color_id, 'value_ids': [[6, False, [p_color_id]]]}],[0,0, {'attribute_id': attribute_size_id, 'value_ids': [[6, False, size_list]]}]]
					})
			for i in range(0,n_cols):
				if sheet.cell(1,i).value:
					fabric_col = i
					if len(str(sheet.cell(1,i).value)) > 10:
						fabric_dict[1] = {}
						fabric_dict[1]['name'] = str(sheet.cell(1,i).value)
						fabric_dict[1]['total_row'] = total_row
						fabric_row_list.append(1)
						break
			if fabric_dict:
				for r in range(2,sheet.nrows):
					if 'VAI CHINH' in str(sheet.cell(r,fabric_col).value).upper() or 'VẢI CHÍNH' in str(sheet.cell(r,fabric_col).value).upper():
						fabric_dict[r] = {}
						fabric_dict[r]['name'] = str(sheet.cell(r,fabric_col).value)
						for row in range(r+1,sheet.nrows):
							if str(sheet.cell(row,2).value).upper() == 'TỔNG OC':
								fabric_dict[r]['total_row'] = row
						fabric_row_list.append(r)

			if self.env['namco.mrp.cut.production'].search([('order_code_ids','in',oc_list),('product_color','=',p_color_id)]):
				cut_production_id = self.env['namco.mrp.cut.production'].search([('order_code_ids','in',oc_list),('product_color','=',p_color_id)], limit=1)
				add_detail_list = []
				product_dict = {}
				for i in range(5,n_cols):
					if sheet.cell(3,i).value == 'TOTAL':
						break
					if not sheet.cell(3,i).value:
						continue
					if sheet.cell(total_row,i).value or any(sheet.cell(fabric_dict[fr]['total_row'],i).value for fr in fabric_row_list):
						detail_lines = cut_production_id.line_ids.filtered(lambda l: l.product_id.product_size == format_number(sheet.cell(3,i).value))
						if detail_lines:
							detail_line = detail_lines[0]
							product_dict[i] = detail_line.product_id.id
							if detail_line.quantity != (int(sheet.cell(total_row,i).value) + (sum(fr != 1 and int(sheet.cell(fabric_dict[fr]['total_row'],i).value or 0) for fr in fabric_row_list) or 0)):
								detail_line.update({
									'quantity': (int(sheet.cell(total_row,i).value) + (sum(fr != 1 and int(sheet.cell(fabric_dict[fr]['total_row'],i).value or 0) for fr in fabric_row_list) or 0))
									})
						else:
							product_id = self.env['product.product'].search([('product_tmpl_id','=',p_template_id.id),('product_size','=',format_number(sheet.cell(3,i).value)),('product_v_color','=',p_color)], limit=1)
							product_dict[i] = product_id.id
							add_detail_list.append((0,0,{
								'product_id': product_id.id,
								'quantity': (int(sheet.cell(total_row,i).value) + (sum(fr != 1 and int(sheet.cell(fabric_dict[fr]['total_row'],i).value or 0) for fr in fabric_row_list) or 0))
								}))
				if add_detail_list:
					cut_production_id.update({
						'line_ids': add_detail_list
						})
				for fr in fabric_row_list:
					if fr == 1:
						continue
					for r in range(fabric_dict[fr]['total_row']+1,sheet.nrows):
						if not sheet.cell(r,2).value:
							break
						if not sheet.cell(r,4).value:
							continue
						if not check_number(sheet.cell(r,2).value):
							continue
						table_lines = []
						ratio = ''
						table_ids = cut_production_id.table_ids.filtered(lambda t: t.name == format_number(sheet.cell(r,2).value) and t.giang_type == fabric_dict[fr]['name'][10:])
						if table_ids:
							table_id = table_ids[0]
							for i in range(5,n_cols):
								if sheet.cell(3,i).value == 'TOTAL':
									break
								if not sheet.cell(3,i).value:
									continue
								if sheet.cell(r,i).value:
									ratio += (format_number(sheet.cell(3,i).value)) + ":" + str(int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value)) + "  "
							if table_id.layer_qty != int(sheet.cell(r,4).value):
								table_id.update({
									'layer_qty': int(sheet.cell(r,4).value)
									})
							if table_id.ratio != ratio[:-2]:
								table_id.update({
									'ratio': ratio[:-2]
									})
								table_id.recompute_ratio()
							if get_bg_color(wb, sheet, r ,4) == (0, 128, 0) and table_id.state != 'confirm':
								table_id.confirm_cut_table()
						else:
							for i in range(5,n_cols):
								if sheet.cell(3,i).value == 'TOTAL':
									break
								if not sheet.cell(3,i).value:
									continue
								if sheet.cell(r,i).value:
									table_lines.append((0,0, {
										'product_id': product_dict[i],
										'ratio': int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value),
										'real_cut_quantity': int(sheet.cell(r,i).value),
										'cut_quantity': int(sheet.cell(r,i).value),
										}))
									ratio += (format_number(sheet.cell(3,i).value)) + ":" + str(int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value)) + "  "
							
							if table_lines:
								table_id = self.env['namco.mrp.cut.table.production'].create({
									'name': format_number(sheet.cell(r,2).value),
									'layer_qty': int(sheet.cell(r,4).value),
									'ratio': ratio[:-2],
									'table_line_ids': table_lines,
									'parent_id': cut_production_id.id,
									'giang_type': fabric_dict[fr]['name'][10:],
									})
								if get_bg_color(wb, sheet, r ,4) == (0, 128, 0):
									table_id.confirm_cut_table()
									if cut_production_id.state != 'progress':
										cut_production_id.update({
											'state': 'progress'
											})
				for r in range(total_row+1,sheet.nrows):
					if not sheet.cell(r,2).value:
						break
					if not sheet.cell(r,4).value:
						continue
					if not check_number(sheet.cell(r,2).value):
						continue
					table_lines = []
					ratio = ''
					table_ids = cut_production_id.table_ids.filtered(lambda t: t.name == format_number(sheet.cell(r,2).value))
					if table_ids:
						table_id = table_ids[0]
						for i in range(5,n_cols):
							if sheet.cell(3,i).value == 'TOTAL':
								break
							if not sheet.cell(3,i).value:
								continue
							if sheet.cell(r,i).value:
								ratio += (format_number(sheet.cell(3,i).value)) + ":" + str(int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value)) + "  "
						if table_id.layer_qty != int(sheet.cell(r,4).value):
							table_id.update({
								'layer_qty': int(sheet.cell(r,4).value)
								})
						if table_id.ratio != ratio[:-2]:
							table_id.update({
								'ratio': ratio[:-2]
								})
							table_id.recompute_ratio()
						if get_bg_color(wb, sheet, r ,4) == (0, 128, 0) and table_id.state != 'confirm':
							table_id.confirm_cut_table()
					else:
						for i in range(5,n_cols):
							if sheet.cell(3,i).value == 'TOTAL':
								break
							if not sheet.cell(3,i).value:
								continue
							if sheet.cell(r,i).value:
								table_lines.append((0,0, {
									'product_id': product_dict[i],
									'ratio': int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value),
									'real_cut_quantity': int(sheet.cell(r,i).value),
									'cut_quantity': int(sheet.cell(r,i).value),
									}))
								ratio += (format_number(sheet.cell(3,i).value)) + ":" + str(int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value)) + "  "
						
						if table_lines:
							table_id = self.env['namco.mrp.cut.table.production'].create({
								'name': format_number(sheet.cell(r,2).value),
								'layer_qty': int(sheet.cell(r,4).value),
								'ratio': ratio[:-2],
								'table_line_ids': table_lines,
								'parent_id': cut_production_id.id,
								})
							if get_bg_color(wb, sheet, r ,4) == (0, 128, 0):
								table_id.confirm_cut_table()
								if cut_production_id.state != 'progress':
									cut_production_id.update({
										'state': 'progress'
										})



				# raise ValidationError(_("Lệnh cắt với OC %s, màu %s đã được tạo, hiện chưa hỗ trợ update" % (oc_string, p_color)))
			else:
				detail_line_list = []
				table_list = []
				product_dict = {}
				for i in range(5,n_cols):
					if sheet.cell(3,i).value == 'TOTAL':
						break
					if not sheet.cell(3,i).value:
						continue
					if sheet.cell(total_row,i).value or any(sheet.cell(fabric_dict[fr]['total_row'],i).value for fr in fabric_row_list):
						product_id = self.env['product.product'].search([('product_tmpl_id','=',p_template_id.id),('product_size','=',format_number(sheet.cell(3,i).value)),('product_v_color','=',p_color)], limit=1)
						product_dict[i] = product_id.id
						detail_line_list.append((0,0,{
							'product_id': product_id.id,
							'quantity': int(sheet.cell(total_row,i).value) + (sum(fr != 1 and int(sheet.cell(fabric_dict[fr]['total_row'],i).value or 0) for fr in fabric_row_list) or 0)
							}))
				if detail_line_list:
					cut_production_id = self.env['namco.mrp.cut.production'].create({
						'order_code_ids': [[6,0,oc_list]],
						'product_template_id': p_template_id.id,
						'product_color': p_color_id,
						'line_ids': detail_line_list,
						})
				for r in range(total_row+1,sheet.nrows):
					if not sheet.cell(r,2).value:
						break
					if not sheet.cell(r,4).value:
						continue
					if not check_number(sheet.cell(r,2).value):
						continue
					table_lines = []
					ratio = ''
					for i in range(5,n_cols):
						if sheet.cell(3,i).value == 'TOTAL':
							break
						if not sheet.cell(3,i).value:
							continue
						if sheet.cell(r,i).value:
							table_lines.append((0,0, {
								'product_id': product_dict[i],
								'ratio': int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value),
								'real_cut_quantity': int(sheet.cell(r,i).value),
								'cut_quantity': int(sheet.cell(r,i).value),
								}))
							ratio += (format_number(sheet.cell(3,i).value)) + ":" + str(int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value)) + "  "
					
					if table_lines:
						table_id = self.env['namco.mrp.cut.table.production'].create({
							'name': format_number(sheet.cell(r,2).value),
							'layer_qty': int(sheet.cell(r,4).value),
							'ratio': ratio[:-2],
							'table_line_ids': table_lines,
							'parent_id': cut_production_id.id,
							'giang_type': fabric_dict.get(1, False) and fabric_dict[1]['name'][10:] or '',
							})
						if get_bg_color(wb, sheet, r ,4) == (0, 128, 0):
							table_id.confirm_cut_table()
							if cut_production_id.state != 'progress':
								cut_production_id.update({
									'state': 'progress'
									})
				for fr in fabric_row_list:
					if fr == 1:
						continue
					for r in range(fabric_dict[fr]['total_row']+1,sheet.nrows):
						if not sheet.cell(r,2).value:
							break
						if not sheet.cell(r,4).value:
							continue
						if not check_number(sheet.cell(r,2).value):
							continue
						table_lines = []
						ratio = ''
						for i in range(5,n_cols):
							if sheet.cell(3,i).value == 'TOTAL':
								break
							if not sheet.cell(3,i).value:
								continue
							if sheet.cell(r,i).value:
								table_lines.append((0,0, {
									'product_id': product_dict[i],
									'ratio': int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value),
									'real_cut_quantity': int(sheet.cell(r,i).value),
									'cut_quantity': int(sheet.cell(r,i).value),
									}))
								ratio += (format_number(sheet.cell(3,i).value)) + ":" + str(int(sheet.cell(r,i).value)//int(sheet.cell(r,4).value)) + "  "
						
						if table_lines:
							table_id = self.env['namco.mrp.cut.table.production'].create({
								'name': format_number(sheet.cell(r,2).value),
								'layer_qty': int(sheet.cell(r,4).value),
								'ratio': ratio[:-2],
								'table_line_ids': table_lines,
								'parent_id': cut_production_id.id,
								'giang_type': fabric_dict[fr]['name'][10:] or '',
								})
							if get_bg_color(wb, sheet, r ,4) == (0, 128, 0):
								table_id.confirm_cut_table()
								if cut_production_id.state != 'progress':
									cut_production_id.update({
										'state': 'progress'
										})

			break

		return True