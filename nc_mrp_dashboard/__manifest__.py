# -*- coding: utf-8 -*-
{
    'name': "Nam&Co MRP DashBoard",
    'summary': """.
    """,
    'author': "Hoan",
    'category': 'MRP',
    'version': '15.0.1.1',
    'license': 'LGPL-3',
    'depends': ['mrp', 'hr', 'mrp_workorder', 'contacts','sale_stock', 'sale_product_matrix'],
    'data': [
        'security/ir.model.access.csv',
        'security/mrp_security.xml',
        'wizard/mrp_cut_production_import_view.xml',
        'wizard/mrp_cut_table_quick_create_views.xml',
        'views/mrp_production_views.xml',
        'views/hr_employee_views.xml',
        'views/mrp_cut_production_views.xml',
        'views/sale_order_views.xml',

    ],
    'demo': [],
    'test': [],
    'installable': True,
}
