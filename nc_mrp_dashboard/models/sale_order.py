# -*- coding: utf-8 -*-

import itertools
import logging
from collections import defaultdict

from odoo import api, fields, models, SUPERUSER_ID, _
from collections import defaultdict
from odoo.tools import float_compare, OrderedSet
from odoo.addons.stock.models.stock_rule import ProcurementException


class NAMCOSTOCKRULE(models.Model):
	_inherit = "stock.rule"

	def _prepare_mo_vals(self, product_id, product_qty, product_uom, location_id, name, origin, company_id, values, bom):
		res = super(NAMCOSTOCKRULE, self)._prepare_mo_vals(product_id, product_qty, product_uom, location_id, name, origin, company_id, values, bom)
		res['department_line_id'] = self._context.get('department_line_id',False)
		res['order_code'] = self._context.get('order_code', False)
		res['product_template_id'] = self._context.get('product_template_id', '')
		return res


class NAMCOSALEORDER(models.Model):
	_inherit = "sale.order"


	def _domain_attribute_color_value(self):
		attribute_id = self.env['product.attribute'].search_read([('name','=ilike','Color')], limit=1)
		domain = []
		if attribute_id:
			domain = [('attribute_id','=',attribute_id[0].get('id',False))]
		return domain

	order_code = fields.Many2one('namco.order.code', string="OC No.", required=True)
	department_line_id = fields.Many2one('hr.department', string="Chuyền", required=True)
	product_template_id = fields.Many2one('product.template', string="Style", required=True)
	product_v_color = fields.Many2one('product.attribute.value',domain=_domain_attribute_color_value,required=True, string="Màu")

	def action_confirm(self):
		context = self._context.copy()
		context['department_line_id'] = self.department_line_id and self.department_line_id.id or False
		context['order_code'] = self.order_code.id
		context['product_template_id'] = self.product_template_id.id
		res = super(NAMCOSALEORDER, self.with_context(context)).action_confirm()
		return res





