# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from datetime import date, datetime, timedelta, time
from odoo.tools.misc import formatLang
from odoo.exceptions import ValidationError
try:
	from odoo.tools.misc import xlsxwriter
except ImportError:
	import xlsxwriter
import xlwt
import os
import io
import base64
import requests


class NAMCOMRPPRODUCTION(models.Model):
	_inherit = "mrp.production"

	# def _default_production_line(self):
	# 	line_id = self.env['hr.department'].search([('parent_id.name','=ilike','Sản Xuất'),('manager_id','=',self.env.user.employee_id.id)],limit=1)
	# 	return line_id and line_id.id or False

	def _production_line_domain(self):
		domain = []
		parent_id = self.env['hr.department'].search_read([('name','=ilike','Sản Xuất')],['id'],limit=1)
		if parent_id:
			domain = [('parent_id','=',parent_id[0]['id'])]
		return domain

	cut_table_ids = fields.Many2many('namco.mrp.cut.table.production', string="Bàn cắt")
	order_code = fields.Many2one('namco.order.code', string="OC No.", required=True)
	product_size = fields.Char(string="Size", related='product_id.product_size', store=True, readonly=True)
	sequence_size = fields.Integer(string="Sequence size", related='product_id.sequence_size', store=True, readonly=True)
	product_v_color = fields.Char(string="Màu", related='product_id.product_v_color', store=True, readonly=True)
	product_template_id = fields.Many2one('product.template', string="Product template")
	cut_quantity = fields.Integer(string="Vải chính đã cắt", default=0)
	cut_update_time = fields.Datetime(string="Cut updated")
	cut_lining_quantity = fields.Integer(string="Lót đã cắt", default=0)
	cut_lining_update_time = fields.Datetime(string="Cut lining updated")
	delivered_semi_sew_qty = fields.Integer("BTP may đã giao", default=0)
	delivered_semi_sew_time = fields.Datetime(string="Semi sew delivery update time")
	delivered_semi_special_qty = fields.Integer(string="BTP CD đã giao", default=0)
	delivered_semi_specical_time = fields.Datetime(string="Semi special update time")
	semi_special_qty = fields.Integer(string="BTP CD đã nhận", default=0)
	semi_special_update_time = fields.Datetime(string="Semi special updated time")
	semi_quantity = fields.Integer(string="BTP may đã nhận", default=0)
	semi_update_time = fields.Datetime(string="Semi updated")
	detail_quantity = fields.Integer(string="Chi tiết", default=0)
	special_quantity = fields.Integer(string="Cạp đã nhận", default=0)
	special_update_time = fields.Datetime(string="Special update time")
	combine_qty = fields.Integer(string="Lên xích", default=0)
	combine_update_time = fields.Datetime(string="Combine updated time")
	consumed_qty = fields.Integer(string="Giựt xích", default=0)
	consumed_update_time = fields.Datetime(string="Consumed updated time")
	worker_count = fields.Integer(string="Số lao động", default=0)
	qc_pass = fields.Integer(string="QC Pass", default=0)
	qc_update_time = fields.Datetime(string="QC updated")
	qc_failed = fields.Integer(string="QC Failed", default=0)
	department_line_id = fields.Many2one('hr.department', string="Chuyền", required=True, domain=_production_line_domain)
	department_line_sequence = fields.Integer(string="Line Sequence", related="department_line_id.department_line_sequence", store=True)
	date_target = fields.Integer(string="Target ngày", default=0)
	start_production_date = fields.Datetime(string="Latest start")
	cut_record_dailys = fields.One2many('mrp.production.cut.record.daily', 'production_id', string="Cut daily")
	semi_record_dailys = fields.One2many('mrp.production.semi.record.daily', 'production_id', string="Semi daily")
	detail_record_dailys = fields.One2many('mrp.production.detail.record.daily', 'production_id', string="detail daily")
	specific_record_dailys = fields.One2many('mrp.production.specific.record.daily', 'production_id', string="Specific daily")
	semi_specific_record_dailys = fields.One2many('mrp.production.semi.specific.record.daily', 'production_id', string="Semi specific daily")
	combine_record_dailys = fields.One2many('mrp.production.combine.record.daily', 'production_id', string="Combine daily")
	consume_record_dailys = fields.One2many('mrp.production.consume.record.daily', 'production_id', string="Consume daily")
	qc_pass_record_dailys = fields.One2many('mrp.production.qc.pass.record.daily', 'production_id', string="QC pass daily")
	table_line_ids = fields.One2many('mrp.production.table.line', 'production_id', string="Table lines")
	semi_specific_table_line_ids = fields.One2many('mrp.production.semi.specific.table.line', 'production_id', string="Semi Specific Table lines")

	def unlink(self):
		line_list = []
		semi_line_list = []
		for record in self:
			line_list += record.table_line_ids.ids
			semi_line_list += record.semi_specific_table_line_ids.ids
		table_lines = self.env['mrp.production.table.line'].browse(line_list)
		semi_table_lines = self.env['mrp.production.semi.specific.table.line'].browse(semi_line_list)
		res = super(NAMCOMRPPRODUCTION, self).unlink()
		for line in table_lines:
			quantity = line.quantity
			mrp = self.env['mrp.production'].search([('order_code','in',line.table_line_id.table_id.parent_id.order_code_ids.ids),('product_id','=',line.table_line_id.product_id.id),('state','in',('confirmed','progress'))],order="order_code asc", limit=1)
			
			if mrp:
				mrp.with_context(table_line_id=line.table_line_id.id).update({
					'cut_quantity': quantity
					})
			else:
				line.table_line_id.update({
					'cut_remain_qty': line.table_line_id.cut_remain_qty + quantity
					})
		for line in semi_table_lines:
			quantity = line.quantity
			mrp = self.env['mrp.production'].search([('order_code','in',line.table_line_id.table_id.parent_id.order_code_ids.ids),('product_id','=',line.table_line_id.product_id.id),('state','in',('confirmed','progress'))],order="order_code asc", limit=1)
			
			if mrp:
				mrp.with_context(table_line_id=line.table_line_id.id).update({
					'semi_special_qty': quantity
					})
			else:
				line.table_line_id.update({
					'semi_specific_remain_quantity': line.table_line_id.semi_specific_remain_quantity + quantity
					})
		table_lines.unlink()
		semi_table_lines.unlink()
		return res

	@api.model
	def create(self, values):
		res = super(NAMCOMRPPRODUCTION, self).create(values)
		for record in res:
			remain_qty = record.product_qty
			semi_remain_qty = record.product_qty
			total_add_qty = 0
			table_line_list = []
			table_semi_line_list = []
			table_list = []
			for cut_production in self.env['namco.mrp.cut.production'].search([('product_v_color','=',record.product_v_color),('order_code_ids','in',[record.order_code.id])]):
				table_list += [(4,t.id) for t in cut_production.table_ids.filtered(lambda l: l.state == 'confirm')]
				for line in cut_production.table_line_ids.filtered(lambda l: l.product_id.id == record.product_id.id and l.semi_specific_remain_quantity > 0):
					if semi_remain_qty <= 0:
						break
					if line.semi_specific_remain_quantity >= semi_remain_qty:
						total_add_semi_qty += semi_remain_qty
						line.update({
							'semi_specific_remain_quantity': line.semi_specific_remain_quantity - semi_remain_qty
							})
						table_semi_line_list.append((0,0, {
							'table_line_id': line.id,
							'quantity': semi_remain_qty,
							}))
						semi_remain_qty = 0
					else:
						total_add_semi_qty += line.semi_specific_remain_quantity
						table_semi_line_list.append((0,0, {
							'table_line_id': line.id,
							'quantity': line.semi_specific_remain_quantity,
							}))
						semi_remain_qty -= line.semi_specific_remain_quantity
						line.update({
							'semi_specific_remain_quantity': 0
							})

				for line in cut_production.table_line_ids.filtered(lambda l: l.product_id.id == record.product_id.id and l.cut_remain_qty > 0):
					if remain_qty <= 0:
						break
					if line.cut_remain_qty >= remain_qty:
						total_add_qty += remain_qty
						line.update({
							'cut_remain_qty': line.cut_remain_qty - remain_qty
							})
						table_line_list.append((0,0, {
							'table_line_id': line.id,
							'quantity': remain_qty,
							}))
						remain_qty = 0
					else:
						total_add_qty += line.cut_remain_qty
						table_line_list.append((0,0, {
							'table_line_id': line.id,
							'quantity': line.cut_remain_qty,
							}))
						remain_qty -= line.cut_remain_qty
						line.update({
							'cut_remain_qty': 0
							})
						
			record.with_context(accumulated_qty=False).update({
				'cut_quantity': total_add_qty,
				'table_line_ids': table_line_list,
				'cut_table_ids': table_list,
				'semi_special_qty': total_add_semi_qty,
				'semi_specific_table_line_ids': table_line_list
				})

		return res

	def refresh_view(self):
		return True


	def open_record(self):
		rec_id = self.id
		form_id = self.env.ref('mrp.mrp_production_form_view')
		return {
			'type': 'ir.actions.act_window',
			'name': 'title',
			'res_model': 'mrp.production',
			'res_id': rec_id,
			'view_mode': 'form',
			'view_id': form_id.id,
			'context': {},          
			'flags': {'initial_mode': 'edit'},
			'target': 'new',
		}

	def open_cut_record(self):
		rec_id = self.id
		form_id = self.env.ref('nc_mrp_dashboard.mrp_production_cut_record_form_view')
		return {
			'type': 'ir.actions.act_window',
			'name': 'Ghi nhận cắt theo ngày',
			'res_model': 'mrp.production',
			'res_id': rec_id,
			'view_mode': 'form',
			'view_id': form_id.id,
			'context': {},          
			'flags': {'initial_mode': 'edit'},
			'target': 'new',
		}

	def open_semi_record(self):
		rec_id = self.id
		form_id = self.env.ref('nc_mrp_dashboard.mrp_production_semi_record_form_view')
		return {
			'type': 'ir.actions.act_window',
			'name': 'Ghi nhận BTP may nhận theo bàn',
			'res_model': 'mrp.production',
			'res_id': rec_id,
			'view_mode': 'form',
			'view_id': form_id.id,
			'context': {'department_line_id': self.department_line_id.id},          
			'flags': {'initial_mode': 'edit'},
			'target': 'new',
		}

	def open_detail_record(self):
		rec_id = self.id
		form_id = self.env.ref('nc_mrp_dashboard.mrp_production_detail_record_form_view')
		return {
			'type': 'ir.actions.act_window',
			'name': 'Ghi nhận chi tiết may nhận theo bàn',
			'res_model': 'mrp.production',
			'res_id': rec_id,
			'view_mode': 'form',
			'view_id': form_id.id,
			'context': {'department_line_id': self.department_line_id.id},          
			'flags': {'initial_mode': 'edit'},
			'target': 'new',
		}

	def open_semi_specific_record(self):
		rec_id = self.id
		form_id = self.env.ref('nc_mrp_dashboard.mrp_production_semi_specific_record_form_view')
		return {
			'type': 'ir.actions.act_window',
			'name': 'Ghi nhận BTP chuyên dùng nhận theo bàn',
			'res_model': 'mrp.production',
			'res_id': rec_id,
			'view_mode': 'form',
			'view_id': form_id.id,
			'context': {},          
			'flags': {'initial_mode': 'edit'},
			'target': 'new',
		}

	def open_specific_record(self):
		rec_id = self.id
		form_id = self.env.ref('nc_mrp_dashboard.mrp_production_specific_record_form_view')
		return {
			'type': 'ir.actions.act_window',
			'name': 'Ghi nhận cạp nhận theo bàn',
			'res_model': 'mrp.production',
			'res_id': rec_id,
			'view_mode': 'form',
			'view_id': form_id.id,
			'context': {'department_line_id': self.department_line_id.id},          
			'flags': {'initial_mode': 'edit'},
			'target': 'new',
		}

	def open_combine_record(self):
		rec_id = self.id
		form_id = self.env.ref('nc_mrp_dashboard.mrp_production_combine_record_form_view')
		return {
			'type': 'ir.actions.act_window',
			'name': 'Ghi nhận lên xích theo bàn',
			'res_model': 'mrp.production',
			'res_id': rec_id,
			'view_mode': 'form',
			'view_id': form_id.id,
			'context': {'department_line_id': self.department_line_id.id},        
			'flags': {'initial_mode': 'edit'},
			'target': 'new',
		}

	def open_consume_record(self):
		rec_id = self.id
		form_id = self.env.ref('nc_mrp_dashboard.mrp_production_consume_record_form_view')
		return {
			'type': 'ir.actions.act_window',
			'name': 'Ghi nhận giật xích theo bàn',
			'res_model': 'mrp.production',
			'res_id': rec_id,
			'view_mode': 'form',
			'view_id': form_id.id,
			'context': {'department_line_id': self.department_line_id.id},           
			'flags': {'initial_mode': 'edit'},
			'target': 'new',
		}

	def open_qc_pass_record(self):
		rec_id = self.id
		form_id = self.env.ref('nc_mrp_dashboard.mrp_production_qc_pass_record_form_view')
		return {
			'type': 'ir.actions.act_window',
			'name': 'Ghi nhận QC pass theo bàn',
			'res_model': 'mrp.production',
			'res_id': rec_id,
			'view_mode': 'form',
			'view_id': form_id.id,
			'context': {},          
			'flags': {'initial_mode': 'edit'},
			'target': 'new',
		}

	@api.model 
	def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
		res = super(NAMCOMRPPRODUCTION, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
		if 'cut_quantity' in fields:
			for line in res:
				if '__domain' in line:
					lines = self.search(line['__domain'])
					cut_quantity = 0.0
					for record in lines:
						cut_quantity += record.cut_quantity
					line['cut_quantity'] = cut_quantity
		if 'cut_lining_quantity' in fields:
			for line in res:
				if '__domain' in line:
					lines = self.search(line['__domain'])
					cut_lining_quantity = 0.0
					for record in lines:
						cut_lining_quantity += record.cut_lining_quantity
					line['cut_lining_quantity'] = cut_lining_quantity
		if 'semi_special_qty' in fields:
			for line in res:
				if '__domain' in line:
					lines = self.search(line['__domain'])
					semi_special_qty = 0.0
					for record in lines:
						semi_special_qty += record.semi_special_qty
					line['semi_special_qty'] = semi_special_qty
		
		if 'semi_quantity' in fields:
			for line in res:
				if '__domain' in line:
					lines = self.search(line['__domain'])
					semi_quantity = 0.0
					for record in lines:
						semi_quantity += record.semi_quantity
					line['semi_quantity'] = semi_quantity

		if 'detail_quantity' in fields:
			for line in res:
				if '__domain' in line:
					lines = self.search(line['__domain'])
					detail_quantity = 0.0
					for record in lines:
						detail_quantity += record.detail_quantity
					line['detail_quantity'] = detail_quantity
		
		if 'special_quantity' in fields:
			for line in res:
				if '__domain' in line:
					lines = self.search(line['__domain'])
					special_quantity = 0.0
					for record in lines:
						special_quantity += record.special_quantity
					line['special_quantity'] = special_quantity
		
		if 'combine_qty' in fields:
			for line in res:
				if '__domain' in line:
					lines = self.search(line['__domain'])
					combine_qty = 0.0
					for record in lines:
						combine_qty += record.combine_qty
					line['combine_qty'] = combine_qty
		
		if 'consumed_qty' in fields:
			for line in res:
				if '__domain' in line:
					lines = self.search(line['__domain'])
					consumed_qty = 0.0
					for record in lines:
						consumed_qty += record.consumed_qty
					line['consumed_qty'] = consumed_qty
		if 'qc_pass' in fields:
			for line in res:
				if '__domain' in line:
					lines = self.search(line['__domain'])
					qc_pass = 0.0
					for record in lines:
						qc_pass += record.qc_pass
					line['qc_pass'] = qc_pass

		return res

	def action_start_production(self):
		for record in self.filtered(lambda l: l.state == "confirmed"):
			# if not record.cut_quantity:
			# 	raise ValidationError(_("Không thể bắt đầu lệnh sản xuất chưa được cắt."))
			# self.search([('state','in',('progress','confirmed')),('department_line_id','=',record.department_line_id.id)]).write({
			# 	'state': 'draft',
			# 	})
			record.update({
				'state': 'progress',
				'start_production_date': datetime.now(),
				'qc_update_time': datetime.now(),
				})

	def action_confirm_production(self):
		for record in self.filtered(lambda l: l.state == 'draft'):
			# if not record.cut_quantity:
			# 	raise ValidationError(_("Người dùng cần nhập số lượng đã cắt trước khi xác nhận."))
			# self.search([('state','in',('progress','confirmed')),('department_line_id','=',record.department_line_id.id)]).write({
			# 	'state': 'draft',
			# 	})
			record.update({
				'state': 'confirmed',
				})

	# def check_data(self):
		# first_production = self.search_read([('id','=',self.id)], \
		# 	['order_code','department_line_id','product_id','date_target','product_qty','qc_pass', \
		# 	'qc_failed', 'cut_quantity', 'semi_quantity', 'bottle_neck_qty', 'worker_count', 'cut_update_time', \
		# 	'semi_update_time', 'bottle_update_time', 'qc_update_time', 'start_production_date'],limit=1)
		# result = []
		# if first_production and first_production[0]:
		# 	# raise Warning(first_production[0].get('bottle_update_time'))

		# 	druation_time = datetime.now() - first_production[0].get('start_production_date',datetime.now())
		# 	started_time = first_production[0].get('bottle_update_time',datetime.now()) - first_production[0].get('start_production_date',datetime.now())
		# 	# raise Warning(started_time,started_time.seconds//60, started_time.seconds//3600)
		# 	# raise Warning(first_production[0])
		# 	result.append({
		# 		"line_name": first_production[0].get('department_line_id')[1],
		# 		"product": first_production[0].get('product_id')[1],
		# 		"date_target": first_production[0].get('date_target', 0),
		# 		"worker_count": first_production[0].get('worker_count',0),
		# 		"qc_pass": first_production[0].get('qc_pass',0),
		# 		"qc_update_time": str(first_production[0].get('qc_update_time') and first_production[0].get('qc_update_time').hour + 7 or "00") + ":" + str(first_production[0].get('qc_update_time') and first_production[0].get('qc_update_time').minute or "00"),
		# 		"cut_quantity": first_production[0].get('cut_quantity',0),
		# 		"cut_update_time": str(first_production[0].get('cut_update_time') and first_production[0].get('cut_update_time').hour + 7 or "00") + ":" + str(first_production[0].get('cut_update_time') and first_production[0].get('cut_update_time').minute or "00"),
		# 		"semi_quantity": first_production[0].get('semi_quantity',0),
		# 		"semi_update_time": str(first_production[0].get('semi_update_time') and first_production[0].get('semi_update_time').hour + 7 or "00") + ":" + str(first_production[0].get('semi_update_time') and first_production[0].get('semi_update_time').minute or "00"),
		# 		"bottle_neck_qty": first_production[0].get('bottle_neck_qty',0),
		# 		"bottle_update_time": str(first_production[0].get('bottle_update_time') and first_production[0].get('bottle_update_time').hour + 7 or "00") + ":" + str(first_production[0].get('bottle_update_time') and first_production[0].get('bottle_update_time').minute or "00"),
		# 		"druation_time": str(druation_time.seconds//3600) + ":" + str((druation_time.seconds%3600)//60),
		# 		"real_productivity": int(first_production[0].get('bottle_neck_qty',0)*3600/(started_time.seconds or 1)),
		# 		"rate_productivity": int(first_production[0].get('bottle_neck_qty',0)*3600/(started_time.seconds or 1)/(first_production[0].get('date_target', 0)/8)*100),
		# 	})
		# 	raise Warning(result)
		# return result

	def _record_update_cut_semi_qty(self, vals):
		if 'cut_quantity' in vals:
			if self.cut_record_dailys.filtered(lambda l: l.record_date == datetime.today().date()):
				parent_id = self.cut_record_dailys.filtered(lambda l: l.record_date == datetime.today().date())[0]
				parent_id.update({
					'quantity': parent_id.quantity + vals.get('cut_quantity',0) - self.cut_quantity,
					})
			else:
				parent_id = self.env['mrp.production.cut.record.daily'].create({
					'production_id': self.id,
					'quantity': vals.get('cut_quantity',0) - self.cut_quantity,
					'record_date': datetime.today()
					})
			self.env['mrp.production.cut.record.detail'].create({
				'production_id': self.id,
				'quantity': vals.get('cut_quantity',0) - self.cut_quantity,
				'parent_id': parent_id.id,
				})
		if 'semi_quantity' in vals:
			if self.semi_record_dailys.filtered(lambda l: l.record_date == datetime.today().date()):
				semi_parent_id = self.semi_record_dailys.filtered(lambda l: l.record_date == datetime.today().date())[0]
				semi_parent_id.update({
					'quantity': semi_parent_id.quantity + vals.get('semi_quantity',0) - self.semi_quantity,
					})
			else:
				semi_parent_id = self.env['mrp.production.semi.record.daily'].create({
					'production_id': self.id,
					'quantity': vals.get('semi_quantity',0) - self.semi_quantity,
					'record_date': datetime.today()
					})
			self.env['mrp.production.semi.record.detail'].create({
				'production_id': self.id,
				'quantity': vals.get('semi_quantity',0) - self.semi_quantity,
				'parent_id': semi_parent_id.id,
				})
		if 'detail_quantity' in vals:
			if self.detail_record_dailys.filtered(lambda l: l.record_date == datetime.today().date()):
				detail_parent_id = self.detail_record_dailys.filtered(lambda l: l.record_date == datetime.today().date())[0]
				detail_parent_id.update({
					'quantity': detail_parent_id.quantity + vals.get('detail_quantity',0) - self.detail_quantity,
					})
			else:
				detail_parent_id = self.env['mrp.production.detail.record.daily'].create({
					'production_id': self.id,
					'quantity': vals.get('detail_quantity',0) - self.detail_quantity,
					'record_date': datetime.today()
					})
			self.env['mrp.production.detail.record.detail'].create({
				'production_id': self.id,
				'quantity': vals.get('detail_quantity',0) - self.detail_quantity,
				'parent_id': detail_parent_id.id,
				})
		if 'semi_special_qty' in vals:
			if self.semi_specific_record_dailys.filtered(lambda l: l.record_date == datetime.today().date()):
				parent_id = self.semi_specific_record_dailys.filtered(lambda l: l.record_date == datetime.today().date())[0]
				parent_id.update({
					'quantity': parent_id.quantity + vals.get('semi_special_qty',0) - self.semi_special_qty,
					})
			else:
				parent_id = self.env['mrp.production.semi.specific.record.daily'].create({
					'production_id': self.id,
					'quantity': vals.get('semi_special_qty',0) - self.semi_special_qty,
					'record_date': datetime.today()
					})
			self.env['mrp.production.semi.specific.record.detail'].create({
				'production_id': self.id,
				'quantity': vals.get('semi_special_qty',0) - self.semi_special_qty,
				'parent_id': parent_id.id,
				})
		if 'special_quantity' in vals:
			if self.specific_record_dailys.filtered(lambda l: l.record_date == datetime.today().date()):
				parent_id = self.specific_record_dailys.filtered(lambda l: l.record_date == datetime.today().date())[0]
				parent_id.update({
					'quantity': parent_id.quantity + vals.get('special_quantity',0) - self.special_quantity,
					})
			else:
				parent_id = self.env['mrp.production.specific.record.daily'].create({
					'production_id': self.id,
					'quantity': vals.get('special_quantity',0) - self.special_quantity,
					'record_date': datetime.today()
					})
			self.env['mrp.production.specific.record.detail'].create({
				'production_id': self.id,
				'quantity': vals.get('special_quantity',0) - self.special_quantity,
				'parent_id': parent_id.id,
				})
		if 'consumed_qty' in vals:
			if self.consume_record_dailys.filtered(lambda l: l.record_date == datetime.today().date()):
				parent_id = self.consume_record_dailys.filtered(lambda l: l.record_date == datetime.today().date())[0]
				parent_id.update({
					'quantity': parent_id.quantity + vals.get('consumed_qty',0) - self.consumed_qty,
					})
			else:
				parent_id = self.env['mrp.production.consume.record.daily'].create({
					'production_id': self.id,
					'quantity': vals.get('consumed_qty',0) - self.consumed_qty,
					'record_date': datetime.today()
					})
			self.env['mrp.production.consume.record.detail'].create({
				'production_id': self.id,
				'quantity': vals.get('consumed_qty',0) - self.consumed_qty,
				'parent_id': parent_id.id,
				})
		if 'combine_qty' in vals:
			if self.combine_record_dailys.filtered(lambda l: l.record_date == datetime.today().date()):
				parent_id = self.combine_record_dailys.filtered(lambda l: l.record_date == datetime.today().date())[0]
				parent_id.update({
					'quantity': parent_id.quantity + vals.get('combine_qty',0) - self.combine_qty,
					})
			else:
				parent_id = self.env['mrp.production.combine.record.daily'].create({
					'production_id': self.id,
					'quantity': vals.get('combine_qty',0) - self.combine_qty,
					'record_date': datetime.today()
					})
			self.env['mrp.production.combine.record.detail'].create({
				'production_id': self.id,
				'quantity': vals.get('combine_qty',0) - self.combine_qty,
				'parent_id': parent_id.id,
				})
		if 'qc_pass' in vals:
			if self.qc_pass_record_dailys.filtered(lambda l: l.record_date == datetime.today().date()):
				parent_id = self.qc_pass_record_dailys.filtered(lambda l: l.record_date == datetime.today().date())[0]
				parent_id.update({
					'quantity': parent_id.quantity + vals.get('qc_pass',0) - self.qc_pass,
					})
			else:
				parent_id = self.env['mrp.production.qc.pass.record.daily'].create({
					'production_id': self.id,
					'quantity': vals.get('qc_pass',0) - self.qc_pass,
					'record_date': datetime.today()
					})
			self.env['mrp.production.qc.pass.record.detail'].create({
				'production_id': self.id,
				'quantity': vals.get('qc_pass',0) - self.qc_pass,
				'parent_id': parent_id.id,
				})


	def write(self, vals):
		if 'cut_quantity' in vals or 'cut_lining_quantity' in vals:
			if not self.env.user.has_group('nc_mrp_dashboard.group_mrp_cut_user') and not self.env.user.has_group('mrp.group_mrp_manager'):
				raise ValidationError(_("Người dùng không có quyền cập nhật dữ liệu này."))
		if 'detail_quantity' in vals or 'semi_quantity' in vals or 'special_quantity' in vals or 'qc_pass' in vals or 'combine_qty' in vals or 'consumed_qty' in vals:
			if not self.env.user.has_group('nc_mrp_dashboard.group_mrp_line_user') and not self.env.user.has_group('mrp.group_mrp_manager'):
				raise ValidationError(_("Người dùng không có quyền cập nhật dữ liệu này."))
		if 'semi_special_qty' in vals:
			if not self.env.user.has_group('nc_mrp_dashboard.group_mrp_specific_user') and not self.env.user.has_group('mrp.group_mrp_manager'):
				raise ValidationError(_("Người dùng không có quyền cập nhật dữ liệu này."))
		accumulated_dict = {}
		if self._context.get('accumulated_qty',True) and ('detail_quantity' in vals or 'cut_quantity' in vals or 'cut_lining_quantity' in vals or 'semi_quantity' in vals or 'special_quantity' in vals or 'qc_pass' in vals or 'combine_qty' in vals or 'semi_special_qty' in vals or 'consumed_qty' in vals):
			for record in self:
				if 'cut_quantity' in vals:
					if vals['cut_quantity'] > 0:
						if (vals['cut_quantity'] + record.cut_quantity) <= int(record.product_qty):
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['cut_quantity'] = vals['cut_quantity'] + record.cut_quantity
							if self._context.get('table_line_id', False):
								accumulated_dict[record.id]['table_line_ids'] = [(0,0,{
									'table_line_id': self._context.get('table_line_id', False),
									'quantity': vals['cut_quantity']
									})]
							if self._context.get('table_line_id', False):
								self.env['namco.mrp.cut.table.production.line'].browse(self._context.get('table_line_id', False)).update({
									'cut_remain_qty': self.env['namco.mrp.cut.table.production.line'].browse(self._context.get('table_line_id', False)).cut_remain_qty or 0,
									})
						else:
							remain_qty = vals['cut_quantity']
							if record.product_qty > record.cut_quantity:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}
								accumulated_dict[record.id]['cut_quantity'] = record.product_qty
								if self._context.get('table_line_id', False):
									accumulated_dict[record.id]['table_line_ids'] = [(0,0,{
										'table_line_id': self._context.get('table_line_id', False),
										'quantity': record.product_qty - record.cut_quantity
										})]
								remain_qty -= (record.product_qty - record.cut_quantity)
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))], order="order_code asc").filtered(lambda m: m.product_qty > m.cut_quantity):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if (remain_qty + other_mo.cut_quantity) <= other_mo.product_qty:
									accumulated_dict[other_mo.id]['cut_quantity'] = remain_qty + other_mo.cut_quantity
									if self._context.get('table_line_id', False):
										accumulated_dict[other_mo.id]['table_line_ids'] = [(0,0,{
											'table_line_id': self._context.get('table_line_id', False),
											'quantity': remain_qty
											})]
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['cut_quantity'] = other_mo.product_qty
									if self._context.get('table_line_id', False):
										accumulated_dict[other_mo.id]['table_line_ids'] = [(0,0,{
											'table_line_id': self._context.get('table_line_id', False),
											'quantity': other_mo.product_qty - other_mo.cut_quantity
											})]
									remain_qty -= (other_mo.product_qty - other_mo.cut_quantity)
							if self._context.get('table_line_id', False):
								self.env['namco.mrp.cut.table.production.line'].browse(self._context.get('table_line_id', False)).update({
									'cut_remain_qty': (self.env['namco.mrp.cut.table.production.line'].browse(self._context.get('table_line_id', False)).cut_remain_qty or 0) + remain_qty
									})
							# if remain_qty:
							# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					elif vals['cut_quantity'] < 0:
						remain_qty = -1*vals['cut_quantity']
						if record.cut_quantity >= remain_qty:
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['cut_quantity'] = record.cut_quantity + vals['cut_quantity']
							remain_qty = 0
						else:
							if record.cut_quantity > 0:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}

								accumulated_dict[record.id]['cut_quantity'] = 0
								remain_qty -= record.cut_quantity
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress')),('cut_quantity','>',0)], order="order_code desc"):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if remain_qty <= other_mo.cut_quantity:
									accumulated_dict[other_mo.id]['cut_quantity'] = other_mo.cut_quantity - remain_qty
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['cut_quantity'] = 0
									remain_qty -= other_mo.cut_quantity
				if 'cut_lining_quantity' in vals:
					if vals['cut_lining_quantity'] > 0:
						if (vals['cut_lining_quantity'] + record.cut_lining_quantity) <= int(record.product_qty):
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['cut_lining_quantity'] = vals['cut_lining_quantity'] + record.cut_lining_quantity
						else:
							remain_qty = vals['cut_lining_quantity']
							if record.product_qty > record.cut_lining_quantity:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}
								accumulated_dict[record.id]['cut_lining_quantity'] = record.product_qty
								remain_qty -= (record.product_qty - record.cut_lining_quantity)
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))], order="order_code asc").filtered(lambda m: m.product_qty > m.cut_lining_quantity):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if (remain_qty + other_mo.cut_lining_quantity) <= other_mo.product_qty:
									accumulated_dict[other_mo.id]['cut_lining_quantity'] = remain_qty + other_mo.cut_lining_quantity
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['cut_lining_quantity'] = other_mo.product_qty
									remain_qty -= (other_mo.product_qty - other_mo.cut_lining_quantity)
							# if remain_qty:
							# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					elif vals['cut_lining_quantity'] < 0:
						remain_qty = -1*vals['cut_lining_quantity']
						if record.cut_lining_quantity >= remain_qty:
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['cut_lining_quantity'] = record.cut_lining_quantity + vals['cut_lining_quantity']
							remain_qty = 0
						else:
							if record.cut_lining_quantity > 0:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}

								accumulated_dict[record.id]['cut_lining_quantity'] = 0
								remain_qty -= record.cut_lining_quantity
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress')),('cut_lining_quantity','>',0)], order="order_code desc"):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if remain_qty <= other_mo.cut_lining_quantity:
									accumulated_dict[other_mo.id]['cut_lining_quantity'] = other_mo.cut_lining_quantity - remain_qty
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['cut_lining_quantity'] = 0
									remain_qty -= other_mo.cut_lining_quantity
				if 'semi_quantity' in vals:
					if vals['semi_quantity'] > 0:
						if (vals['semi_quantity'] + record.semi_quantity) <= int(record.product_qty):
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['semi_quantity'] = vals['semi_quantity'] + record.semi_quantity
						else:
							remain_qty = vals['semi_quantity']
							if record.product_qty > record.semi_quantity:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}
								accumulated_dict[record.id]['semi_quantity'] = record.product_qty
								remain_qty -= (record.product_qty - record.semi_quantity)
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('department_line_id','=',record.department_line_id.id),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))], order="order_code asc").filtered(lambda m: m.product_qty > m.semi_quantity):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if (remain_qty + other_mo.semi_quantity) <= other_mo.product_qty:
									accumulated_dict[other_mo.id]['semi_quantity'] = remain_qty + other_mo.semi_quantity
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['semi_quantity'] = other_mo.product_qty
									remain_qty -= (other_mo.product_qty - other_mo.semi_quantity)
							# if remain_qty:
							# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					elif vals['semi_quantity'] < 0:
						remain_qty = -1*vals['semi_quantity']
						if record.semi_quantity >= remain_qty:
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['semi_quantity'] = record.semi_quantity + vals['semi_quantity']
							remain_qty = 0
						else:
							if record.semi_quantity > 0:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}

								accumulated_dict[record.id]['semi_quantity'] = 0
								remain_qty -= record.semi_quantity
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('department_line_id','=',record.department_line_id.id),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress')),('semi_quantity','>',0)], order="order_code desc"):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if remain_qty <= other_mo.semi_quantity:
									accumulated_dict[other_mo.id]['semi_quantity'] = other_mo.semi_quantity - remain_qty
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['semi_quantity'] = 0
									remain_qty -= other_mo.semi_quantity
				if 'detail_quantity' in vals:
					if vals['detail_quantity'] > 0:
						if (vals['detail_quantity'] + record.detail_quantity) <= int(record.product_qty):
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['detail_quantity'] = vals['detail_quantity'] + record.detail_quantity
						else:
							remain_qty = vals['detail_quantity']
							if record.product_qty > record.detail_quantity:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}
								accumulated_dict[record.id]['detail_quantity'] = record.product_qty
								remain_qty -= (record.product_qty - record.detail_quantity)
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('department_line_id','=',record.department_line_id.id),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))], order="order_code asc").filtered(lambda m: m.product_qty > m.detail_quantity):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if (remain_qty + other_mo.detail_quantity) <= other_mo.product_qty:
									accumulated_dict[other_mo.id]['detail_quantity'] = remain_qty + other_mo.detail_quantity
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['detail_quantity'] = other_mo.product_qty
									remain_qty -= (other_mo.product_qty - other_mo.detail_quantity)
							# if remain_qty:
							# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					elif vals['detail_quantity'] < 0:
						remain_qty = -1*vals['detail_quantity']
						if record.detail_quantity >= remain_qty:
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['detail_quantity'] = record.detail_quantity + vals['detail_quantity']
							remain_qty = 0
						else:
							if record.detail_quantity > 0:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}

								accumulated_dict[record.id]['detail_quantity'] = 0
								remain_qty -= record.detail_quantity
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('department_line_id','=',record.department_line_id.id),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress')),('detail_quantity','>',0)], order="order_code desc"):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if remain_qty <= other_mo.detail_quantity:
									accumulated_dict[other_mo.id]['detail_quantity'] = other_mo.detail_quantity - remain_qty
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['detail_quantity'] = 0
									remain_qty -= other_mo.detail_quantity
				if 'special_quantity' in vals:
					if vals['special_quantity'] > 0:
						if (vals['special_quantity'] + record.special_quantity) <= int(record.product_qty):
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['special_quantity'] = vals['special_quantity'] + record.special_quantity
						else:
							remain_qty = vals['special_quantity']
							if record.product_qty > record.special_quantity:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}
								accumulated_dict[record.id]['special_quantity'] = record.product_qty
								remain_qty -= (record.product_qty - record.special_quantity)
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('department_line_id','=',record.department_line_id.id),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))], order="order_code asc").filtered(lambda m: m.product_qty > m.special_quantity):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if (remain_qty + other_mo.special_quantity) <= other_mo.product_qty:
									accumulated_dict[other_mo.id]['special_quantity'] = remain_qty + other_mo.special_quantity
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['special_quantity'] = other_mo.product_qty
									remain_qty -= (other_mo.product_qty - other_mo.special_quantity)
							# if remain_qty:
							# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					elif vals['special_quantity'] < 0:
						remain_qty = -1*vals['special_quantity']
						if record.special_quantity >= remain_qty:
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['special_quantity'] = record.special_quantity + vals['special_quantity']
							remain_qty = 0
						else:
							if record.special_quantity > 0:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}

								accumulated_dict[record.id]['special_quantity'] = 0
								remain_qty -= record.special_quantity
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('department_line_id','=',record.department_line_id.id),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress')),('special_quantity','>',0)], order="order_code desc"):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if remain_qty <= other_mo.special_quantity:
									accumulated_dict[other_mo.id]['special_quantity'] = other_mo.special_quantity - remain_qty
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['special_quantity'] = 0
									remain_qty -= other_mo.special_quantity
				if 'qc_pass' in vals:
					if vals['qc_pass'] > 0:
						remain_qty = vals['qc_pass']
						for other_mo in self.search([('product_v_color','=',record.product_v_color),('consumed_qty','>',0),('special_quantity','>',0),('department_line_id','=',record.department_line_id.id),('product_template_id','=',record.product_template_id.id),('state','in',('confirmed','progress'))], order="order_code asc, sequence_size asc").filtered(lambda m: m.consumed_qty > m.qc_pass and m.special_quantity > m.qc_pass):
							if remain_qty <= 0:
								break
							compare_qty = 0
							if other_mo.consumed_qty >= other_mo.special_quantity:
								compare_qty = other_mo.special_quantity
							else:
								compare_qty = other_mo.consumed_qty
							if other_mo.id not in accumulated_dict:
								accumulated_dict[other_mo.id] = {}
							if (remain_qty + other_mo.qc_pass) <= compare_qty:
								accumulated_dict[other_mo.id]['qc_pass'] = remain_qty + other_mo.qc_pass
								remain_qty = 0
							else:
								accumulated_dict[other_mo.id]['qc_pass'] = compare_qty
								remain_qty -= (compare_qty - other_mo.qc_pass)
							# if remain_qty:
							# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					elif vals['qc_pass'] < 0:
						remain_qty = -1*vals['qc_pass']
						for other_mo in self.search([('product_v_color','=',record.product_v_color),('department_line_id','=',record.department_line_id.id),('product_template_id','=',record.product_template_id.id),('state','in',('confirmed','progress')),('qc_pass','>',0)], order="order_code desc, sequence_size desc "):
							if remain_qty <= 0:
								break
							if other_mo.id not in accumulated_dict:
								accumulated_dict[other_mo.id] = {}
							if remain_qty <= other_mo.qc_pass:
								accumulated_dict[other_mo.id]['qc_pass'] = other_mo.qc_pass - remain_qty
								remain_qty = 0
							else:
								accumulated_dict[other_mo.id]['qc_pass'] = 0
								remain_qty -= other_mo.qc_pass
				if 'combine_qty' in vals:
					if vals['combine_qty'] > 0:
						if (vals['combine_qty'] + record.combine_qty) <= int(record.product_qty):
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['combine_qty'] = vals['combine_qty'] + record.combine_qty
						else:
							remain_qty = vals['combine_qty']
							if record.product_qty > record.combine_qty:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}
								accumulated_dict[record.id]['combine_qty'] = record.product_qty
								remain_qty -= (record.product_qty - record.combine_qty)
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('department_line_id','=',record.department_line_id.id),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))], order="order_code asc").filtered(lambda m: m.product_qty > m.combine_qty):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if (remain_qty + other_mo.combine_qty) <= other_mo.product_qty:
									accumulated_dict[other_mo.id]['combine_qty'] = remain_qty + other_mo.combine_qty
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['combine_qty'] = other_mo.product_qty
									remain_qty -= (other_mo.product_qty - other_mo.combine_qty)
							# if remain_qty:
							# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					elif vals['combine_qty'] < 0:
						remain_qty = -1*vals['combine_qty']
						if record.combine_qty >= remain_qty:
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['combine_qty'] = record.combine_qty + vals['combine_qty']
							remain_qty = 0
						else:
							if record.combine_qty > 0:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}

								accumulated_dict[record.id]['combine_qty'] = 0
								remain_qty -= record.combine_qty
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('department_line_id','=',record.department_line_id.id),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress')),('combine_qty','>',0)], order="order_code desc"):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if remain_qty <= other_mo.combine_qty:
									accumulated_dict[other_mo.id]['combine_qty'] = other_mo.combine_qty - remain_qty
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['combine_qty'] = 0
									remain_qty -= other_mo.combine_qty
				if 'semi_special_qty' in vals:
					if vals['semi_special_qty'] > 0:
						if (vals['semi_special_qty'] + record.semi_special_qty) <= int(record.product_qty):
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['semi_special_qty'] = vals['semi_special_qty'] + record.semi_special_qty
							if self._context.get('table_line_id', False):
								accumulated_dict[record.id]['semi_specific_table_line_ids'] = [(0,0,{
									'table_line_id': self._context.get('table_line_id', False),
									'quantity': vals['semi_special_qty']
									})]
							if self._context.get('table_line_id', False):
								self.env['namco.mrp.cut.table.production.line'].browse(self._context.get('table_line_id', False)).update({
									'semi_specific_remain_quantity': self.env['namco.mrp.cut.table.production.line'].browse(self._context.get('table_line_id', False)).semi_specific_remain_quantity or 0,
									})
						else:
							remain_qty = vals['semi_special_qty']
							if record.product_qty > record.semi_special_qty:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}
								accumulated_dict[record.id]['semi_special_qty'] = record.product_qty
								if self._context.get('table_line_id', False):
									accumulated_dict[record.id]['semi_specific_table_line_ids'] = [(0,0,{
										'table_line_id': self._context.get('table_line_id', False),
										'quantity': record.product_qty - record.semi_special_qty
										})]
								remain_qty -= (record.product_qty - record.semi_special_qty)
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))], order="order_code asc").filtered(lambda m: m.product_qty > m.semi_special_qty):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if (remain_qty + other_mo.semi_special_qty) <= other_mo.product_qty:
									accumulated_dict[other_mo.id]['semi_special_qty'] = remain_qty + other_mo.semi_special_qty
									if self._context.get('table_line_id', False):
										accumulated_dict[other_mo.id]['semi_specific_table_line_ids'] = [(0,0,{
											'table_line_id': self._context.get('table_line_id', False),
											'quantity': remain_qty
											})]
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['semi_special_qty'] = other_mo.product_qty
									if self._context.get('table_line_id', False):
										accumulated_dict[other_mo.id]['semi_specific_table_line_ids'] = [(0,0,{
											'table_line_id': self._context.get('table_line_id', False),
											'quantity': other_mo.product_qty - other_mo.semi_special_qty
											})]
									remain_qty -= (other_mo.product_qty - other_mo.semi_special_qty)
							if self._context.get('table_line_id', False):
								self.env['namco.mrp.cut.table.production.line'].browse(self._context.get('table_line_id', False)).update({
									'semi_specific_remain_quantity': (self.env['namco.mrp.cut.table.production.line'].browse(self._context.get('table_line_id', False)).semi_specific_remain_quantity or 0) + remain_qty
									})
							# if remain_qty:
							# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					elif vals['semi_special_qty'] < 0:
						remain_qty = -1*vals['semi_special_qty']
						if record.semi_special_qty >= remain_qty:
							if record.id not in accumulated_dict:
								accumulated_dict[record.id] = {}
							accumulated_dict[record.id]['semi_special_qty'] = record.semi_special_qty + vals['semi_special_qty']
							remain_qty = 0
						else:
							if record.semi_special_qty > 0:
								if record.id not in accumulated_dict:
									accumulated_dict[record.id] = {}

								accumulated_dict[record.id]['semi_special_qty'] = 0
								remain_qty -= record.semi_special_qty
							order_code_list = []
							if record.cut_table_ids:
								order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
							for other_mo in self.search([('order_code','in',order_code_list),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress')),('cut_quantity','>',0)], order="order_code desc"):
								if remain_qty <= 0:
									break
								if other_mo.id not in accumulated_dict:
									accumulated_dict[other_mo.id] = {}
								if remain_qty <= other_mo.semi_special_qty:
									accumulated_dict[other_mo.id]['semi_special_qty'] = other_mo.semi_special_qty - remain_qty
									remain_qty = 0
								else:
									accumulated_dict[other_mo.id]['semi_special_qty'] = 0
									remain_qty -= other_mo.semi_special_qty


					# if vals['semi_special_qty'] > 0:
					# 	if (vals['semi_special_qty'] + record.semi_special_qty) <= int(record.product_qty):
					# 		if record.id not in accumulated_dict:
					# 			accumulated_dict[record.id] = {}
					# 		accumulated_dict[record.id]['semi_special_qty'] = vals['semi_special_qty'] + record.semi_special_qty
					# 	else:
					# 		remain_qty = vals['semi_special_qty']
					# 		if record.product_qty > record.semi_special_qty:
					# 			if record.id not in accumulated_dict:
					# 				accumulated_dict[record.id] = {}
					# 			accumulated_dict[record.id]['semi_special_qty'] = record.product_qty
					# 			remain_qty -= (record.product_qty - record.semi_special_qty)
					# 		order_code_list = []
					# 		if record.cut_table_ids:
					# 			order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
					# 		for other_mo in self.search([('order_code','in',order_code_list),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))], order="order_code asc").filtered(lambda m: m.product_qty > m.semi_special_qty):
					# 			if remain_qty <= 0:
					# 				break
					# 			if other_mo.id not in accumulated_dict:
					# 				accumulated_dict[other_mo.id] = {}
					# 			if (remain_qty + other_mo.semi_special_qty) <= other_mo.product_qty:
					# 				accumulated_dict[other_mo.id]['semi_special_qty'] = remain_qty + other_mo.semi_special_qty
					# 				remain_qty = 0
					# 			else:
					# 				accumulated_dict[other_mo.id]['semi_special_qty'] = other_mo.product_qty
					# 				remain_qty -= (other_mo.product_qty - other_mo.semi_special_qty)
					# 		# if remain_qty:
					# 		# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					# elif vals['semi_special_qty'] < 0:
					# 	remain_qty = -1*vals['semi_special_qty']
					# 	if record.semi_special_qty >= remain_qty:
					# 		if record.id not in accumulated_dict:
					# 			accumulated_dict[record.id] = {}
					# 		accumulated_dict[record.id]['semi_special_qty'] = record.semi_special_qty + vals['semi_special_qty']
					# 		remain_qty = 0
					# 	else:
					# 		if record.semi_special_qty > 0:
					# 			if record.id not in accumulated_dict:
					# 				accumulated_dict[record.id] = {}

					# 			accumulated_dict[record.id]['semi_special_qty'] = 0
					# 			remain_qty -= record.semi_special_qty
					# 		order_code_list = []
					# 		if record.cut_table_ids:
					# 			order_code_list += record.cut_table_ids[0].parent_id.order_code_ids.ids
					# 		for other_mo in self.search([('order_code','in',order_code_list),('id','!=',record.id),('product_id','=',record.product_id.id),('state','in',('confirmed','progress')),('semi_special_qty','>',0)], order="order_code desc"):
					# 			if remain_qty <= 0:
					# 				break
					# 			if other_mo.id not in accumulated_dict:
					# 				accumulated_dict[other_mo.id] = {}
					# 			if remain_qty <= other_mo.semi_special_qty:
					# 				accumulated_dict[other_mo.id]['semi_special_qty'] = other_mo.semi_special_qty - remain_qty
					# 				remain_qty = 0
					# 			else:
					# 				accumulated_dict[other_mo.id]['semi_special_qty'] = 0
					# 				remain_qty -= other_mo.semi_special_qty
				if 'consumed_qty' in vals:
					if vals['consumed_qty'] > 0:
						remain_qty = vals['consumed_qty']
						for other_mo in self.search([('product_v_color','=',record.product_v_color),('department_line_id','=',record.department_line_id.id),('product_template_id','=',record.product_template_id.id),('state','in',('confirmed','progress'))], order="order_code asc, sequence_size asc").filtered(lambda m: m.combine_qty > m.consumed_qty):
							if remain_qty <= 0:
								break
							if other_mo.id not in accumulated_dict:
								accumulated_dict[other_mo.id] = {}
							if (remain_qty + other_mo.consumed_qty) <= other_mo.combine_qty:
								accumulated_dict[other_mo.id]['consumed_qty'] = remain_qty + other_mo.consumed_qty
								remain_qty = 0
							else:
								accumulated_dict[other_mo.id]['consumed_qty'] = other_mo.combine_qty
								remain_qty -= (other_mo.combine_qty - other_mo.consumed_qty)
							# if remain_qty:
							# 	raise ValidationError(_("Số lượng nhập vượt quá chỉ tiêu."))
					elif vals['consumed_qty'] < 0:
						remain_qty = -1*vals['consumed_qty']
						for other_mo in self.search([('product_v_color','=',record.product_v_color),('department_line_id','=',record.department_line_id.id),('product_template_id','=',record.product_template_id.id),('state','in',('confirmed','progress')),('consumed_qty','>',0)], order="order_code desc, sequence_size desc"):
							if remain_qty <= 0:
								break
							if other_mo.id not in accumulated_dict:
								accumulated_dict[other_mo.id] = {}
							if remain_qty <= other_mo.consumed_qty:
								accumulated_dict[other_mo.id]['consumed_qty'] = other_mo.consumed_qty - remain_qty
								remain_qty = 0
							else:
								accumulated_dict[other_mo.id]['consumed_qty'] = 0
								remain_qty -= other_mo.consumed_qty
			vals.pop('cut_quantity', None)
			vals.pop('cut_lining_quantity', None)
			vals.pop('semi_quantity', None)
			vals.pop('special_quantity', None)
			vals.pop('qc_pass', None)
			vals.pop('combine_qty', None)
			vals.pop('semi_special_qty', None)
			vals.pop('consumed_qty', None)
			vals.pop('detail_quantity', None)
		if accumulated_dict:
			for key,value in accumulated_dict.items():
				self.browse(key)._record_update_cut_semi_qty(value)
				self.browse(key).with_context(accumulated_qty=False).update(value)
		res = super(NAMCOMRPPRODUCTION, self).write(vals)
		
		return res

	def query_specific_dashboard_data(self):
		params = []
		query = '''
			SELECT mo.department_line_id, mo.product_template_id, mo.product_v_color,
				SUM(mo.product_qty)	AS product_qty,
				SUM(mo.cut_quantity)	AS cut_quantity,
				SUM(mo.semi_special_qty)	AS semi_special_qty,
				SUM(mo.semi_quantity)	AS semi_quantity,
				SUM(mo.special_quantity)	AS special_quantity,
				SUM(mo.combine_qty)	AS combine_qty,
				SUM(mo.consumed_qty)	AS consumed_qty,
				SUM(mo.qc_pass)	AS qc_pass
			FROM mrp_production mo
			WHERE mo.state IN ('confirmed', 'progress')
			GROUP BY mo.department_line_id, mo.product_template_id, mo.product_v_color
			ORDER BY mo.department_line_id
		'''
		self._cr.execute(query, params)
		query_res = self._cr.dictfetchall()
		return query_res

	@api.model
	def format_specific_dashboard_data(self):
		results = self.query_specific_dashboard_data()
		format_data = []
		for record in results:
			format_data.append({
				'department_line_id': self.env['hr.department'].search_read([('id','=',record.get('department_line_id',False))],['name'], limit=1) and \
					self.env['hr.department'].search_read([('id','=',record.get('department_line_id',False))],['name'], limit=1)[0].get('name', '') or '',
				'product_template_id': self.env['product.template'].search_read([('id','=',record.get('product_template_id',False))],['name'], limit=1) and \
					self.env['product.template'].search_read([('id','=',record.get('product_template_id',False))],['name'], limit=1)[0].get('name', ''),
				'product_v_color': record.get('product_v_color', ''),
				'cut_quantity': record.get('cut_quantity', 0),
				'product_qty': int(record.get('product_qty', 0)),
				'semi_special_qty': record.get('semi_special_qty', 0),
				'special_quantity': record.get('special_quantity', 0),
				'combine_qty': record.get('combine_qty', 0),
				'consumed_qty': record.get('consumed_qty', 0),
				'qc_pass': record.get('qc_pass', 0),
				})
		return format_data


class NAMCOMRPPRODUCTIONTABLELINE(models.Model):
	_name = "mrp.production.table.line"

	table_line_id = fields.Many2one('namco.mrp.cut.table.production.line', string="Table line")
	quantity = fields.Integer(string="Quantity")
	production_id = fields.Many2one('mrp.production', string="Production")


class NAMCOMRPPRODUCTIONSEMISPECIFICTABLELINE(models.Model):
	_name = "mrp.production.semi.specific.table.line"

	table_line_id = fields.Many2one('namco.mrp.cut.table.production.line', string="Table line")
	quantity = fields.Integer(string="Quantity")
	production_id = fields.Many2one('mrp.production', string="Production")
	

class HRDEPARTMENT(models.Model):
	_inherit = "hr.department"
	_rec_name = 'name'

	department_line_sequence = fields.Integer(string="Line Sequence")

class HREMPLOYEE(models.Model):
	_inherit = "hr.employee"

	workcenter_id = fields.Many2one('mrp.workcenter', string="Work Center")


class MRPCUTRECORDDETAIL(models.Model):
	_name = "mrp.production.cut.record.detail"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	record_time = fields.Datetime(string="Record time")
	quantity = fields.Integer(string="Record quantity")
	parent_id = fields.Many2one('mrp.production.cut.record.daily', string="Parent")

class MRPCUTRECORDDAILY(models.Model):
	_name = "mrp.production.cut.record.daily"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	record_date = fields.Date(string="Record date")
	quantity = fields.Integer(string="Record quantity")
	line_ids = fields.One2many('mrp.production.cut.record.detail', 'parent_id',string="Detail")

class MRPSEMIRECORDDETAIL(models.Model):
	_name = "mrp.production.semi.record.detail"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	quantity = fields.Integer(string="Record quantity")
	parent_id = fields.Many2one('mrp.production.semi.record.daily', string="Parent")

class MRPSEMIRECORDDAILY(models.Model):
	_name = "mrp.production.semi.record.daily"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	record_date = fields.Date(string="Record date")
	quantity = fields.Integer(string="Record quantity")
	line_ids = fields.One2many('mrp.production.semi.record.detail', 'parent_id', string="Detail")

class MRPDETAILRECORDDETAIL(models.Model):
	_name = "mrp.production.detail.record.detail"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	quantity = fields.Integer(string="Record quantity")
	parent_id = fields.Many2one('mrp.production.detail.record.daily', string="Parent")

class MRPDETAILRECORDDAILY(models.Model):
	_name = "mrp.production.detail.record.daily"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	record_date = fields.Date(string="Record date")
	quantity = fields.Integer(string="Record quantity")
	line_ids = fields.One2many('mrp.production.detail.record.detail', 'parent_id', string="Detail")

class MRPSPECIFICRECORDDETAIL(models.Model):
	_name = "mrp.production.specific.record.detail"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	quantity = fields.Integer(string="Record quantity")
	parent_id = fields.Many2one('mrp.production.specific.record.daily', string="Parent")

class MRPSPECIFICRECORDDAILY(models.Model):
	_name = "mrp.production.specific.record.daily"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	record_date = fields.Date(string="Record date")
	quantity = fields.Integer(string="Record quantity")
	line_ids = fields.One2many('mrp.production.specific.record.detail', 'parent_id',string="Detail")

class MRPSEMISPECIFICRECORDDETAIL(models.Model):
	_name = "mrp.production.semi.specific.record.detail"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	quantity = fields.Integer(string="Record quantity")
	parent_id = fields.Many2one('mrp.production.semi.specific.record.daily', string="Parent")

class MRPSEMISPECIFICRECORDDAILY(models.Model):
	_name = "mrp.production.semi.specific.record.daily"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	record_date = fields.Date(string="Record date")
	quantity = fields.Integer(string="Record quantity")
	line_ids = fields.One2many('mrp.production.semi.specific.record.detail', 'parent_id',string="Detail")

class MRPCOMBINERECORDDETAIL(models.Model):
	_name = "mrp.production.combine.record.detail"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	quantity = fields.Integer(string="Record quantity")
	parent_id = fields.Many2one('mrp.production.combine.record.daily', string="Parent")

class MRPCOMBINERECORDDAILY(models.Model):
	_name = "mrp.production.combine.record.daily"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	record_date = fields.Date(string="Record date")
	quantity = fields.Integer(string="Record quantity")
	line_ids = fields.One2many('mrp.production.combine.record.detail', 'parent_id',string="Detail")

class MRPCONSUMERECORDDETAIL(models.Model):
	_name = "mrp.production.consume.record.detail"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	quantity = fields.Integer(string="Record quantity")
	parent_id = fields.Many2one('mrp.production.consume.record.daily', string="Parent")

class MRPCONSUMERECORDDAILY(models.Model):
	_name = "mrp.production.consume.record.daily"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	record_date = fields.Date(string="Record date")
	quantity = fields.Integer(string="Record quantity")
	line_ids = fields.One2many('mrp.production.consume.record.detail', 'parent_id',string="Detail")

class MRPQCPASSRECORDDETAIL(models.Model):
	_name = "mrp.production.qc.pass.record.detail"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	quantity = fields.Integer(string="Record quantity")
	parent_id = fields.Many2one('mrp.production.qc.pass.record.daily', string="Parent")

class MRPQCPASSRECORDDAILY(models.Model):
	_name = "mrp.production.qc.pass.record.daily"

	name = fields.Char(string="Name")
	production_id = fields.Many2one('mrp.production', string="Production")
	record_date = fields.Date(string="Record date")
	quantity = fields.Integer(string="Record quantity")
	line_ids = fields.One2many('mrp.production.qc.pass.record.detail', 'parent_id',string="Detail")


class NAMCOORDERCODE(models.Model):
	_name = 'namco.order.code'

	name = fields.Char(string="OC")