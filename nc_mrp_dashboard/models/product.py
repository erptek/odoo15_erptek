# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from odoo.osv import expression

class NAMCOPRODUCTPRODUCT(models.Model):
	_inherit = "product.product"


	product_size = fields.Char(string="Size")
	sequence_size = fields.Integer(string="Sequence Size", default=0)
	product_v_color = fields.Char(string="Color")


	@api.model
	def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
		args = args or []
		domain = []
		if name:
			domain = ['|', ('product_size', operator, name), ('name', operator, name)]
			if operator in expression.NEGATIVE_TERM_OPERATORS:
				domain = ['&', '!'] + domain[1:]
		return self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)



class NAMCOPRODUCTTEMPLATE(models.Model):
	_inherit = "product.template"

	def _get_routes(self):
		return self.env['stock.location.route'].search([('name', 'in', ['Replenish on Order (MTO)','Manufacture','Bổ sung cho Đơn hàng (MTO)','Sản xuất'])]).ids

	product_add_mode = fields.Selection(default='matrix')
	route_ids = fields.Many2many(default=_get_routes)
	purchase_ok = fields.Boolean(default=False)



	def _prepare_variant_values(self, combination):
		res = super(NAMCOPRODUCTTEMPLATE, self)._prepare_variant_values(combination)
		size = ''
		color = ''
		for comb in combination:
			if comb.attribute_id.name == 'Size':
				size = comb.name
			if comb.attribute_id.name == 'Color':
				color = comb.name
		res['product_size'] = size
		res['product_v_color'] = color
		sequence_size = 0
		if size.isnumeric():
			sequence_size = int(size)
		elif size == 'S' or size == 's':
			sequence_size = 1
		elif size == "M" or size == "m":
			sequence_size = 2
		elif size == "L" or size == "l":
			sequence_size = 3
		elif size == "XL" or size == "xl" or size == "1X" or size == "1x":
			sequence_size = 4
		elif size in ["2XL","2xl","XXL","xxl","2X","2x"]:
			sequence_size = 5
		elif size in ["3XL","3xl","XXXL","xxxl","3X","3x"]:
			sequence_size = 6
		elif size in ["4XL","4xl","XXXXL","xxxxl","4X","4x"]:
			sequence_size = 7
		elif size in ["5XL","5xl","XXXXXL","xxxxxl","5X","5x"]:
			sequence_size = 8
		elif size in ["6XL","6xl","XXXXXXL","xxxxxxl","6X","6x"]:
			sequence_size = 9
		elif size in ["7XL","7xl","XXXXXXXL","xxxxxxxl","7X","7x"]:
			sequence_size = 10
		elif size in ["8XL","8xl","XXXXXXXXL","xxxxxxxxl","8X","8x"]:
			sequence_size = 11
		elif size in ["9XL","9xl","XXXXXXXXXL","xxxxxxxxxl","9X","9x"]:
			sequence_size = 12
		elif size in ["10XL","10xl","XXXXXXXXXXL","xxxxxxxxxxl","10X","10x"]:
			sequence_size = 13
		res['sequence_size'] = sequence_size
		return res

class NAMCORESPARTNER(models.Model):
	_inherit = "res.partner"


	property_account_payable_id = fields.Many2one(required=False)
	property_account_receivable_id = fields.Many2one(required=False)