# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from odoo.exceptions import ValidationError

class NAMCOMRPCUTPRODUCTION(models.Model):
	_name = "namco.mrp.cut.production"


	def _domain_attribute_color_value(self):
		attribute_id = self.env['product.attribute'].search_read([('name','=ilike','Color')], limit=1)
		domain = []
		if attribute_id:
			domain = [('attribute_id','=',attribute_id[0].get('id',False))]
		return domain

	name = fields.Char("Process Order No.")
	product_template_id = fields.Many2one('product.template', string="Style", required=True)
	product_color = fields.Many2one('product.attribute.value',domain=_domain_attribute_color_value,required=True, string="Màu")
	product_v_color = fields.Char(string="Tên Màu", related='product_color.name', store=True, readonly=True)
	order_code_ids = fields.Many2many('namco.order.code', string="OC No.")
	fpo_code = fields.Char(string="FPO No.")
	start_date = fields.Date(string="Start Date")
	end_date = fields.Date(string="End Date")
	line_ids = fields.One2many('namco.mrp.cut.production.detail', 'parent_id', string="Detail")
	table_ids = fields.One2many('namco.mrp.cut.table.production', 'parent_id', string="Tables")
	table_line_ids = fields.One2many('namco.mrp.cut.table.production.line', 'parent_id', string="Table lines")
	state = fields.Selection([('draft','Nháp'),
		('progress','Đang cắt'),
		('done', 'Hoàn thành')], string="Trạng thái", default='draft')

	def refresh_view(self):
		return True


class NAMCOMRPCUTPRODUCTIONDETAIL(models.Model):
	_name = "namco.mrp.cut.production.detail"

	product_id = fields.Many2one('product.product', string="Style", required=True)
	product_size = fields.Char(string="Size", related='product_id.product_size', store=True, readonly=True)
	uom_id = fields.Many2one('uom.uom', string="UOM")
	quantity = fields.Integer(string="Order Qty")
	cut_quantity = fields.Integer(string="Đã cắt", compute='_compute_cut_quantity', readonly=True)
	deviant_quantity = fields.Integer(string="Chênh lệch", compute='_compute_cut_quantity', readonly=True)
	parent_id = fields.Many2one('namco.mrp.cut.production', string="Parent")

	@api.depends('parent_id.table_line_ids')
	def _compute_cut_quantity(self):
		for record in self:
			cut_qty = sum(line.cut_quantity for line in record.parent_id.table_line_ids.filtered(lambda l: l.product_id.id == record.product_id.id and l.cut_state == 'confirm'))
			record.cut_quantity = cut_qty
			record.deviant_quantity = cut_qty - record.quantity
	# @api.onchange('product_id')
	# def _onchange_product_id(self):
	# 	for record in self:
	# 		record.uom_id = record.product_id.uom_id.id


class NAMCOMRPCUTTABLEPRODUCTION(models.Model):
	_name = "namco.mrp.cut.table.production"


	name = fields.Char(string="Số bàn")

	sequence = fields.Integer(string="Sequence",default=0)
	product_v_color = fields.Char(string="Màu", related='parent_id.product_color.name', store=True, readonly=True)
	giang_type = fields.Char(string="Loại")
	layer_qty = fields.Integer(string="Số lớp", default=0, required=True)
	product_template_id = fields.Many2one('product.template', related='parent_id.product_template_id', store=True, readonly=True)
	parent_id = fields.Many2one('namco.mrp.cut.production', string="Parent")
	ratio = fields.Char(string="Tỷ lệ", required=True)
	table_line_ids = fields.One2many('namco.mrp.cut.table.production.line', 'table_id', string="Table lines")
	state = fields.Selection([('draft','Nháp'),
		('confirm','Đã cắt')], default='draft', string="State")
	received_state = fields.Selection([('draft','TT Nhận')], string="R State", default='draft')
	semi_department_line_ids = fields.Many2many('hr.department', 'semi_line_rel', 'semi_id', 'line_id',string="Chuyền đã nhận")
	detail_department_line_ids = fields.Many2many('hr.department', 'detail_line_rel', 'detail_id', 'line_id', string="Chuyền đã nhận")
	semi_specific_department_line_ids = fields.Many2many('hr.department', 'semi_specific_line_rel', 'semi_specific_id', 'line_id',string="Chuyền đã nhận")
	specific_department_line_ids = fields.Many2many('hr.department', 'specific_line_rel', 'specific_id', 'line_id',string="Chuyền đã nhận")
	combine_department_line_ids = fields.Many2many('hr.department', 'combine_line_rel', 'combine_id', 'line_id',string="Chuyền đã nhận")
	consume_department_line_ids = fields.Many2many('hr.department', 'consume_line_rel', 'consume_id', 'line_id',string="Chuyền đã nhận")
	semi_specific_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], default='draft', string="Trạng thái")


	def reverse_semi_specific_table(self):
		self.ensure_one()
		for line in self.table_line_ids:
			line.reverse_semi_specific_quantity()
		self.update({
			'semi_specific_state': 'draft'
			})
		return True

	def confirm_semi_received_quantity(self):
		self.ensure_one()
		products = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('state','in',('confirmed', 'progress')),('order_code','in',self.parent_id.order_code_ids.ids)], order="order_code asc").mapped('product_id').ids
		for line in self.table_line_ids.filtered(lambda l: l.product_id.id in products and l.semi_state != 'confirm'):
			line.with_context(force_update_check=True).update({
				'check_semi_confirm': True
				})
			line.confirm_semi_quantity()
		if self._context.get('department_line_id', False):
			self.update({
				'semi_department_line_ids': [(4,self._context.get('department_line_id', False))]
				})
		return True

	def confirm_detail_received_quantity(self):
		self.ensure_one()
		products = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('state','in',('confirmed', 'progress')),('order_code','in',self.parent_id.order_code_ids.ids)], order="order_code asc").mapped('product_id').ids
		for line in self.table_line_ids.filtered(lambda l: l.product_id.id in products and l.detail_state != 'confirm'):
			line.with_context(force_update_check=True).update({
				'check_detail_confirm': True
				})
			line.confirm_detail_quantity()
		if self._context.get('department_line_id', False):
			self.update({
				'detail_department_line_ids': [(4,self._context.get('department_line_id', False))]
				})
		return True

	def confirm_semi_specific_received_quantity(self):
		self.ensure_one()
		for line in self.table_line_ids:
			line.with_context(force_update_check=True).update({
				'check_semi_specific_confirm': True
				})
			line.confirm_semi_specific_quantity()
		self.update({
			'semi_specific_state': 'confirm',
			})
		return True

	def confirm_specific_received_quantity(self):
		self.ensure_one()
		products = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('state','in',('confirmed', 'progress')),('order_code','in',self.parent_id.order_code_ids.ids)], order="order_code asc").mapped('product_id').ids
		for line in self.table_line_ids.filtered(lambda l: l.product_id.id in products and l.specific_state != 'confirm'):
			line.with_context(force_update_check=True).update({
				'check_specific_confirm': True
				})
			line.confirm_specific_quantity()
		if self._context.get('department_line_id', False):
			self.update({
				'specific_department_line_ids': [(4,self._context.get('department_line_id', False))]
				})
		return True

	def confirm_combine_quantity(self):
		self.ensure_one()
		products = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('state','in',('confirmed', 'progress')),('order_code','in',self.parent_id.order_code_ids.ids)], order="order_code asc").mapped('product_id').ids
		for line in self.table_line_ids.filtered(lambda l: l.product_id.id in products and l.combine_state != 'confirm'):
			line.with_context(force_update_check=True).update({
				'check_combine_confirm': True
				})
			line.confirm_combine_quantity()
		if self._context.get('department_line_id', False):
			self.update({
				'combine_department_line_ids': [(4,self._context.get('department_line_id', False))]
				})
		return True

	def confirm_consume_quantity(self):
		self.ensure_one()
		products = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('state','in',('confirmed', 'progress')),('order_code','in',self.parent_id.order_code_ids.ids)], order="order_code asc").mapped('product_id').ids
		for line in self.table_line_ids.filtered(lambda l: l.product_id.id in products and l.consume_state != 'confirm'):
			line.with_context(force_update_check=True).update({
				'check_consume_confirm': True
				})
			line.confirm_consume_quantity()
		if self._context.get('department_line_id', False):
			self.update({
				'consume_department_line_ids': [(4,self._context.get('department_line_id', False))]
				})
		return True

	def get_real_cut_quantity(self):
		for line in self.env['namco.mrp.cut.table.production.line'].search([]):
			line.update({
				'real_cut_quantity': line.cut_quantity
				})

	def confirm_cut_table(self):
		self.ensure_one()
		self.update({
			'state': 'confirm'
			})
		for line in self.table_line_ids:
			mrp = self.env['mrp.production'].search([('order_code','in',self.parent_id.order_code_ids.ids),('product_id','=',line.product_id.id),('state','in',('confirmed','progress'))],order="order_code asc", limit=1)
			if mrp:
				mrp.with_context(table_line_id=line.id).update({
					'cut_quantity': line.cut_quantity
					})
			else:
				line.update({
					'cut_remain_qty': line.cut_quantity
					})
			line.update({
				'semi_quantity': line.cut_quantity,
				'semi_specific_quantity': line.cut_quantity,
				'detail_quantity': line.cut_quantity,
				'cut_state': 'confirm',
				})
		mrps = self.env['mrp.production'].search([('product_v_color','=',self.parent_id.product_v_color),('order_code','in',self.parent_id.order_code_ids.ids),('state','in',('confirmed','progress'))])
		mrps.write({
			'cut_table_ids': [(4,self.id)]
			})
		return True

	def recompute_ratio(self):
		for record in self:
			for line in record.table_line_ids:
				line.update({
					'cut_quantity': line.ratio*record.layer_qty,
					'real_cut_quantity': line.ratio*record.layer_qty,
					})
		return True


	def write(self, vals):
		res = super(NAMCOMRPCUTTABLEPRODUCTION, self).write(vals)
		if 'layer_qty' in vals:
			for record in self:
				for line in record.table_line_ids:
					line.update({
						'cut_quantity': line.ratio*vals['layer_qty'],
						'real_cut_quantity': line.ratio*vals['layer_qty'],
						})
		if 'ratio' in vals:
			for record in self:
				ratio = vals['ratio']
				product_list = []
				for ratio_split in ratio.split("  "):
					if not record.parent_id.line_ids.filtered(lambda l: l.product_size == ratio_split.split(":")[0]):
						raise ValidationError(_("Không tìm thấy size %s trong lệnh cắt") % ratio_split.split(":")[0])
					else:
						product_id = record.parent_id.line_ids.filtered(lambda l: l.product_size == ratio_split.split(":")[0]).product_id.id
						product_list.append(product_id)
						if record.table_line_ids.filtered(lambda l: l.product_id.id == product_id):
							table_line = record.table_line_ids.filtered(lambda l: l.product_id.id == product_id)[0]
							if table_line.ratio != int(ratio_split.split(":")[1]):
								table_line.update({
									'ratio': int(ratio_split.split(":")[1])
									})
						else:
							record.update({
								'table_line_ids': [(0,0,{
									'product_id': product_id,
									'ratio': int(ratio_split.split(":")[1])
									})]
								})
				for line in record.table_line_ids:
					if line.product_id.id not in product_list:
						line.unlink()
		if 'name' in vals:
			for record in self:
				if vals['name'].isnumeric():
					record.update({
						'sequence': int(vals['name'])
						})
		return res


class NAMCOMRPCUTTABLEPRODUCTIONLINE(models.Model):
	_name = "namco.mrp.cut.table.production.line"

	product_id = fields.Many2one('product.product', string="Style", required=True)
	check_semi_confirm = fields.Boolean(string="Check")
	check_detail_confirm = fields.Boolean(string="Check")
	check_semi_specific_confirm = fields.Boolean(string="Check")
	check_specific_confirm = fields.Boolean(string="Check")
	check_combine_confirm = fields.Boolean(string="Check")
	check_consume_confirm = fields.Boolean(string="Check")
	ratio = fields.Integer(string="Tỷ lệ", default=0)
	product_size = fields.Char(string="Size", related='product_id.product_size', store=True, readonly=True)
	sequence_size = fields.Integer(string="Sequence size", related='product_id.sequence_size', store=True, readonly=True)
	real_cut_quantity = fields.Integer(string="SL thực cắt")
	cut_quantity = fields.Integer(string="Số lượng cắt", default=0)
	cut_remain_qty = fields.Integer(string="Remain Qty", default=0)
	cut_state = fields.Selection([('draft','Nháp'),
		('confirm','Đã cắt')], string="Trạng thái cắt", default='draft', related='table_id.state', store=True, readonly=True)
	semi_quantity = fields.Integer(string="BTP nhận", default=0)
	semi_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái BTP", default='draft', readonly=True)
	detail_quantity = fields.Integer(string="Chi tiết nhận", default=0)
	detail_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái CT", default='draft', readonly=True)
	semi_specific_quantity = fields.Integer(string="SL BTP CD nhận", default=0)
	semi_specific_remain_quantity = fields.Integer(string="Semi specific remain qty", default=0)
	semi_specific_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái BTP CD", default='draft', readonly=True)
	specific_quantity = fields.Integer(string="Cạp đã nhận", default=0)
	specific_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái cạp đã nhận", default='draft', readonly=True)
	combine_quantity = fields.Integer(string="Lên xích", default=0)
	combine_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái lên xích", default='draft', readonly=True)
	consume_quantity = fields.Integer(string="Giật xích", default=0)
	consume_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái giật xích", default='draft', readonly=True)
	table_id = fields.Many2one('namco.mrp.cut.table.production', string="Table")
	parent_id = fields.Many2one('namco.mrp.cut.production', related="table_id.parent_id", store=True, string="Parent")
	line_detail_ids = fields.One2many('mrp.production.table.line', 'table_line_id', string="Line detail")
	semi_specific_detail_line_ids = fields.One2many('mrp.production.semi.specific.table.line', 'table_line_id', string="Semi Specific detail lines")

	def unlink(self):
		for record in self:
			record.update({
				'cut_quantity': 0
				})
		return super(NAMCOMRPCUTTABLEPRODUCTIONLINE, self).unlink()

	def write(self,vals):
		products = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('state','in',('confirmed', 'progress')),('order_code','in',self.table_id.parent_id.order_code_ids.ids)], order="order_code asc").mapped('product_id').ids
		if any(record.product_id.id not in products for record in self) and ('check_detail_confirm' in vals or 'check_semi_confirm' in vals or 'check_specific_confirm' in vals or 'check_combine_confirm' in vals or 'check_consume_confirm' in vals):
			raise ValidationError(_("Không thể xác nhận sản lượng của chuyền khác."))
		if 'cut_quantity' in vals:
			for record in self:
				if record.cut_state == 'confirm':
					diff_qty = vals['cut_quantity'] - record.cut_quantity
					if diff_qty > 0:
						mrp = self.env['mrp.production'].search([('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code asc", limit=1)
						if mrp:
							mrp.with_context(table_line_id=record.id).update({
								'cut_quantity': diff_qty
								})
						else:
							record.update({
								'cut_remain_qty': record.cut_remain_qty + diff_qty
								})
					else:
						remain_diff_qty = -1*diff_qty
						for line in record.line_detail_ids:
							if remain_diff_qty <= 0:
								break
							if line.quantity >= remain_diff_qty:
								line.production_id.with_context(accumulated_qty=False).update({
									'cut_quantity': line.production_id.cut_quantity - remain_diff_qty
									})
								line.update({
									'quantity': line.quantity - remain_diff_qty
									})
								remain_diff_qty = 0
							else:
								line.production_id.with_context(accumulated_qty=False).update({
									'cut_quantity': line.production_id.cut_quantity - line.quantity
									})
								remain_diff_qty -= line.quantity
								line.unlink()
						if remain_diff_qty:
							record.update({
								'cut_remain_qty': record.cut_remain_qty - remain_diff_qty
								})
				if record.semi_specific_state != 'confirm':
					record.update({
						'semi_specific_quantity': vals['cut_quantity']
						})
				if record.semi_state != 'confirm':
					record.update({
						'semi_quantity': vals['cut_quantity']
						})
				if record.detail_state != 'confirm':
					record.update({
						'detail_quantity': vals['cut_quantity']
						})

		res = super(NAMCOMRPCUTTABLEPRODUCTIONLINE, self).write(vals)
		if 'check_semi_confirm' in vals:
			if vals['check_semi_confirm'] and not self._context.get('force_update_check',False):
				for record in self.filtered(lambda l: l.semi_state != 'confirm'):
					record.confirm_semi_quantity()
			if not vals['check_semi_confirm']:
				self.filtered(lambda l: l.semi_state == 'confirm').reverse_semi_quantity()
		if 'check_detail_confirm' in vals:
			if vals.get('check_detail_confirm',False) and not self._context.get('force_update_check',False):
				self.filtered(lambda l: l.detail_state != 'confirm').confirm_detail_quantity()
			if not vals['check_detail_confirm']:
				self.filtered(lambda l: l.detail_state == 'confirm').reverse_detail_quantity()
		if 'check_semi_specific_confirm' in vals:
			if vals.get('check_semi_specific_confirm',False) and not self._context.get('force_update_check',False):
				self.filtered(lambda l: l.semi_specific_state != 'confirm').confirm_semi_specific_quantity()
			if not vals['check_semi_specific_confirm']:
				self.filtered(lambda l: l.semi_specific_state == 'confirm').reverse_semi_specific_quantity()
		if 'check_specific_confirm' in vals:
			if vals.get('check_specific_confirm',False) and not self._context.get('force_update_check',False):
				self.filtered(lambda l: l.specific_state != 'confirm').confirm_specific_quantity()
			if not vals['check_specific_confirm']:
				self.filtered(lambda l: l.specific_state == 'confirm').reverse_specific_quantity()
		if 'check_combine_confirm' in vals:
			if vals.get('check_combine_confirm',False) and not self._context.get('force_update_check',False):
				self.filtered(lambda l: l.combine_state != 'confirm').confirm_combine_quantity()
			if not vals['check_combine_confirm']:
				self.filtered(lambda l: l.combine_state == 'confirm').reverse_combine_quantity()
		if vals.get('check_consume_confirm',False) and not self._context.get('force_update_check',False):
			self.filtered(lambda l: l.consume_state != 'confirm').confirm_consume_quantity()
		return res

	def reverse_semi_quantity(self):
		for record in self.filtered(lambda r: r.semi_state == 'confirm'):
			mrp = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code desc", limit=1)
			if mrp:
				mrp.update({'semi_quantity': -1*record.semi_quantity})
			else:
				raise ValidationError(_("Không tìm thấy lệnh SX tương ứng."))
			record.update({
				'semi_state': 'draft',
				'check_semi_confirm': False
				})
		return True

	def confirm_semi_quantity(self):
		for record in self.filtered(lambda r: r.semi_state != 'confirm'):
			mrp = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code", limit=1)
			if mrp:
				mrp.update({'semi_quantity': record.semi_quantity})
			else:
				raise ValidationError(_("Không tìm thấy lệnh SX tương ứng."))
			record.update({
				'semi_state': 'confirm',
				})
			if record.combine_state != 'confirm':
				record.update({
				'combine_quantity': record.semi_quantity,
				})
		return True

	def confirm_detail_quantity(self):
		for record in self.filtered(lambda r: r.detail_state != 'confirm'):
			mrp = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code", limit=1)
			if mrp:
				mrp.update({'detail_quantity': record.detail_quantity})
			else:
				raise ValidationError(_("Không tìm thấy lệnh SX tương ứng."))
			record.update({
				'detail_state': 'confirm',
				})
			# if record.detail_state != 'confirm':
			# 	record.update({
			# 	'combine_quantity': record.detail_quantity,
			# 	})
		return True

	def reverse_detail_quantity(self):
		for record in self.filtered(lambda r: r.detail_state == 'confirm'):
			mrp = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code desc", limit=1)
			if mrp:
				mrp.update({'detail_quantity': -1*record.detail_quantity})
			else:
				raise ValidationError(_("Không tìm thấy lệnh SX tương ứng."))
			record.update({
				'detail_state': 'draft',
				'check_detail_confirm': False
				})
		return True

	def confirm_semi_specific_quantity(self):
		for record in self.filtered(lambda r: r.semi_specific_state != 'confirm'):
			mrp = self.env['mrp.production'].search([('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code asc", limit=1)
			if mrp:
				mrp.with_context(table_line_id=record.id).update({'semi_special_qty': record.semi_specific_quantity})
			else:
				record.update({'semi_specific_remain_quantity': record.semi_specific_quantity})
			record.update({
				'semi_specific_state': 'confirm',
				})
			if record.specific_state != 'confirm':
				record.update({
				'specific_quantity': record.semi_specific_quantity,
				})
		return True

	def reverse_semi_specific_quantity(self):
		for record in self.filtered(lambda r: r.semi_specific_state == 'confirm'):
			if record.semi_specific_detail_line_ids:
				for line in record.semi_specific_detail_line_ids:
					line.production_id.with_context(accumulated_qty=False).update({
						'semi_special_qty': line.production_id.semi_special_qty - line.quantity
						})
					line.unlink()
				record.update({
					'semi_specific_remain_quantity': 0,
					'semi_specific_state': 'draft',
					'check_semi_specific_confirm': False,
					})
			else:
				mrp = self.env['mrp.production'].search([('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code desc", limit=1)
				if mrp:
					mrp.update({'semi_special_qty': -1*record.semi_specific_quantity})
				record.update({
					'semi_specific_remain_quantity': 0,
					'semi_specific_state': 'draft',
					'check_semi_specific_confirm': False,
					})
		return True

	def confirm_specific_quantity(self):
		for record in self.filtered(lambda r: r.specific_state != 'confirm'):
			mrp = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code", limit=1)
			if mrp:
				mrp.update({'special_quantity': record.specific_quantity})
			else:
				raise ValidationError(_("Không tìm thấy lệnh SX tương ứng."))
			record.update({
				'specific_state': 'confirm',
				})
		return True

	def reverse_specific_quantity(self):
		for record in self.filtered(lambda r: r.specific_state == 'confirm'):
			mrp = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code desc", limit=1)
			if mrp:
				mrp.update({'special_quantity': -1*record.specific_quantity})
			else:
				raise ValidationError(_("Không tìm thấy lệnh SX tương ứng."))
			record.update({
				'specific_state': 'draft',
				'check_specific_confirm': False,
				})
		return True

	def confirm_combine_quantity(self):
		for record in self.filtered(lambda r: r.combine_state != 'confirm'):
			mrp = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code", limit=1)
			if mrp:
				mrp.update({'combine_qty': record.combine_quantity})
			else:
				raise ValidationError(_("Không tìm thấy lệnh SX tương ứng."))
			record.update({
				'combine_state': 'confirm',
				'consume_quantity': record.combine_quantity,
				})
		return True

	def reverse_combine_quantity(self):
		for record in self.filtered(lambda r: r.combine_state == 'confirm'):
			mrp = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code desc", limit=1)
			if mrp:
				mrp.update({'combine_qty': -1*record.combine_quantity})
			else:
				raise ValidationError(_("Không tìm thấy lệnh SX tương ứng."))
			record.update({
				'combine_state': 'draft',
				'check_combine_confirm': False,
				})
		return True

	def confirm_consume_quantity(self):
		for record in self.filtered(lambda r: r.consume_state != 'confirm'):
			mrp = self.env['mrp.production'].search([('department_line_id','=',self._context.get('department_line_id', False)),('order_code','in',record.table_id.parent_id.order_code_ids.ids),('product_id','=',record.product_id.id),('state','in',('confirmed','progress'))],order="order_code", limit=1)
			if mrp:
				mrp.update({'consumed_qty': record.consume_quantity})
			else:
				raise ValidationError(_("Không tìm thấy lệnh SX tương ứng."))
			record.update({
				'consume_state': 'confirm',
				})
		return True

class NAMCOMRPCUTTABLEPRODUCTIONLINEDETAIL(models.Model):
	_name = "namco.mrp.cut.table.production.line.detail"

	line_id = fields.Many2one('namco.mrp.cut.table.production.line', string="Line")
	production_id = fields.Many2one('mrp.production', string="Production")
	product_id = fields.Many2one('product.product', string="Style", required=True)
	product_size = fields.Char(string="Size", related='product_id.product_size', store=True, readonly=True)
	cut_quantity = fields.Integer(string="Số lượng cắt", default=0)
	cut_state = fields.Selection([('draft','Nháp'),
		('confirm','Đã cắt')], string="Trạng thái cắt", default='draft')
	semi_quantity = fields.Integer(string="Số lượng BTP nhận", default=0)
	semi_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái BTP", default='draft')
	semi_specific_quantity = fields.Integer(string="SL BTP CD nhận", default=0)
	semi_specific_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái BTP CD", default='draft')
	specific_quantity = fields.Integer(string="Cạp đã nhận", default=0)
	specific_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái cạp đã nhận", default='draft')
	combine_quantity = fields.Integer(string="Lên xích", default=0)
	combine_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái lên xích", default='draft')
	consume_quantity = fields.Integer(string="Giật xích", default=0)
	consume_state = fields.Selection([('draft','Chưa nhận'),
		('confirm','Đã nhận')], string="Trạng thái lên xích", default='draft')