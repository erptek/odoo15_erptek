# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from datetime import date, datetime, timedelta, time


class NAMCOMRPPRODUCTIONSEW(models.Model):
	_name = "nc.mrp.production.sew"

	# def _default_production_line(self):
	# 	line_id = self.env['hr.department'].search([('parent_id.name','=ilike','Sản Xuất'),('manager_id','=',self.env.user.employee_id.id)],limit=1)
	# 	return line_id and line_id.id or False

	def _production_line_domain(self):
		domain = []
		parent_id = self.env['hr.department'].search_read([('name','=ilike','Sản Xuất')],['id'],limit=1)
		if parent_id:
			domain = [('parent_id','=',parent_id[0]['id'])]
		return domain

	name = fields.Char(strign="Name")
	state = fields.Selection([('draft','Draft'),
		('confirm','Confirmed'),
		('progress','Progress'),
		('done','Done'),
		('cancel',('Canceled'))], default='draft')
	order_code = fields.Char(string="Order Code", required=True)
	oc_qty = fields.Integer(string="OC Quantity")
	# product_id = fields.Many2one('product.product', string="Style", required=True)
	department_line_id = fields.Many2one('hr.department', string="Chuyền", required=True, domain=_production_line_domain)
	department_line_sequence = fields.Integer(string="Line Sequence", related="department_line_id.department_line_sequence", store=True)
	start_date = fields.Date(string="Start Date", default=fields.Date.today())
	end_date = fields.Date(string="End Date", default=fields.Date.today())
	cut_quantity = fields.Integer(string="Số lượng đã cắt")
	cut_update_time = fields.Datetime(string="Cut updated")
	semi_quantity = fields.Integer(string="BTP đã nhận")
	semi_update_time = fields.Datetime(string="Semi updated")
	bottle_neck_qty = fields.Integer(string="SL CĐ cổ chai")
	bottle_update_time = fields.Datetime(string="Bottle updated")
	worker_count = fields.Integer(string="Số lao động")
	qc_pass = fields.Integer(string="QC Pass")
	qc_update_time = fields.Datetime(string="QC updated")
	qc_failed = fields.Integer(string="QC Failed")
	date_target = fields.Integer(string="Target ngày")
	start_production_date = fields.Datetime(string="Latest start")
	cut_record_details = fields.One2many('mrp.production.cut.record.detail', 'production_id', string="Cut detail")
	cut_record_dailys = fields.One2many('mrp.production.cut.record.daily', 'production_id', string="Cut daily")
	semi_record_details = fields.One2many('mrp.production.semi.record.detail', 'production_id', string="Semi detail")
	semi_record_dailys = fields.One2many('mrp.production.semi.record.daily', 'production_id', string="Semi daily")



class NAMCOMRPPRODUCTIONSEWLINE(models.Model):
	_name = "nc.mrp.production.sew.line"

	product_id = fields.Many2one('product.product', string="Style", required=True)
	color = fields.Many2one('nc.product.color', string="Color", required=True)
	size = fields.Many2one('nc.product.size', string="Size", required=True)
	quantity = fields.Integer(string="Quantity")
	cut_qty = fields.Integer(string="Cut Qty")
	semi_received_qty = fields.Integer(string="Semi Received")
	scrap_qty = fields.Integer(string="Scrap Qty")
	scrap_received_qty = fields.Integer(string="Scrap Received")
	uom_id = fields.Many2one('uom.uom', string="UOM")
	sew_id = fields.Many2one('nc.mrp.production.sew', string="Parent")
	order_code = fields.Char(string="OC", related="sew_id.order_code", store=True, readonly=True)
	department_line_id = fields.Many2one('hr.department', string="Line", related="sew_id.department_line_id", store=True, readonly=True)



class NAMCOPRODUCTCOLOR(models.Model):
	_name =  "nc.product.color"

	name = fields.Char(string="Color", required=True)

class NAMCOPRODUCTSIZE(models.Model):
	_name =  "nc.product.size"

	name = fields.Char(string="Size", required=True)
	