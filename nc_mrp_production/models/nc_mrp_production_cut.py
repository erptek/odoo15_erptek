# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from datetime import date, datetime, timedelta, time


class NAMCOMRPPRODUCTIONCUT(models.Model):
	_name = "nc.mrp.production.cut"

	name = fields.Char(string="Name")
	process_order = fields.Char(string="Process Order")
	state = fields.Selection([('draft','Draft'),
		('confirm','Confirmed'),
		('progress','Progress'),
		('done','Done'),
		('cancel',('Canceled'))], default='draft')
	order_code = fields.Char(string="Order Code", required=True)
	start_date = fields.Date(string="Start Date", default=fields.Date.today())
	end_date = fields.Date(string="End Date", default=fields.Date.today())
	product_id = fields.Many2one('product.product', string="Style", required=True)



class NAMCOMRPPRODUCTIONCUTLINE(models.Model):
	_name = "nc.mrp.production.cut.line"

	product_id = fields.Many2one('product.product', string="Style", required=True)
	color = fields.Many2one('nc.product.color', string="Color", required=True)
	size = fields.Many2one('nc.product.size', string="Size", required=True)
	quantity = fields.Integer(string="Quantity")
	done_quantity = fields.Integer(string="Qty Done")
	uom_id = fields.May2one('uom.uom', string="UOM")

