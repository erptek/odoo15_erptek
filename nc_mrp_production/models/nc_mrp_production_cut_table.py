# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from datetime import date, datetime, timedelta, time


class NAMCOMRPPRODUCTIONCUTTABLE(models.Model):
	_name = "nc.mrp.production.cut.table"

	product_id = fields.