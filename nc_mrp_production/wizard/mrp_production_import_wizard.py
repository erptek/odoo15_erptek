# -*- coding: utf-8 -*-
from datetime import date

from dateutil.relativedelta import *
from odoo.exceptions import ValidationError
from odoo import models, fields
from odoo.osv import expression
import base64
import xlrd

class MRPPRODUCTIONIMPORTWIZARD(models.TransientModel):
	_name = 'mrp.production.import.wizard'

	file = fields.Binary(string="Planning Document", required=True)
	filename = fields.Char('File Name')


	def action_confirm(self):
		wb = xlrd.open_workbook(file_contents=base64.decodestring(self.file))
		data = []
		for sheet in wb.sheets():
			workcenter_col = False
			unit_price_col = False
			time_col = False
			for i in range(2,sheet.ncols):
				if sheet.cell(13,i).value == 'Nhóm CV':
					workcenter_col = i
				if sheet.cell(13,i).value == 'Đơn giá duyệt theo % hỗ trợ TT':
					unit_price_col = i
				if sheet.cell(13,i).value == 'TG':
					time_col = i
			workcenter_id = False
			for row in range(15,sheet.nrows):
				if type(sheet.cell(row,0).value) != int and type(sheet.cell(row,0).value) != float:
					break
				if sheet.cell(row,workcenter_col).value:
					workcenter = self.env['mrp.workcenter'].search_read([('name','=',sheet.cell(row,workcenter_col).value)],['id'],limit=1)
					if workcenter:
						workcenter_id = workcenter[0]['id']
					else:
						workcenter_id = self.env['mrp.workcenter'].create({'name': sheet.cell(row,workcenter_col).value}).id
				data.append({
					'name': sheet.cell(row,1).value,
					'step_sequence': int(sheet.cell(row,0).value),
					'workcenter_id': workcenter_id,
					'unit_price': float(sheet.cell(row,unit_price_col).value or 0.0),
					'time_cycle': float(sheet.cell(row,time_col).value/60 or 0.0),
					'routing_id': active_id,
					})
			break
		self.env['mrp.routing.workcenter'].create(data)

		return True