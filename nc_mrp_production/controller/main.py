# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo.http import content_disposition, request
from odoo.addons.web.controllers.main import ExcelExport
from datetime import date, datetime, timedelta, time
from odoo import http, models, fields, _


class DBCRUD(http.Controller):

	@http.route('/production/mainboard', type='json', auth='none', methods=['POST'], cors='*')	
	def search_read(self, db, login, password, model, fields=None, domain=None):
		# body = self.process_form(kwargs)
		request.session.authenticate(db, login, password)
		result = []
		if not domain:
			productions = request.env[model].search_read([('state','in',('progress','confirmed'))], \
				['order_code','department_line_id','product_id','date_target','product_qty','qc_pass', \
				'qc_failed', 'cut_quantity', 'semi_quantity', 'bottle_neck_qty', 'worker_count', 'cut_update_time', \
				'semi_update_time', 'bottle_update_time', 'qc_update_time', 'start_production_date'], \
				order='department_line_sequence asc')
			
			result = []

			for production in productions:
				druation_time = False
				if production.get('start_production_date', False):
					druation_time = datetime.now() - production.get('start_production_date',datetime.now())
				started_time = False
				if production.get('bottle_update_time', False) and production.get('start_production_date', False):
					started_time = production.get('bottle_update_time',datetime.now()) - production.get('start_production_date',datetime.now())
				result.append({
					"id": production.get("id"),
					"line_name": production.get('department_line_id')[1],
					"product": production.get('product_id')[1],
					"date_target": production.get('date_target', 0),
					"worker_count": production.get('worker_count',0),
					"qc_pass": production.get('qc_pass',0),
					"qc_update_time": str(production.get('qc_update_time') and production.get('qc_update_time').hour + 7 or "00") + ":" + str(production.get('qc_update_time') and production.get('qc_update_time').minute or "00"),
					"cut_quantity": production.get('cut_quantity',0),
					"cut_update_time": str(production.get('cut_update_time') and production.get('cut_update_time').hour + 7 or "00") + ":" + str(production.get('cut_update_time') and production.get('cut_update_time').minute or "00"),
					"semi_quantity": production.get('semi_quantity',0),
					"semi_update_time": str(production.get('semi_update_time') and production.get('semi_update_time').hour + 7 or "00") + ":" + str(production.get('semi_update_time') and production.get('semi_update_time').minute or "00"),
					"bottle_neck_qty": production.get('bottle_neck_qty',0),
					"bottle_update_time": str(production.get('bottle_update_time') and production.get('bottle_update_time').hour + 7 or "00") + ":" + str(production.get('bottle_update_time') and production.get('bottle_update_time').minute or "00"),
					"druation_time": druation_time and str(druation_time.seconds//3600) + ":" + str((druation_time.seconds%3600)//60) or "00:00",
					"real_productivity": started_time and int(production.get('bottle_neck_qty',0)*3600/(started_time.seconds or 1)) or 0,
					"rate_productivity": started_time and int(production.get('bottle_neck_qty',0)*3600/(started_time.seconds or 1)/(production.get('date_target', 0)/9)*100) or 0,
				})
		else:
			first_production = request.env[model].search_read([('state','in',('progress','confirmed')),('department_line_id.name','=',domain and domain[0] or '')], \
				['order_code','department_line_id','product_id','date_target','product_qty','qc_pass', \
				'qc_failed', 'cut_quantity', 'semi_quantity', 'bottle_neck_qty', 'worker_count', 'cut_update_time', \
				'semi_update_time', 'bottle_update_time', 'qc_update_time', 'start_production_date'],limit=1)
			second_production = request.env[model].search_read([('state','in',('progress','confirmed')),('department_line_id.name','=',domain and domain[1] or '')], \
				['order_code','department_line_id','product_id','date_target','product_qty','qc_pass', \
				'qc_failed', 'cut_quantity', 'semi_quantity', 'bottle_neck_qty', 'worker_count', 'cut_update_time', \
				'semi_update_time', 'bottle_update_time', 'qc_update_time', 'start_production_date'],limit=1)
			if first_production and first_production[0]:
				druation_time = False
				if first_production[0].get('start_production_date', False):
					druation_time = datetime.now() - first_production[0].get('start_production_date',datetime.now())
				started_time = False
				if first_production[0].get('bottle_update_time', False) and first_production[0].get('start_production_date', False):
					started_time = first_production[0].get('bottle_update_time',datetime.now()) - first_production[0].get('start_production_date',datetime.now())
				result.append({
					"id": first_production[0].get("id"),
					"line_name": first_production[0].get('department_line_id')[1],
					"product": first_production[0].get('product_id')[1],
					"date_target": first_production[0].get('date_target', 0),
					"worker_count": first_production[0].get('worker_count',0),
					"qc_pass": first_production[0].get('qc_pass',0),
					"qc_update_time": str(first_production[0].get('qc_update_time') and first_production[0].get('qc_update_time').hour + 7 or "00") + ":" + str(first_production[0].get('qc_update_time') and first_production[0].get('qc_update_time').minute or "00"),
					"cut_quantity": first_production[0].get('cut_quantity',0),
					"cut_update_time": str(first_production[0].get('cut_update_time') and first_production[0].get('cut_update_time').hour + 7 or "00") + ":" + str(first_production[0].get('cut_update_time') and first_production[0].get('cut_update_time').minute or "00"),
					"semi_quantity": first_production[0].get('semi_quantity',0),
					"semi_update_time": str(first_production[0].get('semi_update_time') and first_production[0].get('semi_update_time').hour + 7 or "00") + ":" + str(first_production[0].get('semi_update_time') and first_production[0].get('semi_update_time').minute or "00"),
					"bottle_neck_qty": first_production[0].get('bottle_neck_qty',0),
					"bottle_update_time": str(first_production[0].get('bottle_update_time') and first_production[0].get('bottle_update_time').hour + 7 or "00") + ":" + str(first_production[0].get('bottle_update_time') and first_production[0].get('bottle_update_time').minute or "00"),
					"druation_time": druation_time and str(druation_time.seconds//3600) + ":" + str((druation_time.seconds%3600)//60) or "00:00",
					"real_productivity": started_time and int(first_production[0].get('bottle_neck_qty',0)*3600/(started_time.seconds or 1)) or 0,
					"rate_productivity": started_time and int(first_production[0].get('bottle_neck_qty',0)*3600/(started_time.seconds or 1)/(first_production[0].get('date_target', 0)/9)*100) or 0,
				})
			if second_production and second_production[0]:
				druation_time = False
				if second_production[0].get('start_production_date', False):
					druation_time = datetime.now() - second_production[0].get('start_production_date',datetime.now())
				started_time = False
				if second_production[0].get('bottle_update_time', False) and second_production[0].get('start_production_date', False):
					started_time = second_production[0].get('bottle_update_time') - second_production[0].get('start_production_date')
				result.append({
					"id": second_production[0].get("id"),
					"line_name": second_production[0].get('department_line_id')[1],
					"product": second_production[0].get('product_id')[1],
					"date_target": second_production[0].get('date_target', 0),
					"worker_count": second_production[0].get('worker_count',0),
					"qc_pass": second_production[0].get('qc_pass',0),
					"qc_update_time": str(second_production[0].get('qc_update_time') and second_production[0].get('qc_update_time').hour + 7 or "00") + ":" + str(second_production[0].get('qc_update_time') and second_production[0].get('qc_update_time').minute or "00"),
					"cut_quantity": second_production[0].get('cut_quantity',0),
					"cut_update_time": str(second_production[0].get('cut_update_time') and second_production[0].get('cut_update_time').hour + 7 or "00") + ":" + str(second_production[0].get('cut_update_time') and second_production[0].get('cut_update_time').minute or "00"),
					"semi_quantity": second_production[0].get('semi_quantity',0),
					"semi_update_time": str(second_production[0].get('semi_update_time') and second_production[0].get('semi_update_time').hour + 7 or "00") + ":" + str(second_production[0].get('semi_update_time') and second_production[0].get('semi_update_time').minute or "00"),
					"bottle_neck_qty": second_production[0].get('bottle_neck_qty',0),
					"bottle_update_time": str(second_production[0].get('bottle_update_time') and second_production[0].get('bottle_update_time').hour + 7 or "00") + ":" + str(second_production[0].get('bottle_update_time') and second_production[0].get('bottle_update_time').minute or "00"),
					"druation_time": druation_time and str(druation_time.seconds//3600) + ":" + str((druation_time.seconds%3600)//60) or "00:00",
					"real_productivity": started_time and int(second_production[0].get('bottle_neck_qty',0)*3600/(started_time.seconds or 1)) or 0,
					"rate_productivity": started_time and int(second_production[0].get('bottle_neck_qty',0)*3600/(started_time.seconds or 1)/(second_production[0].get('date_target', 0)/9)*100) or 0,
				})

		return result