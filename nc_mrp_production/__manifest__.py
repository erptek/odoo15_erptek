# -*- coding: utf-8 -*-
{
    'name': "Nam&Co MRP Production",
    'summary': """.
    """,
    'author': "Hoan",
    'category': 'MRP',
    'version': '15.0.1.1',
    'license': 'LGPL-3',
    'depends': ['mrp', 'hr', 'mrp_workorder'],
    'data': [
        # 'security/ir.model.access.csv',
        # 'security/mrp_security.xml',
        'views/mrp_production_views.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
