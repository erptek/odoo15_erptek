odoo.define('nc_mrp_report.nc_mrp_report', function (require) {
'use strict';

var core = require('web.core');
var accountReportsWidget = require('account_reports.account_report');
var RelationalFields = require('web.relational_fields');
var StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');
var Widget = require('web.Widget');
var _t = core._t;


var M2MFiltersCustomize = Widget.extend(StandaloneFieldManagerMixin, {
	/**
	 * @constructor
	 * @param {Object} fields
	 */
	init: function (parent, fields) {
		this._super.apply(this, arguments);
		StandaloneFieldManagerMixin.init.call(this);
		this.fields = fields;
		this.widgets = {};
	},
	/**
	 * @override
	 */
	willStart: function () {
		var self = this;
		var defs = [this._super.apply(this, arguments)];
		_.each(this.fields, function (field, fieldName) {
			defs.push(self._makeM2MWidget(field, fieldName));
		});
		return $.when.apply($, defs);
	},
	/**
	 * @override
	 */
	start: function () {
		var self = this;
		_.each(this.fields, function (field, fieldName) {
			self.$el.append($('<p/>', {style: 'font-weight:bold;'}).text(field.label));
			self.widgets[fieldName].appendTo(self.$el);
		});
		return this._super.apply(this, arguments);
	},

	//--------------------------------------------------------------------------
	// Private
	//--------------------------------------------------------------------------

	/**
	 * This method will be called whenever a field value has changed and has
	 * been confirmed by the model.
	 *
	 * @private
	 * @override
	 * @returns {Deferred}
	 */
	_confirmChange: function () {
		var self = this;
		var result = StandaloneFieldManagerMixin._confirmChange.apply(this, arguments);
		var data = {};
		_.each(this.fields, function (filter, fieldName) {
			data[fieldName] = self.widgets[fieldName].value.res_ids;
		});
		this.trigger_up('value_changed', data);
		return result;
	},
	/**
	 * This method will create a record and initialize M2M widget.
	 *
	 * @private
	 * @param {Object} fieldInfo
	 * @param {string} fieldName
	 * @returns {Deferred}
	 */
	_makeM2MWidget: function (fieldInfo, fieldName) {
		var self = this;
		var options = {};
		options[fieldName] = {
			options: {
				no_create_edit: true,
				no_create: true,
			}
		};
		return this.model.makeRecord(fieldInfo.modelName, [{
			fields: [{
				name: 'id',
				type: 'integer',
			}, {
				name: 'display_name',
				type: 'char',
			}],
			name: fieldName,
			relation: fieldInfo.modelName,
			type: 'many2many',
			value: fieldInfo.value,
			domain: fieldInfo.domain,
		}], options).then(function (recordID) {
			self.widgets[fieldName] = new RelationalFields.FieldMany2ManyTags(self,
				fieldName,
				self.model.get(recordID),
				{mode: 'edit',}
			);
			self._registerWidget(recordID, fieldName, self.widgets[fieldName]);
		});
	},
});


accountReportsWidget.include({
	init: function(parent, action) {
		this.report_model = action.context.model;
        if (this.report_model === undefined) {
            this.report_model = 'nc.mrp.report';
        }
        // this.report_model = 'nc.mrp.report';
        return this._super.apply(this, arguments);
    },

	custom_events: {
		'value_changed': function(ev) {
			var self = this;
			if (ev.data.partner_ids || ev.data.partner_categories){
				self.report_options.partner_ids = ev.data.partner_ids;
				self.report_options.partner_categories = ev.data.partner_categories;
				return self.reload().then(function () {
					self.$searchview_buttons.find('.account_partner_filter').click();
				});
			}
			if (ev.data.account_ids) {
				self.report_options.account_ids = ev.data.account_ids;
				return self.reload().then(function () {
					self.$searchview_buttons.find('.account_account_filter').click();
				});
			}
			if (ev.data.product_ids) {
				self.report_options.product_ids = ev.data.product_ids;
				return self.reload().then(function () {
					self.$searchview_buttons.find('.account_product_filter').click();
				});
			}
			if (ev.data.cag_ids) {
				self.report_options.cag_ids = ev.data.cag_ids;
				return self.reload().then(function () {
					self.$searchview_buttons.find('.account_cag_filter').click();
				});
			}
			if (ev.data.location_ids) {
				self.report_options.location_ids = ev.data.location_ids;
				return self.reload().then(function () {
					self.$searchview_buttons.find('.account_location_filter').click();
				});
			}
		},
	},

	load_more: function (ev) {
        var $line = $(ev.target).parents('td');
        var id = $line.data('id');
        var offset = $line.data('offset') || 0;
        var progress = $line.data('progress') || 0;
        var exchange_progress = $line.data('exchangeprogress') || 0;
        var options = _.extend({}, this.report_options, {lines_offset: offset, lines_progress: progress, exchange_progress: exchange_progress});
        var self = this;
        this._rpc({
                model: this.report_model,
                method: 'get_html',
                args: [this.financial_id, options, id],
                context: this.odoo_context,
            })
            .then(function (result){
                var $tr = $line.parents('.o_account_reports_load_more');
                $tr.after(result);
                $tr.remove();
                self._add_line_classes();
            });
    },

	render_searchview_buttons: function() {
		this._super.apply(this, arguments);
		var self = this;
		console.log(this.M2MFiltersCustomize);
		 // partner filter
		if (this.report_options.partner) {
			//console.warn(this.report_options);
			if (!this.M2MFiltersCustomize) {
				var fields = {};
				if ('partner_ids' in this.report_options) {
					//console.log(this.report_options.partner_domain);
					fields['partner_ids'] = {
						label: _t('Partners'),
						modelName: 'res.partner',
						value: this.report_options.partner_ids.map(Number),
						domain: [],
					};
					if (this.report_options.partner_domain) {
						fields['partner_ids']['domain'] = this.report_options.partner_domain
					}
					
				}
				if ('partner_categories' in this.report_options) {
					fields['partner_categories'] = {
						label: _t('Tags'),
						modelName: 'res.partner.category',
						value: this.report_options.partner_categories.map(Number),
					};
				}
				if (!_.isEmpty(fields)) {
					this.M2MFiltersCustomize = new M2MFiltersCustomize(this, fields);
					this.M2MFiltersCustomize.appendTo(this.$searchview_buttons.find('.js_account_partner_m2m').empty());
				}
			} else {
				this.$searchview_buttons.find('.js_account_partner_m2m').empty().append(this.M2MFiltersCustomize.$el);
			}
		}
		//product filter
		if (this.report_options.stock) {
			if (!this.M2MFiltersCustomizeStock) {
				var fields = {};
				if ('product_ids' in this.report_options) {
					fields['product_ids'] = {
						label: _t('Products'),
						modelName: 'product.product',
						value: this.report_options.product_ids.map(Number),
						domain: [],
					};
				}
				if (!_.isEmpty(fields)) {
					this.M2MFiltersCustomizeStock = new M2MFiltersCustomize(this, fields);
					this.M2MFiltersCustomizeStock.appendTo(this.$searchview_buttons.find('.js_account_product_m2m').empty());
				}
			} 
			else {
				this.$searchview_buttons.find('.js_account_product_m2m').empty().append(this.M2MFiltersCustomizeStock.$el);
			}
		}
		//product_category filter
		if (this.report_options.cag) {
			if (!this.M2MFiltersCustomizeCag) {
				var fields = {};
				if ('cag_ids' in this.report_options) {
					fields['cag_ids'] = {
						label: _t('Categories'),
						modelName: 'product.category',
						value: this.report_options.cag_ids.map(Number),
						domain: [],
					};
				}
				if (!_.isEmpty(fields)) {
					this.M2MFiltersCustomizeCag = new M2MFiltersCustomize(this, fields);
					this.M2MFiltersCustomizeCag.appendTo(this.$searchview_buttons.find('.js_account_cag_m2m').empty());
				}
			} 
			else {
				this.$searchview_buttons.find('.js_account_cag_m2m').empty().append(this.M2MFiltersCustomizeCag.$el);
			}
		}
		//location filter
		if (this.report_options.location) {
			if (!this.M2MFiltersCustomizeLocation) {
				var fields = {};
				if ('location_ids' in this.report_options) {
					fields['location_ids'] = {
						label: _t('Locations'),
						modelName: 'stock.location',
						value: this.report_options.location_ids.map(Number),
						domain: [],
					};
				}
				if (!_.isEmpty(fields)) {
					this.M2MFiltersCustomizeLocation = new M2MFiltersCustomize(this, fields);
					this.M2MFiltersCustomizeLocation.appendTo(this.$searchview_buttons.find('.js_account_location_m2m').empty());
				}
			} 
			else {
				this.$searchview_buttons.find('.js_account_location_m2m').empty().append(this.M2MFiltersCustomizeLocation.$el);
			}
		}
	},
});

return accountReportsWidget;

});