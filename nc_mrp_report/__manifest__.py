# -*- coding: utf-8 -*-
{
	'name': 'Nam&Co MRP Report',
	'version': '1.0',
	'author': 'DV Co. Ltd',
	'category': 'MRP',
	'summary': 'MRP Report',
	'description': """
This is the module to manage the stock report for Vietnam in Odoo.
=========================================================================

	""",
	'depends': ['nc_mrp_payroll','account_reports'],
	'data': [
		'views/report_template_view.xml',
		'views/search_template_view.xml',
		'data/account_report_data.xml',
		'views/menu_view.xml',
	],
	'installable': True,
	'auto_install': False,
}
