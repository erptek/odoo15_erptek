# -*- coding: utf-8 -*-

from odoo import models, api, _, fields
from odoo.tools import float_is_zero
from odoo.tools.misc import format_date
from datetime import datetime, timedelta
from odoo.tools.misc import formatLang

class MRPQUANTITYREPORT(models.AbstractModel):
	_inherit = "nc.mrp.report"
	_name = "mrp.quantity.report"
	_description = "Tình hình sản xuất"

	filter_date = {'date_from': '', 'date_to': '', 'filter': 'this_month'}

	def _get_templates(self):
		templates = super(MRPQUANTITYREPORT, self)._get_templates()
		templates['line_template'] = 'nc_mrp_report.line_template_report_vn_add_view_journal_shortcut'
		return templates

	def _get_columns_name(self, options):
		columns = [
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			
		]
		return columns

	def _get_super_columns(self, options):
		header_lines = []
		header_lines.append({
			'id': 'header',
			'name': _('Mã hàng'),
			'level': 2,
			'class': 'text-center o_account_vn_report_border',
			'columns': [{'name': 'Đơn hàng'},
						{'name': 'Số lượng đơn hàng'},
						{'name': 'Số lượng chỉ định'},
						{'name': 'Số lượng đã SX'},
						{'name': 'Tiến trình'},
						{'name': 'Lương SP'},
						],
			'span_style': 'margin:0',
			})

		columns = self._get_columns_name(options)
		return {'header': header_lines, 'x_offset': 3, 'y_offset': 0, 'total_col': len(columns)}

	@api.model
	def _get_report_name(self):
		return _("Tình hình sản xuất")

	def _get_report_template(self):
		res = b''
		return res

	def _get_report_template_name(self):
		res = ''
		return res

	def _do_query_group_by_production(self, options, line_id):
		context = dict(self._context or {})
		
		date_to = context.get('date_to',False) or options['date']['date_to']
		date_from = options['date']['date_from']
		params = [date_to, date_from]

		query = '''
			SELECT mwe.production_id,
				SUM(mwe.quantity*mwe.step_price) AS amount
				
			FROM mrp_workorder_employee mwe
			WHERE mwe.date_record <= %s
			AND mwe.date_record >= %s
			GROUP BY mwe.production_id
		'''
		self._cr.execute(query, params)
		query_res = self._cr.dictfetchall()
		return dict((res['production_id'], res) for res in query_res)


	@api.model
	def _get_lines(self, options, line_id=None):
		context = self.env.context
		lines = []
		if not context.get('print_mode') and not line_id or context.get('pdf',False):
			headers = self._get_super_columns(options)
			if headers:
				for header in headers['header']:
					lines.append(header)
		grouped_mrps = self._do_query_group_by_production(options, line_id)
		tot_order_qty, tot_product_qty, tot_produced_qty, tot_amount = 0.0, 0.0, 0.0, 0.0
		for key,value in grouped_mrps.items():
			mrp_info = self.env['mrp.production'].search_read([('id','=',key)],['product_id','product_qty','order_quantity','order_code','produced_qty'],limit=1)
			raw_columns = [
				mrp_info[0]['order_code'],
				formatLang(self.env,mrp_info[0]['order_quantity'],digits=0),
				formatLang(self.env,mrp_info[0]['product_qty'],digits=0),
				formatLang(self.env,mrp_info[0]['produced_qty'],digits=0),
				mrp_info[0]['product_qty'] > 0.0 and str(formatLang(self.env,round(mrp_info[0]['produced_qty']/mrp_info[0]['product_qty']*100,0),digits=0)) + '%' or '',
				self.format_value(value['amount'])]
			columns = [{'name': v} for v in raw_columns]
			columns[0]['span_style'] = 'margin:0;text-align:left;font-weight:bold'
			for x in range(1,6):
				columns[x]['money_style'] = 'white-space: nowrap;text-align:right;font-weight:400!important;'
			lines.append({
				'class': 'o_account_vn_report_border',
				'id': 'mrp_' + str(key),
				'name': _(mrp_info[0]['product_id'][1]),
				'columns': columns,
				'level': 3,
				'span_style': 'margin-left:5px;font-weight:bold',
			})
			tot_order_qty += mrp_info[0]['order_quantity']
			tot_product_qty += mrp_info[0]['product_qty']
			tot_produced_qty += mrp_info[0]['produced_qty']
			tot_amount += value['amount']
			
		raw_columns = [
		formatLang(self.env,tot_order_qty,digits=0),
		formatLang(self.env,tot_product_qty,digits=0),
		formatLang(self.env,tot_produced_qty,digits=0),
		'',
		self.format_value(tot_amount),
		]
		columns = [{'name': v} for v in raw_columns]
		for x in range(0,5):
			columns[x]['money_style'] = 'white-space: nowrap;text-align:right;'
		lines.append({
			'id': 'sum_categ',
			'class': 'total text-center o_account_vn_report_border',
			'name': _("Tổng"),
			'columns': columns,
			'level': 2,
			'colspan': 2,
			'parent_header': True,
		})

		return lines
