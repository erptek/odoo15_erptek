# -*- coding: utf-8 -*-

from odoo import models, api, _, fields
from odoo.tools import float_is_zero
from odoo.tools.misc import format_date
from datetime import datetime, timedelta
from odoo.tools.misc import formatLang

class MRPPRODUCTIVITYREPORT(models.AbstractModel):
	_inherit = "nc.mrp.report"
	_name = "mrp.productivity.report"
	_description = "Theo dõi năng suất"

	filter_date = {'date': '', 'filter': 'today'}

	def _get_templates(self):
		templates = super(MRPPRODUCTIVITYREPORT, self)._get_templates()
		templates['line_template'] = 'nc_mrp_report.line_template_report_vn_add_view_journal_shortcut'
		return templates

	def _get_columns_name(self, options):
		columns = [
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			{'name': _(''), 'style': 'white-space: normal; word-wrap: break-word; width:8%; border: 1px solid black; padding: 0 8px;text-align: center;'},
			
		]
		return columns

	def _get_super_columns(self, options):
		header_lines = []
		header_lines.append({
			'id': 'header',
			'name': _('Mã hàng'),
			'level': 2,
			'class': 'text-center o_account_vn_report_border',
			'columns': [{'name': 'Đơn hàng'},
						{'name': 'Số lượng đơn hàng'},
						{'name': 'Số lượng chỉ định'},
						{'name': 'Năng suất TB'},
						{'name': 'Năng suất thực'},
						{'name': 'Số lượng đã SX'},
						],
			'span_style': 'margin:0',
			})

		columns = self._get_columns_name(options)
		return {'header': header_lines, 'x_offset': 3, 'y_offset': 0, 'total_col': len(columns)}

	@api.model
	def _get_report_name(self):
		return _("Theo dõi năng suất")

	def _get_report_template(self):
		res = b''
		return res

	def _get_report_template_name(self):
		res = ''
		return res

	def _do_query_group_by_production(self, options, line_id):
		context = dict(self._context or {})
		
		date = options.get('date')
		params = [date['date']]

		query = '''
			SELECT md.production_id,
				SUM(md.record_quantity) AS quantity
				
			FROM mrp_production_daily_record md
			WHERE md.date_record = %s
			GROUP BY md.production_id
		'''
		self._cr.execute(query, params)
		query_res = self._cr.dictfetchall()
		return dict((res['production_id'], res) for res in query_res)


	@api.model
	def _get_lines(self, options, line_id=None):
		date_r = options.get('date')
		date = date_r['date']
		context = self.env.context
		lines = []
		if not context.get('print_mode') and not line_id or context.get('pdf',False):
			headers = self._get_super_columns(options)
			if headers:
				for header in headers['header']:
					lines.append(header)
		grouped_mrps = self._do_query_group_by_production(options, line_id)
		tot_order_qty, tot_product_qty, tot_produced_qty, tot_amount = 0.0, 0.0, 0.0, 0.0
		for key,value in grouped_mrps.items():
			mrp_info = self.env['mrp.production'].search_read([('id','=',key)],['product_id','product_qty','order_quantity','order_code','produced_qty','productivity_expect'],limit=1)
			raw_columns = [
				mrp_info[0]['order_code'],
				formatLang(self.env,mrp_info[0]['order_quantity'],digits=0),
				formatLang(self.env,mrp_info[0]['product_qty'],digits=0),
				formatLang(self.env,mrp_info[0]['productivity_expect'],digits=0),
				formatLang(self.env,value['quantity'],digits=0),
				formatLang(self.env,mrp_info[0]['produced_qty'],digits=0)]
			columns = [{'name': v} for v in raw_columns]
			columns[0]['span_style'] = 'margin:0;text-align:left;font-weight:bold'
			for x in range(1,6):
				columns[x]['money_style'] = 'white-space: nowrap;text-align:right;font-weight:400!important;'
			curr_qty = sum(l['record_quantity'] for l in self.env['mrp.production.daily.record'].search_read([('date_record','<=',date),('production_id','=',key)],['record_quantity']) or 0.0)
			columns[4]['money_style'] += (value['quantity'] < mrp_info[0]['productivity_expect'] and curr_qty < mrp_info[0]['product_qty']) and ';color:red' or ''
			lines.append({
				'class': 'o_account_vn_report_border',
				'id': 'mrp_' + str(key),
				'name': _(mrp_info[0]['product_id'][1]),
				'columns': columns,
				'level': 3,
				'span_style': 'margin-left:5px;font-weight:bold',
			})

		return lines
