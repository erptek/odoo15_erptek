# -*- coding: utf-8 -*-

import io
import math
import numbers
import decimal
import collections

try:
	from odoo.tools.misc import xlsxwriter
except ImportError:
	import xlsxwriter

from odoo import models, api, fields, _
from datetime import datetime, timedelta
from odoo.tools.misc import formatLang
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, pycompat, config, date_utils
from odoo.exceptions import UserError, ValidationError

class NCMRPREPORT(models.AbstractModel):
	_inherit = "account.report"
	_name = 'nc.mrp.report'

	
	filter_account = None
	filter_asset = None
	filter_bank_account = None
	filter_currency = None
	filter_stock = None
	filter_partner_domain = None
	filter_payroll_category = None
	filter_payslip = None
	filter_multi_company = None
	filter_warehouse = None
	filter_sign = None
	filter_user = None
	filter_cag = None
	filter_location = None


	def _get_templates(self):
		templates = super(NCMRPREPORT, self)._get_templates()
		templates['main_template'] = 'nc_mrp_report.account_vn_main_template'
		return templates

	def _get_report_template(self):
		return b''

	def _get_report_date_range(self, options):
		res = b''
		if options.get('date',False):
			if options['date'].get('date_from',False) and options['date'].get('date_to',False):
				dt_from = '/'.join(reversed(options['date'].get('date_from').split('-')))
				dt_to = '/'.join(reversed(options['date'].get('date_to').split('-')))
				res = bytes('<h5>Từ ngày: %s đến ngày: %s</h5>' % (dt_from, dt_to), 'utf-8')
			else:
				date = '/'.join(reversed(options['date'].get('date').split('-')))
				res = bytes('<h5>Ngày: %s</h5>' % (date), 'utf-8')
		return res

	def _get_report_currency(self, options):
		res = b''
		if options.get('currency',False):
			currency_ids = [c.get('id') for c in options.get('currency') if c.get('selected', False)]
			if not currency_ids or len(currency_ids) > 1 or len(currency_ids) < 1:
				return bytes('<p>Đơn vị tiền tệ: </p>', 'utf-8')
			else:
				currency = self.env['res.currency'].browse(currency_ids[0])
				return bytes('<p>Đơn vị tiền tệ: %s</p>' % currency.name, 'utf-8')
		return res

	def _get_company_address(self):
		street, street2, city = '', '', ''
		if self.env.user.company_id.street:
			street = self.env.user.company_id.street
		if self.env.user.company_id.street2:
			street2 = self.env.user.company_id.street2
		if self.env.user.company_id.city:
			city = self.env.user.company_id.city
		res = bytes('<h6>%s</h6>' % (street + ', ' + street2 + ', ' + city), 'utf-8')
		return res

	def _get_company_registry(self):
		company_registry = ''
		if self.env.user.company_id.company_registry:
			company_registry = self.env.user.company_id.company_registry
		res = bytes('<h6>MST: %s</h6>' % company_registry, 'utf-8')
		return res

	def _get_parent_columns(self, options):
		return {}

	
	def get_html(self, options, line_id=None, additional_context=None):
		html = super(NCMRPREPORT, self).get_html(options, line_id=line_id, additional_context=additional_context)
		html = html.replace(b'<p class="js_report_header_template"></p>', self._get_report_template())
		html = html.replace(b'<h5 class="js_company_address"></h5>', self._get_company_address())
		html = html.replace(b'<h5 class="js_company_mst"></h5>', self._get_company_registry())
		html = html.replace(b'<h5 class="js_report_date_range"></h5>', self._get_report_date_range(options=options))
		html = html.replace(b'<p class="js_report_currency"></p>', self._get_report_currency(options=options))
		return html

	def get_report_filename(self, options):
		# Chưa có tên file
		file_name = _("")
		# Gán tên file
		file_name = self._get_report_name()
		if file_name and file_name != _(""):
			return file_name
		return _("New Excel")

	# def format_value(self, value, currency=False):
	# 	currency_id = currency or self.env.user.company_id.currency_id
	# 	if currency_id.is_zero(value):
	# 		return ''
	# 	res = formatLang(self.env, value, digits=0)
	# 	return res

	def _get_filter_currency(self):
		return []

	def _get_currency(self):
		currencies = self._get_filter_currency()
		currency_ids = []
		for c in currencies:
			if c.get('id') == self.env.user.company_id.currency_id.id:
				currency_ids.append({'id': c.get('id'), 'name': _(c.get('name','')), 'selected': True})
			else:
				currency_ids.append({'id': c.get('id'), 'name': _(c.get('name','')), 'selected': False})
		return currency_ids

	def _get_filter_accounts(self):
		return []

	def _get_accounts(self):
		accounts = self._get_filter_accounts()
		account_ids = []
		for a in accounts:
			account_ids.append({'id': a.get('id'), 'name': _(a.get('code', '') + ' - ' + a.get('name', '')), 'selected': False})
		return account_ids

	def _get_filter_stocks(self):
		return []

	def _get_stocks(self):
		stocks = self._get_filter_stocks()
		stock_ids = []
		for s in stocks:
			stock_ids.append({'id': s.get('id'), 'name': s.get('name', ''), 'selected': False})
		return stock_ids

	def _get_filter_cags(self):
		return []

	def _get_cags(self):
		cag = self._get_filter_cags()
		cag_ids = []
		for s in cags:
			cag_ids.append({'id': s.get('id'), 'name': s.get('name', ''), 'selected': False})
		return cag_ids

	def _get_filter_locations(self):
		return []

	def _get_locations(self):
		locations = self._get_filter_locations()
		location_ids = []
		for s in locations:
			location_ids.append({'id': s.get('id'), 'name': s.get('name', ''), 'selected': False})
		return location_ids

	def _get_filter_users(self):
		return []

	def _get_users(self):
		users = self._get_filter_users()
		user_ids = []
		for u in users:
			user_ids.append({'id': u.get('id'), 'name': u.get('name', ''), 'selected': False})
		return user_ids

	def _get_filter_assets(self):
		return []

	def _get_assets(self):
		assets = self._get_filter_assets()
		asset_ids = []
		for s in assets:
			asset_ids.append({'id': s.get('id'), 'name': s.get('name', ''), 'selected': False})
		return asset_ids

	def _get_filter_bank_accounts(self):
		return []

	def _get_bank_accounts(self):
		bank_accounts = self._get_filter_bank_accounts()
		bank_account_ids = []
		for s in bank_accounts:
			bank_account_ids.append({'id': s.get('id'), 'name': _(s.get('bank_name', '') + ' - ' + s.get('acc_number', '')), 'selected': False})
		return bank_account_ids

	def _get_filter_payroll_category(self):
		return []

	def _get_filter_payslip(self, options):
		return []

	def _get_payroll_category(self):
		category_list = self._get_filter_payroll_category()
		category_ids = []
		for c in category_list:
			if c.get('id') == category_list[0].get('id'):
				category_ids.append({'id': c.get('id'), 'name': _(c.get('name', '')), 'selected': True})
			else:
				category_ids.append({'id': c.get('id'), 'name': _(c.get('name' '')), 'selected': False})
		return category_ids
	def _get_payslip(self, options):
		payslip_list = self._get_filter_payslip(options)
		payslip_ids = []
		for p in payslip_list:
			payslip_ids.append({'id': p.get('id'), 'name': _(p.get('name', '')), 'selected': False})
		return payslip_ids

	def _get_filter_warehouse(self):
		return []

	def _get_warehouse(self):
		warehouse_list = self._get_filter_warehouse()
		warehouse_ids = []
		if len(warehouse_list) > 1:
			for c in warehouse_list:
				warehouse_ids.append({'id': c.get('id'), 'name': _(c.get('name', '')), 'selected': False})
		else:
			warehouse_ids.append({'id': warehouse_list[0].get('id'), 'name': _(warehouse_list[0].get('name', '')), 'selected': True})
		return warehouse_ids

	def _get_filter_departments(self):
		return []

	def _get_department(self):
		departments = self._get_filter_departments()
		department_ids = []
		for s in departments:
			department_ids.append({'id': s.get('id'), 'name': s.get('name', ''), 'selected': False})
		return department_ids

	def _build_options(self, previous_options=None):
		if not previous_options:
			previous_options = {}
		options = {}
		filter_list = [attr for attr in dir(self) if attr.startswith('filter_') and len(attr) > 7 and not callable(getattr(self, attr))]
		for element in filter_list:
			filter_name = element[7:]
			options[filter_name] = getattr(self, element)

		if options.get('multi_company'):
			company_ids = self.env['res.users'].sudo().browse(self.env.user.id).company_ids.ids
			if len(company_ids) > 1:
				companies = self.env['res.company'].sudo().browse(company_ids)
				options['multi_company'] = [{'id': c.id, 'name': c.name, 'selected': True if c.id == self.env.user.company_id.id else False} for c in companies]
			else:
				del options['multi_company']

		if options.get('journals'):
			options['journals'] = self._get_journals()

		if options.get('currency'):
			options['currency'] = self._get_currency()

		if options.get('account'):
			options['account'] = self._get_accounts()

		# if options.get('stock'):
		# 	options['stock'] = []

		if options.get('user'):
			options['user'] = self._get_users()

		if options.get('asset'):
			options['asset'] = self._get_assets()

		if options.get('bank_account'):
			options['bank_account'] = self._get_bank_accounts()

		if options.get('payroll_category'):
			options['payroll_category'] = self._get_payroll_category()

		if options.get('payslip'):
			options['payslip'] = self._get_payslip(options)

		if options.get('warehouse'):
			options['warehouse'] = self._get_warehouse()

		if options.get('department'):
			options['department'] = self._get_department()

		options['unfolded_lines'] = []
		# Merge old options with default from this report
		for key, value in options.items():
			if key == 'payslip':
				# chạy lại hàm get_payslip để lấy lại list payslip theo category được chọn mới
				options['payslip'] = self._get_payslip(options)
			if key in previous_options and value is not None and previous_options[key] is not None:
				# special case handler for date and comparison as from one report to another, they can have either a date range or single date
				if key == 'date' or key == 'comparison':
					if key == 'comparison':
						options[key]['number_period'] = previous_options[key]['number_period']
					options[key]['filter'] = 'custom'
					if previous_options[key].get('filter', 'custom') != 'custom':
						# just copy filter and let the system compute the correct date from it
						options[key]['filter'] = previous_options[key]['filter']
					elif value.get('date_from') is not None and not previous_options[key].get('date_from'):
						date = fields.Date.from_string(previous_options[key]['date'])
						company_fiscalyear_dates = self.env.user.company_id.compute_fiscalyear_dates(date)
						options[key]['date_from'] = company_fiscalyear_dates['date_from'].strftime(DEFAULT_SERVER_DATE_FORMAT)
						options[key]['date_to'] = previous_options[key]['date']
					elif value.get('date') is not None and not previous_options[key].get('date'):
						options[key]['date'] = previous_options[key]['date_to']
					else:
						options[key] = previous_options[key]
				else:
					if previous_options[key] in [False, True, None, [], {}, ()]:
						continue
					elif not isinstance(previous_options[key], collections.Iterable):
						options[key] = previous_options[key]
						continue
					elif not isinstance(previous_options[key][0], collections.Iterable):
						options[key] = previous_options[key]
						continue
					elif 'selected' not in previous_options[key][0]:
						options[key] = previous_options[key]
						continue
					if key in ['stock','cag','location']:
						continue
					for item in options[key]:
						if not isinstance(item, collections.Iterable):
							continue
						for o_item in previous_options[key]:
							if item['id'] != o_item['id']:
								continue
							item['selected'] = o_item['selected']

		return options

	@api.model
	def _get_options(self, previous_options=None):
		# Be sure that user has group analytic if a report tries to display analytic
		if self.filter_analytic:
			self.filter_analytic_accounts = [] if self.env.user.id in self.env.ref('analytic.group_analytic_accounting').users.ids else None
			self.filter_analytic_tags = [] if self.env.user.id in self.env.ref('analytic.group_analytic_tags').users.ids else None
			#don't display the analytic filtering options if no option would be shown
			if self.filter_analytic_accounts is None and self.filter_analytic_tags is None:
				self.filter_analytic = None
		if self.filter_partner:
			self.filter_partner_ids = []
			self.filter_partner_categories = []
		if self.filter_stock:
			self.filter_product_ids = []
		if self.filter_cag:
			self.filter_cag_ids = []
		if self.filter_location:
			self.filter_location_ids = []
		return self._build_options(previous_options)

	def _set_context(self, options):
		ctx = super(NCMRPREPORT, self)._set_context(options)
		if options.get('product_ids'):
			ctx['product_ids'] = self.env['product.product'].browse([int(product) for product in options['product_ids']])
		if options.get('cag_ids'):
			ctx['cag_ids'] = self.env['product.category'].browse([int(cag) for cag in options['cag_ids']])
		if options.get('location_ids'):
			ctx['location_ids'] = self.env['stock.location'].browse([int(location) for location in options['location_ids']])
		return ctx

	def get_report_informations(self, options):
		options = self._get_options(options)
		# apply date and date_comparison filter
		self._apply_date_filter(options)

		searchview_dict = {'options': options, 'context': self.env.context}
		# Check if report needs analytic
		if options.get('analytic_accounts') is not None:
			searchview_dict['analytic_accounts'] = self.env.user.id in self.env.ref('analytic.group_analytic_accounting').users.ids and [(t.id, t.name) for t in self.env['account.analytic.account'].search([])] or False
			options['selected_analytic_account_names'] = [self.env['account.analytic.account'].browse(int(account)).name for account in options['analytic_accounts']]
		if options.get('analytic_tags') is not None:
			searchview_dict['analytic_tags'] = self.env.user.id in self.env.ref('analytic.group_analytic_tags').users.ids and [(t.id, t.name) for t in self.env['account.analytic.tag'].search([])] or False
			options['selected_analytic_tag_names'] = [self.env['account.analytic.tag'].browse(int(tag)).name for tag in options['analytic_tags']]
		if options.get('partner'):
			options['selected_partner_ids'] = [self.env['res.partner'].browse(int(partner)).name for partner in options['partner_ids']]
			options['selected_partner_categories'] = [self.env['res.partner.category'].browse(int(category)).name for category in options['partner_categories']]
		if options.get('stock'):
			options['selected_product_ids'] = [self.env['product.product'].browse(int(product)).name for product in options['product_ids']]
		if options.get('cag'):
			options['selected_cag_ids'] = [self.env['product.category'].browse(int(cag)).name for cag in options['cag_ids']]
		if options.get('location'):
			options['selected_location_ids'] = [self.env['stock.location'].browse(int(location)).name for location in options['location_ids']]
		# Check whether there are unposted entries for the selected period or not (if the report allows it)
		if options.get('date') and options.get('all_entries') is not None:
			date_to = options['date'].get('date_to') or options['date'].get('date') or fields.Date.today()
			period_domain = [('state', '=', 'draft'), ('date', '<=', date_to)]
			options['unposted_in_period'] = bool(self.env['account.move'].search_count(period_domain))

		report_manager = self._get_report_manager(options)
		info = {'options': options,
				'context': self.env.context,
				'report_manager_id': report_manager.id,
				'footnotes': [{'id': f.id, 'line': f.line, 'text': f.text} for f in report_manager.footnotes_ids],
				'buttons': self._get_reports_buttons(),
				'main_html': self.get_html(options),
				'searchview_html': self.env['ir.ui.view'].render_template(self._get_templates().get('search_template', 'nc_mrp_report.account_vn_search_template'), values=searchview_dict),
				}
		return info

	def get_xlsx(self, options, response):
		context = self.env.context
		output = io.BytesIO()
		workbook = xlsxwriter.Workbook(output, {'in_memory': True})
		sheet = workbook.add_worksheet(self._get_report_name()[:31])

		parent_column_line_format = workbook.add_format({'font_name': 'Times New Roman','align': 'left','border': 1,'valign': 'vcenter'})
		parent_column_title_format = workbook.add_format({'font_name': 'Times New Roman','align': 'left','border': 1,'valign': 'vcenter', 'bold': True})
		parent_column_content_format = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 1,'valign': 'vcenter', 'bold': True})
		parent_header_format = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 1,'valign': 'vcenter', 'font_size': 12, 'bold': True})
		parent_column_format = workbook.add_format({'font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter', 'bold': True})
		title_format = workbook.add_format({'font_name': 'Times New Roman','align': 'left','border': 1,'valign': 'vcenter', 'font_size': 12, 'bold': True})
		money_format = workbook.add_format({'font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter'})
		money_format.set_num_format('#,##0')
		parent_column_format.set_num_format('#,##0')
		right_header_format = workbook.add_format({'font_name': 'Times New Roman','align': 'center','valign': 'vcenter', 'text_wrap': True})
		left_footer_format = workbook.add_format({'font_name': 'Times New Roman','align': 'center','valign': 'vcenter', 'text_wrap': True, 'bold': True})
		left_header_format = workbook.add_format({'font_name': 'Times New Roman','align': 'left','valign': 'vcenter', 'text_wrap': True})
		left_header_top_format = workbook.add_format({'font_name': 'Times New Roman', 'font_size': 10, 'bold': True})
		left_header_center_format = workbook.add_format({'font_name': 'Times New Roman','valign': 'vcenter', 'font_size': 10, 'italic': True})
		left_header_bottom_format = workbook.add_format({'font_name': 'Times New Roman','valign': 'vcenter', 'font_size': 10})
		center_header_format = workbook.add_format({'font_name': 'Times New Roman','align': 'center','valign': 'top', 'text_wrap': True, 'font_size': 16,'bold':1})
		center_header_date_format = workbook.add_format({'font_name': 'Times New Roman','align': 'center','valign': 'vcenter', 'text_wrap': True, 'font_size': 12})
		header_format = workbook.add_format({'font_name': 'Times New Roman','align': 'center','valign': 'vcenter','border': 1, 'bold': True, 'text_wrap': True})
		line_format = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 1, 'text_wrap': True,'valign': 'vcenter'})
		first_col_format = workbook.add_format({'font_name': 'Times New Roman','align': 'left','border': 1, 'text_wrap': True,'valign': 'vcenter'})

		
		sheet.set_row(0, 22)
		sheet.set_column(2,6,20)
		sheet.set_column(0,0,12)
		super_columns = self._get_super_columns(options)
		total_col = super_columns.get('total_col', 0)
		# left_header = ''
		# left_header += self.env.user.company_id.name + '\n'
		# street, street2, city = '', '', ''
		# address = ''
		# if self.env.user.company_id.street:
		# 	address += self.env.user.company_id.street + ', '
		# if self.env.user.company_id.street2:
		# 	address += self.env.user.company_id.street2 + ', '
		# if self.env.user.company_id.city:
		# 	address += self.env.user.company_id.city
		# company_reg = str(self._get_company_registry())[6:-6]
		# left_header += '\n' + company_reg
		# if super_columns.get('flow', False):
		# 	# sheet.merge_range(0, 0, 2, 0, left_header, left_header_format)
		# 	sheet.write(0,0,self.env.user.company_id.name, left_header_top_format)
		# 	sheet.write(1,0,address, left_header_center_format)
		# 	sheet.write(2,0,company_reg, left_header_bottom_format)
		# else:
		# 	# sheet.merge_range(0, 0, 2, 2, left_header, left_header_format)
		# 	sheet.write(0,0,self.env.user.company_id.name, left_header_top_format)
		# 	sheet.write(1,0,address, left_header_center_format)
		# 	sheet.write(2,0,company_reg, left_header_bottom_format)

		if options.get('date',False):
			if options.get('date',False).get('date_from',False) and options.get('date',False).get('date_to',False):
				dt_from = '/'.join(reversed(options['date'].get('date_from').split('-')))
				dt_to = '/'.join(reversed(options['date'].get('date_to').split('-')))
				center_header_date = 'Từ Ngày: ' + str(dt_from) + ' đến ngày: ' + str(dt_to)
				sheet.merge_range(1, 0, 1, int(total_col)-1, center_header_date, center_header_date_format)
			elif options.get('date',False).get('date',False):
				date = '/'.join(reversed(options['date'].get('date').split('-')))
				center_header_date = 'Tính đến ngày: ' + str(date)
				sheet.merge_range(1, 0, 1, int(total_col)-1, center_header_date, center_header_date_format)
		center_header = ''
		center_header_title = self._get_report_name() + '\n'
		header_col_list = []
		sheet.merge_range(0, 0, 0, int(total_col)-1, center_header_title, center_header_format)

		
		# right_header = self._get_report_template_name()
		# if super_columns.get('flow', False) and total_col == 3:
		# 	sheet.set_column(int(total_col)-2, int(total_col)-1, 20)
		# 	sheet.merge_range(0, int(total_col)-2, 2, int(total_col)-1, right_header, right_header_format)
		# else:
		# 	sheet.set_column(int(total_col)-3, int(total_col)-1, 15)
		# 	sheet.merge_range(0, int(total_col)-3, 2, int(total_col)-1, right_header, right_header_format)
		x = super_columns.get('x_offset', 0) + 1
		y = super_columns.get('y_offset', 0)
		for header in super_columns.get('header', []):
			f_row = header.get('f_row', 0) + x
			f_col = header.get('f_col', 0) + y
			l_row = header.get('rowspan', 1) - 1 + f_row
			l_col = header.get('colspan', 1) - 1 + f_col
			data = header.get('name', '')
			if f_row == l_row and f_col == l_col:
				sheet.write(f_row, f_col, data, header_format)
			else:
				sheet.merge_range(f_row, f_col, l_row, l_col, data, header_format)
			c_f_row = f_row
			c_f_col = f_col + header.get('colspan', 1)
			for child in header.get('columns', []):
				c_l_row = c_f_row + child.get('rowspan', 1) - 1
				c_l_col = c_f_col + child.get('colspan', 1) - 1
				child_data = child.get('name', '')
				if c_l_row == c_f_row and c_f_col == c_l_col:
					sheet.write(c_f_row, c_f_col, child_data, header_format)
				else:
					sheet.merge_range(c_f_row, c_f_col, c_l_row, c_l_col, child_data, header_format)
				c_f_col += child.get('colspan', 1) + child.get('space', 0)

		ctx = self._set_context(options)
		ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})
		location_selected = ctx.get('location_ids') and ctx['location_ids'].ids or False
		if location_selected and len(location_selected) == 1:
			location = self.env['stock.location'].browse(location_selected[0])
			sheet.merge_range(5, 0, 5, int(total_col)-1, 'Kho: ' + location.name, center_header_format)
		lines = self.with_context(ctx)._get_lines(options)
		line_row = c_f_row + 1
		line_col = 0
		max_dict = {}
		header_col_list.append([total_col-3,total_col-2,total_col-1,0,1,2])
		for x in range(0, total_col):
			if x in header_col_list:
				max_dict[x] = 15
			else:
				max_dict[x] = 7
		for line in lines:
			line_data = line.get('name', '')
			if line.get('parent', False) == True:
				if line.get('colspan', 0) == 0:
					sheet.write(line_row, line_col, line_data, title_format)
				else:
					sheet.merge_range(line_row, line_col, line_row, line_col + line.get('colspan', 1) - 1, line_data, title_format)
			elif line.get('parent_header', False) == True:
				if line.get('colspan', 1) > 1:
					sheet.merge_range(line_row, line_col, line_row, line_col + line.get('colspan', 1) - 1, line_data, parent_header_format)
				else:
					sheet.write(line_row, line_col, line_data, parent_header_format)
			else:
				if len(str(line_data)) > 100:
					sheet.set_column(line_col, line_col, 100)
					max_dict[line_col] = len(str(line_data))
				elif len(str(line_data)) > max_dict[line_col]:
					sheet.set_column(line_col, line_col, len(str(line_data)) + 4)
					max_dict[line_col] = len(str(line_data))
				sheet.write(line_row, line_col, line_data, first_col_format)
			line_col += 1
			for column in line.get('columns', []):
				column_data = column.get('name', '')
				if len(str(column_data)) > 50:
					sheet.set_column(line_col, line_col, 50)
					max_dict[line_col] = len(str(column_data))
				elif len(str(column_data)) > max_dict[line_col]:
					sheet.set_column(line_col, line_col, len(str(column_data)) + 4)
					max_dict[line_col] = len(str(column_data))
				if column.get('money_style', ''):
					if line.get('parent', False):
						if column.get('currency', ''):
							sheet.write(line_row, line_col + line.get('colspan', 1) - 1, column_data, parent_column_format)
						else:
							sheet.write(line_row, line_col + line.get('colspan', 1) - 1, column_data, parent_column_format)
					elif line.get('parent_header', False):
						if column.get('currency', ''):
							sheet.write(line_row, line_col + line.get('colspan', 1) - 1, column_data, parent_column_format)
						else:
							sheet.write(line_row, line_col + line.get('colspan', 1) - 1, column_data, parent_column_format)
					else:
						if column.get('currency', ''):
							sheet.write(line_row, line_col + line.get('colspan', 1) - 1, column_data, money_format)
						else:
							sheet.write(line_row, line_col, column_data, money_format)
				else:
					if line.get('parent', False):
						if column.get('flow', False):
							sheet.write(line_row, line_col + line.get('colspan', 1) - 1, column_data, parent_header_format)
						else:
							sheet.write(line_row, line_col + line.get('colspan', 1) - 1, column_data, parent_column_title_format)
					elif line.get('parent_header', False):
						sheet.write(line_row, line_col + line.get('colspan', 1) - 1, column_data, parent_column_content_format)
					else:
						if line.get('trial', False) or column.get('trial', False):
							sheet.write(line_row, line_col, column_data, parent_column_line_format)
						else:
							sheet.write(line_row, line_col, column_data, line_format)
				line_col += 1
			line_row += 1
			line_col = 0

		# left_sign_top =  "Người lập"
		# left_sign_bottom = "(Ký, họ tên)"
		# if super_columns.get('flow', False):
		# 	sheet.write(line_row + 1, 0, left_sign_top, left_footer_format)
		# 	sheet.write(line_row + 2, 0, left_sign_bottom, right_header_format)
		# else:
		# 	sheet.merge_range(line_row + 1, 0, line_row + 1, 2, left_sign_top, left_footer_format)
		# 	sheet.merge_range(line_row + 2, 0, line_row + 2, 2, left_sign_bottom, right_header_format)
		# center_sign_top = "Thủ kho"
		# center_sign_bottom = "(Ký, họ tên)"
		# if super_columns.get('flow', False) and total_col == 3:
		# 	sheet.write(line_row + 1, 1, center_sign_top, left_footer_format)
		# 	sheet.write(line_row + 2, 1, center_sign_bottom, right_header_format)
		# else:
		# 	sheet.merge_range(line_row + 1, 3, line_row + 1, int(total_col)-3, center_sign_top, left_footer_format)
		# 	sheet.merge_range(line_row + 2, 3, line_row + 2, int(total_col)-3, center_sign_bottom, right_header_format)
		# right_sign_top = "Giám đốc"
		# right_sign_bottom = "(Ký, họ tên, đóng dấu)"
		# if super_columns.get('flow', False) and total_col == 3:
		# 	sheet.write(line_row + 1, 2, right_sign_top, left_footer_format)
		# 	sheet.write(line_row + 2, 2, right_sign_bottom, right_header_format)
		# else:
		# 	sheet.merge_range(line_row + 1, int(total_col)-2, line_row + 1, int(total_col)-1, right_sign_top, left_footer_format)
		# 	sheet.merge_range(line_row + 2, int(total_col)-2, line_row + 2, int(total_col)-1, right_sign_bottom, right_header_format)

		sheet.hide_gridlines(2)
		workbook.close()
		output.seek(0)
		response.stream.write(output.read())
		output.close()

	def get_pdf(self, options, minimal_layout=True):
		if not config['test_enable']:
			self = self.with_context(commit_assetsbundle=True)

		base_url = self.env['ir.config_parameter'].sudo().get_param('report.url') or self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		rcontext = {
			'mode': 'print',
			'base_url': base_url,
			'company': self.env.user.company_id,
		}

		body = self.env['ir.ui.view'].render_template(
			"account_reports.print_template",
			values=dict(rcontext),
		)
		body_html = self.with_context(print_mode=True, pdf=True).get_html(options)

		body = body.replace(b'<body class="o_account_reports_body_print">', b'<body class="o_account_reports_body_print">' + body_html)
		if minimal_layout:
			header = ''
			footer = self.env['ir.actions.report'].render_template("web.internal_layout", values=rcontext)
			spec_paperformat_args = {'data-report-margin-top': 10, 'data-report-header-spacing': 10}
			footer = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=footer))
		else:
			rcontext.update({
					'css': '',
					'o': self.env.user,
					'res_company': self.env.user.company_id,
				})
			header = self.env['ir.actions.report'].render_template("web.external_layout", values=rcontext)
			header = header.decode('utf-8') # Ensure that headers and footer are correctly encoded
			spec_paperformat_args = {}
			# Default header and footer in case the user customized web.external_layout and removed the header/footer
			headers = header.encode()
			footer = b''
			# parse header as new header contains header, body and footer
			try:
				root = lxml.html.fromstring(header)
				match_klass = "//div[contains(concat(' ', normalize-space(@class), ' '), ' {} ')]"

				for node in root.xpath(match_klass.format('header')):
					headers = lxml.html.tostring(node)
					headers = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=headers))

				for node in root.xpath(match_klass.format('footer')):
					footer = lxml.html.tostring(node)
					footer = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=footer))

			except lxml.etree.XMLSyntaxError:
				headers = header.encode()
				footer = b''
			header = headers

		landscape = False
		if len(self.with_context(print_mode=True).get_header(options)[-1]) > 5:
			landscape = True

		return self.env['ir.actions.report']._run_wkhtmltopdf(
			[body],
			header=header, footer=footer,
			landscape=landscape,
			specific_paperformat_args=spec_paperformat_args
		)


	
	def open_document(self, options, params=None):
		if not params:
			params = {}

		ctx = self.env.context.copy()
		ctx.pop('id', '')

		# Decode params
		model = params.get('model', 'account.move.line')
		res_id = params.get('id')
		document = params.get('object', 'account.move')

		# Redirection data
		# target = self._resolve_caret_option_document(model, res_id, document)
		# view_name = self._resolve_caret_option_view(target)
		# module = 'account'
		# if '.' in view_name:
		# 	module, view_name = view_name.split('.')

		Move = self.env[model].browse(res_id).move_id
		Company = self.env.user.company_id
		action = None
		journal_id = Move.journal_id.id
		
		# Redirect
		action['res_id'] = Move.id
		return action