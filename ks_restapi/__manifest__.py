# -*- coding: utf-8 -*-
{
    'name': "ODOO REST API",
    'summary': """
        The Odoo REST API app provides a connection endpoint between an application/website and the Odoo backend.
""",
    'description': """
        In every application, there are two parts- one is the front end and the other is the backend. The front end needs a server that can provide it with the data. Whenever we click anywhere in Odoo, it requests the server for the data to show on the front end. The processing of data happens on the backend.
        The Odoo REST API app creates a medium using which if a person having any other front end look for the data deployed in Odoo or provided by Odoo can easily get it since this application exposes those APIs to the front end.
        The app is great if you have your website on any other platform than Odoo, but you want to use Odoo as a backend, simply for its best functionalities and amazing performance. The app bridges the gap between two platforms by generating APIs between them. That’s how data flow happens from the Odoo backend to your website front end.  
        The app is very beneficial if you make any Android application, and want your Odoo data on that platform too. This app acts as a medium to get you the data deployed on Odoo.

    """,
    'author': "Ksolves India Ltd.",
    'website': "https://store.ksolves.com/",
    'category': 'Tools',
    'license': 'OPL-1',
    'currency': 'EUR',
    'price': '79.0',
    'version': '15.0.1.0.0',
    'maintainer': 'Ksolves India Ltd.',
    'support': 'sales@ksolves.com',
    'installable': True,
    'application': True,
    'sequence': 1,
    "live_test_url":  "http://restapi14.kappso.in/",
    # any module necessary for this one to work correctly
    'depends': ['base'],
    # always loaded
    'data': [
        'views/ks_web_assets.xml',
        'views/ks_swagger_sidebar.xml',
    ],

    'images': [
        "static/description/banner.gif",
    ],
    'assets': {
            'web.assets_backend': [
                'ks_restapi/static/src/js/ks_swagger_ui_link.js',
                'ks_restapi/static/src/scss/ks_swagger_widget.scss',
            ],

            'web.assets_qweb': [
                    'ks_restapi/static/src/xml/ks_swagger_link.xml',
                ],

            'ks_restapi.swagger_ui_assets': [
                    'ks_restapi/static/src/lib/swagger-ui/swagger-ui.css',
                    'ks_restapi/static/src/scss/ks_swagger_sidebar.scss',
                    'ks_restapi/static/src/lib/swagger-ui/swagger-ui-bundle.js',
                    'ks_restapi/static/src/lib/swagger-ui/swagger-ui-standalone-preset.js',
                    'ks_restapi/static/src/js/ks_swagger_sidebar.js',

                ],
    },



}
