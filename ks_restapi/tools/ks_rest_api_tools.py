from collections import defaultdict

import datetime
import functools
import inspect
import json
import re
from inspect import Parameter
from odoo import conf, tools
from odoo import fields, models
from odoo.http import request, controllers_per_module
from odoo.tools import ustr

KS_API_MAIN_URL = '/ks_api'

KS_SWAGGER_API_DEFAULT_RESPONSES = {
    '400': {
        'description': 'Server cannot or will not process the request due to something that is perceived to be a '
                       'client error (e.g., malformed request syntax, invalid request message framing, or deceptive '
                       'request routing).',
    },
    '401': {
        'description': 'The request sent by the client to the server that lacks valid authentication credentials.',
    },
    '500': {
        'description': 'It means that the server encountered an unexpected condition that prevented it from '
                       'fulfilling the request. This error is usually returned by the server when no other error code'
                       ' is suitable.',
    },
}
KS_SWAGGER_DEFAULT_RESPONSE = {'200': {'description': 'Result', 'content': {'application/json': {'schema': {'type': 'object'}}}}}
KS_SWAGGER_DEFAULT_RESPONSE = defaultdict(lambda: {'content': {'application/json': {{'schema': {'type': 'object'}}}}})
KS_SWAGGER_DEFAULT_RESPONSE.update(KS_SWAGGER_API_DEFAULT_RESPONSES)
KS_SWAGGER_DEFAULT_RESPONSE.update(KS_SWAGGER_DEFAULT_RESPONSE)

KS_SWAGGER_COMPONENTS = {
    'KsRecIDs': {
        'type': 'array',
        'items': {
            'type': 'integer'
        },
        'description': 'A list of record IDs.'
    },
    'KsRecFields': {
        'type': 'array',
        'items': {
            'type': 'string'
        },
        'description': 'A list of field names.'
    },
    'KsRecDomainTuple': {
        'type': 'array',
        'items': {
            'oneOf': [
                {'type': 'string'},
                {'type': 'boolean'},
                {'type': 'number'},
                {
                    'type': 'array',
                    'items': {
                        'anyOf': [
                            {'type': 'string'},
                            {'type': 'number'}
                        ]
                    },
                }
            ]
        },
        'minItems': 3,
        'maxItems': 3,
        'description': 'A domain tuple consists of a field name, an operator and a value.'
    },
    'KsRecDomain': {
        'type': 'array',
        'items': {
            'anyOf': [
                {'type': 'string'},
                {'$ref': '#/components/schemas/KsRecDomainTuple'}

            ]
        },
        'description': 'A domain item consists either of a single operator or a tuple.'
    },
    'KsRecTuple': {
        'type': 'array',
        'items': {
            'oneOf': [
                {'type': 'integer'},
                {'type': 'string'},
            ]
        },
        'minItems': 2,
        'maxItems': 2,
        'description': 'A record tuple consists of the id and the display name of the record.'
    },
    'KsRecTuples': {
        'type': 'array',
        'items': {
            '$ref': '#/components/schemas/KsRecTuple'
        },
        'description': 'A list of record tuples.'
    },
    'KsRecReadGroupResult': {
            'type': 'array',
            'items': {
                'type': 'object',
                'properties': {
                    '__domain': {
                        '$ref': '#/components/schemas/KsRecDomain',
                    }
                },
                'additionalProperties': True,
            },
            'description': 'A list of grouped record information.'
        },
    'KsRecData': {
        'type': 'object',
        'properties': {
            'id': {
                'type': 'integer',
            },
        },
        'additionalProperties': True,
        'description': 'A map of field names and their corresponding values.'
    },
    'ksRecValues': {
            'type': 'object',
            'description': 'A map of field names and their corresponding values.'
        },
}

##################################################
# class and static method to encode the api data #
##################################################


def is_ks_date_time(ks_instance):
    if isinstance(ks_instance, datetime.date):
        if isinstance(ks_instance, datetime.datetime):
            return fields.Datetime.to_string(ks_instance)
        return fields.Date.to_string(ks_instance)


class KsApiDataResponseEncoder(json.JSONEncoder):
    def default(self, ks_instance):
        if isinstance(ks_instance, datetime.date):
            is_ks_date_time(ks_instance)
        if isinstance(ks_instance, (bytes, bytearray)):
            return ks_instance.decode()
        return ustr(ks_instance)


class KsApiDataEncoder(KsApiDataResponseEncoder):
    def default(self, ks_instance):
        if isinstance(ks_instance, models.BaseModel):
            return ks_instance.name_get()
        return KsApiDataResponseEncoder.default(self, ks_instance)


##################################################
# static method to create the route for swagger  #
##################################################
def ks_create_route(ks_routes):
    if type(ks_routes) is not list:
        ks_routes = [ks_routes]
    ks_api_routes = []
    for ks_route in ks_routes:
        ks_api_routes.append('{}{}'.format(KS_API_MAIN_URL, ks_route))
    return ks_api_routes


def ks_get_json_header(ks_json_header):
    ks_json_header = {} if not ks_json_header else ks_json_header
    ks_json_header['Content-Type'] = 'application/json'
    return ks_json_header


def ks_make_json_data(ks_api_data, ks_api_data_encoder):
    return json.dumps(ks_api_data, indent=5, cls=ks_api_data_encoder)


def ks_get_json_data(ks_api_data, ks_json_headers=None, ks_json_cookies=None, ks_api_data_encoder=KsApiDataEncoder):
    ks_json_headers = ks_get_json_header(ks_json_headers)
    ks_json_data = ks_make_json_data(ks_api_data, ks_api_data_encoder)
    return request.make_response(ks_json_data, headers=ks_json_headers, cookies=ks_json_cookies)


##########################################################
# static method to create the swagger apis documentation #
##########################################################


@functools.lru_cache()
def ks_generate_docs(ks_server_url):
    ks_tags, ks_paths = ks_generate_routing_docs()
    return {
        'openapi': '3.0.0',
        'info': {
            'version': '1.0.0',
            'title': 'RestAPI Documentation',
        },
        'servers': [{
            'url': ks_server_url,
        }],
        'tags': ks_tags,
        'paths': ks_paths,
        'components': {
            'schemas': {**KS_SWAGGER_COMPONENTS},
        },
    }


def ks_generate_routing_docs():
    ks_installed_modules = request.registry._init_modules | set(conf.server_wide_modules)
    ks_modules_with_controllers = filter(lambda m: m in controllers_per_module, ks_installed_modules)

    ks_rest_api_tags, ks_rest_api_path_values = ks_get_routes_info(ks_modules_with_controllers, ks_installed_modules)
    return list(filter(None, ks_rest_api_tags)), ks_rest_api_path_values,


# getting subclasses of a object using recursion
def ks_get_sub_classes(ks_instance, ks_installed_modules):
    ks_sub_classes_res = []

    for subclass in ks_instance.__subclasses__():
        if ks_is_valid_instance(subclass, ks_installed_modules):
            ks_sub_classes_res.extend(ks_get_sub_classes(subclass, ks_installed_modules))

    # condition to stop the recursive call
    if not ks_sub_classes_res and ks_is_valid_instance(ks_instance, ks_installed_modules):
        ks_sub_classes_res = [ks_instance]
    return ks_sub_classes_res


# check is instance belongs to a valid classes
def ks_is_valid_instance(ks_instance, ks_installed_modules):
    return ks_instance.__module__.startswith('odoo.addons.') and ks_instance.__module__.split(".")[
        2] in ks_installed_modules


def ks_get_routes_info(ks_modules_with_controllers, ks_installed_modules):
    ks_rest_api_tags = set()
    ks_rest_api_path_values = dict()

    for ks_current_module in ks_modules_with_controllers:
        for _, ks_current_cls in controllers_per_module[ks_current_module]:
            ks_children_classes = list(tools.unique(
                c for c in ks_get_sub_classes(ks_current_cls, ks_installed_modules) if c is not ks_current_cls))
            ks_controlller_instance = type('KsGetApisInformation', tuple(reversed(ks_children_classes)),
                                           {}) if ks_children_classes else ks_current_cls()
            for _, ks_current_method in inspect.getmembers(ks_controlller_instance, inspect.ismethod):
                if hasattr(ks_current_method, 'ks_api_info'):
                    ks_method_info, ks_route_info = ks_extract_restapi_method_info(ks_current_method)
                    if ks_method_info.get('show', True):
                        ks_api_tags, ks_api_paths = ks_create_docs_json(ks_current_method, ks_method_info,
                                                                        ks_route_info)
                        ks_rest_api_tags.update(ks_api_tags)
                        ks_rest_api_path_values.update(ks_api_paths)
    return ks_rest_api_tags, ks_rest_api_path_values


def ks_extract_restapi_method_info(ks_method):
    ks_traversed_method = set()
    ks_method_info = dict(responses=KS_SWAGGER_DEFAULT_RESPONSE)
    ks_route_info = dict(auth='none', methods=["GET"], routes=[])
    for ks_current_class in reversed(ks_method.__self__.__class__.mro()):
        ks_current_func = getattr(ks_current_class, ks_method.__name__, None)
        if ks_current_func not in ks_traversed_method:
            if hasattr(ks_current_func, 'routing'):
                ks_route_info.update(ks_current_func.routing)
            if hasattr(ks_current_func, 'ks_api_info'):
                ks_method_info.update(ks_current_func.ks_api_info)
        ks_traversed_method.add(ks_current_func)
    return ks_method_info, ks_route_info


def ks_setting_route_initial_default_vals(ks_current_method, ks_api_route_args):
    for ks_method_args_key, ks_method_args_vals in inspect.signature(ks_current_method).parameters.items():
        if ks_method_args_vals.kind not in [Parameter.VAR_POSITIONAL, Parameter.VAR_KEYWORD]:
            ks_api_route_args[ks_method_args_key] = {
                'name': ks_method_args_key,
                'in': 'query',
            }
            if ks_method_args_vals.default and ks_method_args_vals.default is not Parameter.empty:
                ks_api_route_args[ks_method_args_key]['example'] = ks_method_args_vals.default
    return ks_api_route_args


def ks_add_params_to_route(ks_current_route, ks_api_route_args):
    for param in map(lambda m: m.split(':')[-1], re.findall(r'<(.*?:?.+?)>', ks_current_route)):
        if param not in ks_api_route_args:
            ks_api_route_args[param] = {'name': param}
        ks_api_route_args[param].update({'in': 'path'})
    return ks_api_route_args


def ks_set_parameter_context_company(ks_protected_method, ks_method_info, ks_api_route_args):

    if ks_method_info.get('parameter_context', ks_protected_method):
        ks_api_route_args['with_context'] = {
            'name': 'with_context',
            'description': 'Context',
            'in': 'query',
            'content': {
                'application/json': {
                    'schema': {
                        'type': 'object'
                    }
                }
            },
            'example': {},
        }

    if ks_method_info.get('parameter_company', ks_protected_method):
        ks_api_route_args['with_company'] = {
            'name': 'with_company',
            'description': 'Current Company',
            'in': 'query',
            'schema': {
                'type': 'integer'
            },
            'example': 1,
        }

    for ks_method_args_key, ks_method_args_vals in ks_method_info.get('parameter', {}).items():
        if ks_method_args_key not in ks_api_route_args:
            ks_api_route_args[ks_method_args_key] = {
                'name': ks_method_args_key,
                'in': 'query',
            }
        ks_api_route_args[ks_method_args_key].update(ks_method_args_vals)

    for param in ks_method_info.get('exclude_parameters', []):
        del ks_api_route_args[param]
    return ks_api_route_args


def ks_extract_method_args(ks_current_method, ks_current_route, ks_method_info, ks_route_info):
    ks_protected_method = ks_route_info.get('protected', False)
    ks_api_route_args = dict()

    ks_api_route_args = ks_setting_route_initial_default_vals(ks_current_method, ks_api_route_args)
    ks_api_route_args = ks_add_params_to_route(ks_current_route, ks_api_route_args)
    ks_api_route_args = ks_set_parameter_context_company(ks_protected_method, ks_method_info, ks_api_route_args)

    return ks_api_route_args


def ks_create_docs_json(ks_current_method, ks_method_info, ks_route_info):
    ks_method_routes = ks_method_info.get('paths', ks_route_info['routes'])
    ks_method_types = ks_method_info.get('methods', ks_route_info['methods'])

    ks_extract_api_route_values = dict()
    ks_extract_api_tag_values = set()

    for ks_current_route in ks_method_routes:
        ks_current_method_route = ks_current_route
        ks_method_route_info = dict()
        ks_route_api_parameters = ks_extract_method_args(ks_current_method, ks_current_route, ks_method_info, ks_route_info)

        for ks_route in map(lambda m: m.split(':')[-1], re.findall(r'<(.*?:?.+?)>', ks_current_route)):
            ks_current_method_route = re.sub(r'<(.*?:?{})>'.format(ks_route), '{{{}}}'.format(ks_route), ks_current_method_route)

        for ks_method_type in map(lambda ks_type: ks_type.lower(), ks_method_types):

            ks_route_info_values = {
                'tags': ks_method_info.get('tags', []),
                'summary': ks_method_info.get('summary', ""),
                'description': ks_method_info.get('description', ""),
                'responses': ks_method_info.get('responses', {}),
                'parameters': ks_method_info.get('parameters', list(ks_route_api_parameters.values())),
            }

            if ks_method_info.get('requestBody', False):
                ks_route_info_values['requestBody'] = ks_method_info['requestBody']

            if ks_method_info.get('ks_swagger_default_response', False):
                for response in ks_method_info['ks_swagger_default_response']:
                    if response not in ks_route_info_values['responses']:
                        ks_route_info_values['responses'][response] = KS_SWAGGER_DEFAULT_RESPONSE[response]

            if ks_method_type in ks_method_info:
                ks_route_info_values.update(ks_method_info[ks_method_type])

            if '{}_{}'.format(ks_method_type, ks_current_route) in ks_method_info:
                ks_route_info_values.update(ks_method_info['{}_{}'.format(ks_method_type, ks_current_route)])

            ks_extract_api_tag_values.update(ks_route_info_values.get('tags', []))
            ks_method_route_info[ks_method_type] = ks_route_info_values

        ks_extract_api_route_values[ks_current_method_route] = ks_method_route_info
    return ks_extract_api_tag_values, ks_extract_api_route_values


def ks_define_swagger_doc(**kwargs):
    ks_api_info = kwargs.copy()

    def ks_wrapper_method(func):
        @functools.wraps(func)
        def ks_inner_wrapper_method(*args, **kw):
            return func(*args, **kw)

        ks_inner_wrapper_method.ks_api_info = ks_api_info
        return ks_inner_wrapper_method

    return ks_wrapper_method
