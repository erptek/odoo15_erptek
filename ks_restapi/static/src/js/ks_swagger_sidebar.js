odoo.define('ks_resapi.SwaggerSideBar', function (require) {
'use strict';

owl.utils.whenReady(() => {

SwaggerUIBundle({
    url: odoo.ks_rest_api.ksApiMainUrl + '/ks_get_swagger_doc/ks_apis.json',
    dom_id: '#ks_swagger_ui',
    deepLinking: true,
    presets: [
        SwaggerUIBundle.presets.apis,
        SwaggerUIStandalonePreset
    ],
    plugins: [
        SwaggerUIBundle.plugins.DownloadUrl
    ],
    layout: 'BaseLayout',
    operationsSorter: (elem1, elem2) => {
        let methodsOrder = ['get', 'post', 'put', 'delete', 'patch', 'options', 'trace'];
        let result = methodsOrder.indexOf(elem1.get('method')) - methodsOrder.indexOf(elem2.get('method'));
        return result === 0 ? elem1.get('path').localeCompare(elem2.get('path')) : result;
    },
    tagsSorter: 'alpha',
});

});

});


