odoo.define('ks_resapi.SwaggerUiLink', function (require) {
'use strict';

var Widget = require('web.Widget');
var SystrayMenu = require('web.SystrayMenu');
var config = require('web.config');

var KsSwaggerUiLink = Widget.extend({
    name: 'ks_swagger_ui_link_menu',
    template:'ks_restapi.ksswaggerlink',
    events: {
    	'click .ks_restapi_swagger_ui_icon': 'ks_toggle_swagger_ui',
    },

     ks_toggle_swagger_ui: function(ev) {
        if(this.el.classList.contains('show')) {
        	this.el.classList.remove('show');
        } else {
        	this.el.classList.add('show');
        }
        ev.preventDefault();
    },
});

if (!config.device.isMobile) {
	SystrayMenu.Items.push(KsSwaggerUiLink);
}

return KsSwaggerUiLink;

});
$(".ks_swaggerlink_system_tray").click(function(){
  $("#modal-show").toggleClass("main");
});