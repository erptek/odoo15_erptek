import re

from odoo import SUPERUSER_ID, _
from odoo.http import Controller, route, request, Response
from odoo.addons.ks_restapi.tools.ks_rest_api_tools import ks_create_route, KS_API_MAIN_URL, ks_get_json_data, \
    ks_generate_docs, ks_define_swagger_doc
import werkzeug
from odoo.tools.safe_eval import safe_eval
from odoo.tools.misc import str2bool
import json


def _ks_get_host_domain():
    return request.env['ir.config_parameter'].sudo().get_param('web.base.url')


def ks_get_api_paths():
    rest_docs = ks_generate_docs(_ks_get_host_domain())
    return rest_docs


class KsGetApisInformation(Controller):

    @route(
        route=ks_create_route(['/ks_get_swagger_doc']),
        methods=['GET'],
        type='http',
        auth='none',
    )
    def ks_get_swagger_doc(self, template='ks_swagger_docs'):
        if not template or not template.startswith('ks_swagger_docs'):
            raise werkzeug.exceptions.BadRequest(_('Invalid template name.'))
        return request.render('ks_restapi.{}'.format(template), {
            'ks_host_url': _ks_get_host_domain(), 'ks_api_main_url': KS_API_MAIN_URL
        })

    @route(
        route=ks_create_route('/ks_get_swagger_doc/ks_apis.json'),
        methods=['GET'],
        type='http',
        auth='none',
    )
    def ks_swagger_docs_apis(self, **kw):
        return ks_get_json_data(ks_get_api_paths())


def ks_parse_params(model, domain, fields, groupby, offset=0, limit=80, orderby=None, lazy=True):
    ks_domain = ks_parse_domain(domain)
    ks_fields = safe_eval(fields)
    ks_groupby = safe_eval(groupby, [])
    ks_limit = limit and int(limit) or 0
    ks_offset = offset and int(offset) or 0
    ks_lazy = str2bool(lazy)
    return ks_domain, model, ks_fields, ks_groupby, ks_limit, ks_offset, orderby, ks_lazy


def ks_parse_search_params(model, domain, count=False, limit=80, offset=0, orderby=None):
    ks_domain = ks_parse_domain(domain)
    ks_count = count and str2bool(count) or None
    ks_limit = limit and int(limit) or 0
    ks_offset = offset and int(offset) or 0
    ks_model = request.env[model]
    return ks_domain, ks_model, ks_count, ks_limit, ks_offset, orderby


def ks_parse_search_read_params(model, domain, fields=None, limit=80, offset=0, orderby=None):
    ks_domain = ks_parse_domain(domain)
    ks_fields = json.loads(fields)
    ks_limit = limit and int(limit) or None
    ks_offset = offset and int(offset) or None
    ks_model = model
    return ks_domain, ks_model, ks_fields, ks_limit, ks_offset, orderby


def ks_parse_ids(ids):
    if isinstance(ids, int):
        return [ids]
    values = safe_eval(ids)
    if isinstance(values, int):
        return [values]
    return list(map(lambda i: int(i), values))


def ks_parse_domain(ks_api_domain):
    ks_api_domain = json.loads(ks_api_domain)

    if not ks_api_domain:
        ks_api_domain = []

    ks_valid_domain = []
    for ks_domain in ks_api_domain:
        if isinstance(ks_domain, str) and ks_domain not in ['&', '|', '!']:
            ks_domain = json.loads(re.sub(r'^.*?List\s?', '', ks_domain))
            if not ks_domain:
                ks_domain = []
        ks_valid_domain.append(ks_domain)
    return ks_valid_domain


class KsSwaggerApiRoutesDefinition(Controller):

    @ks_define_swagger_doc(
        tags=["ODOO REST API's"],
        summary='Get Installed Modules',
        description='Returns a list of installed modules.',
        parameter_context=False,
        parameter_company=False,
        responses={
            '200': {
                'description': 'List of Modules',
                'content': {
                    'application/json': {
                        'example': {
                            'base': 1,
                            'web': 2,
                        }
                    }
                }
            }
        },
        ks_swagger_default_response=['400', '401', '500'],
    )
    @route(
        routes=ks_create_route('/ks_get_installed_modules'),
        methods=['GET'], csrf=False,
        auth='none', save_session=False,
    )
    def ks_get_installed_modules(self):
        return ks_get_json_data(request.env['ir.module.module']._installed())

    @ks_define_swagger_doc(
        tags=["ODOO REST API's"],
        summary='Get Record info using XML ID',
        description='Returns the XML ID record.',
        parameter={
            'xmlid': {
                'name': 'xmlid',
                'description': 'XML ID',
                'example': 'base.main_company',
            },
        },
        parameter_context=False,
        parameter_company=False,
        responses={
            '200': {
                'description': 'XML ID Record',
                'content': {
                    'application/json': {
                        'example': {
                            'model': 'res.company',
                            'id': 1,
                        }
                    }
                }
            }
        },
        ks_swagger_default_response=['400', '401', '500'],
    )
    @route(
        routes=ks_create_route([
            '/ks_get_xmlid_info',
        ]),
        methods=['GET'],
        csrf=False,
        auth='none', save_session=False,
    )
    def ks_get_xmlid(self, xmlid):
        record = request.env.ref(xmlid)
        return ks_get_json_data({'model': record._name, 'id': record.id})

    @ks_define_swagger_doc(
        tags=["ODOO REST API's"],
        summary='Get Record Name',
        description='Get the record name.',
        parameter={
            'model': {
                'name': 'model',
                'description': 'Model',
                'required': True,
                'schema': {
                    'type': 'string'
                },
                'example': 'sale.order',
            },
            'ids': {
                'name': 'ids',
                'description': 'Record IDs',
                'required': True,
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecIDs'
                        },
                        'example': [1, 2],
                    }
                },
            },
        },
        responses={
            '200': {
                'description': 'List of ID and Name Tupel',
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecTuples'
                        },
                        'example': [[1, 'SUB001'], ]
                    }
                }
            }
        },
        ks_swagger_default_response=['400', '401', '500'],
    )
    @route(
        routes=ks_create_route([
            '/ks_get_record_name',
        ]),
        methods=['GET']
    )
    def ks_get_name_get(self, model, ids):
        return ks_get_json_data(request.env[model].browse(ks_parse_ids(ids)).name_get())

    @ks_define_swagger_doc(
        tags=["ODOO REST API's"],
        summary='Read Record Information Using READ',
        description='Returns the Record information using odoo READ ORM method.',
        parameter={
            'model': {
                'name': 'model',
                'description': 'Model',
                'required': True,
                'schema': {
                    'type': 'string'
                },
                'example': 'sale.order',
            },
            'ids': {
                'name': 'ids',
                'description': 'Record IDs',
                'required': True,
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecIDs'
                        },
                        'example': [1, 2],
                    }
                },
            },
            'fields': {
                'name': 'fields',
                'description': 'Fields',
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecFields',
                        },
                        'example': ['name'],
                    }
                },
            },
        },
        responses={
            '200': {
                'description': 'List of ID and name tupels',
                'content': {
                    'application/json': {
                        'example': [{
                            'id': 1,
                            'name': 'SUB001'
                        }]
                    }
                }
            }
        },
        ks_swagger_default_response=['400', '401', '500'],
    )
    @route(
        routes=ks_create_route([
            '/ks_get_record_info',
        ]),
        methods=['GET'],
    )
    def ks_get_record_info(self, model, ids, fields=None):
        if fields:
            return ks_get_json_data(request.env[model].browse(ks_parse_ids(ids)).read(safe_eval(fields)))
        else:
            return ks_get_json_data(request.env[model].browse(ks_parse_ids(ids)).read())

    @ks_define_swagger_doc(
        tags=["ODOO REST API's"],
        summary='Read Group is ODOO ORM Method',
        description='Read Group is used to search the records',
        parameter={
            'model': {
                'name': 'model',
                'description': 'Model',
                'required': True,
                'schema': {
                    'type': 'string'
                },
                'example': 'sale.order',
            },
            'domain': {
                'name': 'domain',
                'required': True,
                'description': 'Search Domain',
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecDomain',
                        }
                    }
                },
                'example': [('partner_id', '!=', False)],
            },
            'fields': {
                'name': 'fields',
                'description': 'Fields',
                'required': True,
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecFields',
                        }
                    }
                },
                'example': ['name', 'partner_id'],
            },
            'groupby': {
                'name': 'groupby',
                'description': 'GroupBy',
                'required': True,
                'content': {
                    'application/json': {
                        'schema': {
                            'type': 'array',
                            'items': {
                                'type': 'string',
                            }
                        }
                    }
                },
                'example': ['partner_id'],
            },
            'limit': {
                'name': 'limit',
                'description': 'Limit',
                'schema': {
                    'type': 'integer'
                },
            },
            'offset': {
                'name': 'offset',
                'description': 'Offset',
                'schema': {
                    'type': 'integer'
                },
            },
            'orderby': {
                'name': 'orderby',
                'description': 'Order',
                'schema': {
                    'type': 'string'
                },
            },
            'lazy': {
                'name': 'lazy',
                'description': 'Lazy Loading',
                'schema': {
                    'type': 'boolean'
                },
            },
        },
        responses={
            '200': {
                'description': 'Records',
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecReadGroupResult',
                        },
                        'example': [{
                            '__domain': [
                                '&', ['partner_id', '!=', False],
                                '|', ['name', '=', 'SUB001'],
                            ]
                        }]
                    }
                }
            }
        },
        ks_swagger_default_response=['400', '401', '500'],
    )
    @route(
        routes=ks_create_route([
            '/ks_read_group',
        ]),
        methods=['GET'],
    )
    def ks_get_read_group(self, model, domain, fields, groupby, offset=0, limit=80, orderby=False, lazy=True):
        ks_domain, ks_model, ks_fields, ks_groupby, ks_limit, ks_offset, ks_orderby, ks_lazy = ks_parse_params(model,
                                                                                                               domain,
                                                                                                               fields,
                                                                                                               groupby,
                                                                                                               offset,
                                                                                                               limit,
                                                                                                               orderby,
                                                                                                               lazy)

        return ks_get_json_data(request.env[ks_model].read_group(
            ks_domain, ks_fields, groupby=ks_groupby, offset=ks_offset,
            limit=ks_limit, orderby=ks_orderby, lazy=ks_lazy
        ))

    @ks_define_swagger_doc(
        tags=["ODOO REST API's"],
        summary='Search',
        description='Search for matching records',
        parameter={
            'model': {
                'name': 'model',
                'description': 'Model',
                'required': True,
                'schema': {
                    'type': 'string'
                },
                'example': 'sale.order',
            },
            'domain': {
                'name': 'domain',
                'required': True,
                'description': 'Search Domain',
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecDomain',
                        }
                    }
                },
                'example': [('partner_id', '!=', False)],
            },
            'count': {
                'name': 'count',
                'description': 'Count',
                'schema': {
                    'type': 'boolean'
                },
            },
            'limit': {
                'name': 'limit',
                'description': 'Limit',
                'schema': {
                    'type': 'integer'
                },
            },
            'offset': {
                'name': 'offset',
                'description': 'Offset',
                'schema': {
                    'type': 'integer'
                },
            },
            'orderby': {
                'name': 'orderby',
                'description': 'Order',
                'schema': {
                    'type': 'string'
                },
            },
        },
        responses={
            '200': {
                'description': 'Records IDs',
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecIDs'
                        },
                        'example': [1, 2]
                    }
                }
            }
        },
        ks_swagger_default_response=['400', '401', '500'],
    )
    @route(
        routes=ks_create_route([
            '/ks_search_records'
        ]),
        methods=['GET'],
    )
    def ks_search_rec_info(self, model, domain=None, count=False, limit=80, offset=0, orderby=None):
        ks_domain, ks_model, ks_count, ks_limit, ks_offset, ks_order = ks_parse_search_params(model, domain, count,
                                                                                              limit, offset, orderby)
        ks_search_res = ks_model.search(ks_domain, offset=ks_offset, limit=ks_limit, order=ks_order, count=ks_count)
        if not ks_count:
            return ks_get_json_data(ks_search_res.ids)
        return ks_get_json_data(ks_search_res)

    @ks_define_swagger_doc(
        tags=["ODOO REST API's"],
        summary='Search Read',
        description='Search for matching records',
        parameter={
            'model': {
                'name': 'model',
                'description': 'Model',
                'required': True,
                'schema': {
                    'type': 'string'
                },
                'example': 'sale.order',
            },
            'domain': {
                'name': 'domain',
                'required': True,
                'description': 'Search Domain',
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecDomain',
                        }
                    }
                },
                'example': [('partner_id', '!=', False)],
            },
            'fields': {
                'name': 'fields',
                'description': 'Fields',
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecFields',
                        }
                    }
                },
                'example': ['name', 'partner_id'],
            },
            'limit': {
                'name': 'limit',
                'description': 'Limit',
                'schema': {
                    'type': 'integer'
                },
            },
            'offset': {
                'name': 'offset',
                'description': 'Offset',
                'schema': {
                    'type': 'integer'
                },
            },
            'orderby': {
                'name': 'orderby',
                'description': 'Order',
                'schema': {
                    'type': 'string'
                },
            },
        },
        responses={
            '200': {
                'description': 'Records',
                'content': {
                    'application/json': {
                        'schema': {
                            '$ref': '#/components/schemas/KsRecData'
                        },
                        'example': [{
                            'active': True,
                            'id': 19,
                            'name': 'Ready Mat'
                        }]
                    }
                }
            }
        },
        ks_swagger_default_response=['400', '401', '500'],
    )
    @route(
        routes=ks_create_route([
            '/ks_search_read_rec'
        ]),
        methods=['GET'],
    )
    def ks_search_read_record(self, model, domain=None, fields=None, limit=80, offset=0, orderby=None):
        ks_domain, ks_model, ks_fields, ks_limit, ks_offset, ks_order = ks_parse_search_read_params(model, domain,
                                                                                                    fields, limit,
                                                                                                    offset, orderby)
        return ks_get_json_data(request.env[ks_model].search_read(
            ks_domain, fields=ks_fields, offset=ks_offset, limit=ks_limit, order=ks_order
        ))
