# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo.http import content_disposition, request
from odoo.addons.web.controllers.main import ExcelExport
from datetime import date, datetime, timedelta, time
from odoo import http, models, fields, _


class SALESHIPEXPORTEXCEL(ExcelExport):

	@http.route('/sale/ship/sumary/export/xls/<string:payment_status>/<string:payment_method>', type='http', auth="user")
	def action_export_excel_sale_shipping_sumary(self, payment_status=None, payment_method=None):
		SaleEnv = request.env['sale.order'].sudo()
		response = request.make_response(
			None,
			headers=[
				('Content-Type', 'application/vnd.ms-excel'),
				('Content-Disposition', content_disposition('Ship' + '.xlsx'))]
		)
		SaleEnv.export_sale_ship_sumary_report(payment_status, payment_method, response)
		return response

class DBCRUD(http.Controller):

	@http.route('/woocommerce/sale/crud/create', type='json', auth='none', csrf=False)
	def woo_order_create(self, **kw):
		# request.session.authenticate('fuson-1205', 'itm@icviet.com', 'a')
		data = request.jsonrequest
		city = request.env['woo.res.city'].sudo().search([('name','=',data.get('shipping', {}).get('state','') or data.get('billing', {}).get('state',''))], limit=1)
		if not city:
			city = request.env['woo.res.city'].sudo().create({
				'name': data.get('shipping', {}).get('state','') or data.get('billing', {}).get('state','')
				})
		district = request.env['woo.res.district'].sudo().search([('name','=',data.get('shipping', {}).get('city','') or data.get('billing', {}).get('city','')), ('city_id','=',city.id)], limit=1)
		if not district:
			district = request.env['woo.res.district'].sudo().create({
				'name': data.get('shipping', {}).get('city','') or data.get('billing', {}).get('city',''),
				'city_id': city.id,
				})
		ward = request.env['woo.res.ward'].sudo().search([('name','=',data.get('shipping', {}).get('address_2','') or data.get('billing', {}).get('address_2','')), ('district_id','=',district.id)], limit=1)
		if not ward:
			ward = request.env['woo.res.ward'].sudo().create({
				'name': data.get('shipping', {}).get('address_2','') or data.get('billing', {}).get('address_2',''),
				'district_id': district.id
				})
		customer = request.env['res.partner'].sudo().search([
			('name','=', ((data.get('shipping', {}).get('last_name',False) or data.get('billing', {}).get('last_name',False)) and (data.get('shipping', {}).get('last_name',False) or data.get('billing', {}).get('last_name',False)) + ' ' or '') + (data.get('shipping', {}).get('first_name',False) or data.get('billing', {}).get('first_name',False))),
			('email','=', data.get('shipping', {}).get('email','') or data.get('billing', {}).get('email','')),
			('phone','=', data.get('shipping', {}).get('phone','') or data.get('billing', {}).get('phone','')),
			('street', '=', data.get('shipping', {}).get('address_1','') or data.get('billing', {}).get('address_1','')),
			('street2', '=', data.get('shipping', {}).get('address_2','') or data.get('billing', {}).get('address_2','')),
			('city', '=', data.get('shipping', {}).get('city','') or data.get('billing', {}).get('city','')),
			('zip','=', data.get('shipping', {}).get('postcode','') or data.get('billing', {}).get('postcode',''))],limit=1)
		if not customer:
			country_id = request.env['res.country'].sudo().search([('code','=',data.get('billing',{}).get('country', ''))], limit=1)
			customer = request.env['res.partner'].sudo().create({
				'name': ((data.get('shipping', {}).get('last_name',False) or data.get('billing', {}).get('last_name',False)) and (data.get('shipping', {}).get('last_name',False) or data.get('billing', {}).get('last_name',False)) + ' ' or '') + (data.get('shipping', {}).get('first_name',False) or data.get('billing', {}).get('first_name',False)),
				'email': data.get('shipping', {}).get('email','') or data.get('billing', {}).get('email',''),
				'phone': data.get('shipping', {}).get('phone','') or data.get('billing', {}).get('phone',''),
				'street': data.get('shipping', {}).get('address_1','') or data.get('billing', {}).get('address_1',''),
				'street2': data.get('shipping', {}).get('address_2','') or data.get('billing', {}).get('address_2',''),
				'city': data.get('shipping', {}).get('city','') or data.get('billing', {}).get('city',''),
				'zip': data.get('shipping', {}).get('postcode','') or data.get('billing', {}).get('postcode',''),
				'country_id': country_id and country_id.id or False,
				})
		res = request.env['sale.order'].sudo().create({
			'woo_id': data.get('id', False),
			'partner_id': customer.id,
			'woo_payment_method': data.get('payment_method', 'cod'),
			'woo_payment_status': (data.get('set_paid', False) or data.get('date_paid', False)) and 'paid' or 'not',
			'user_id': False,
			'order_origin_source': 'Woocommerce',
			'woo_city_id': city.id,
			'woo_district_id': district.id,
			'woo_ward_id': ward.id,
			'note': data.get('customer_note',''),
			})
		line_dict = []
		for line in data.get('line_items', []):
			tmp_id = request.env['product.template'].sudo().search([('woo_id','=',line.get('product_id', False))], limit=1)
			product_id = request.env['product.product'].sudo().search([('woo_id','=',line.get('variation_id', False))], limit=1)
			line_dict += [(0,0,{
				'woo_id': line.get('id', False),
				'product_id': product_id and product_id.id or tmp_id.product_variant_id.id,
				'product_uom_qty': line.get('quantity', 0.0),
				'product_uom': product_id and product_id.uom_id.id or tmp_id.product_variant_id.uom_id.id,
				'price_unit': product_id and (product_id.woo_regular_price or product_id.lst_price) or tmp_id.product_variant_id.lst_price,
				})]
		if data.get('shipping_lines', []):
			ship_total = sum(float(l.get('total',0)) for l in data.get('shipping_lines', []))
			ship_product = request.env['product.product'].sudo().search([('name','=','Shipping'),('type','=','service')], limit=1)
			if not ship_product:
				ship_template = request.env['product.template'].sudo().create({'name': 'Shipping', 'type': 'service'})
				ship_product = ship_template.product_variant_id
			line_dict += [(0,0,{
				'woo_id': data.get('shipping_lines', []) and data.get('shipping_lines', [])[0].get('id', 0) or 0,
				'product_id': ship_product.id,
				'product_uom_qty': 1,
				'product_uom': ship_product.uom_id.id,
				'price_unit': ship_total,
				})]
		res.with_context(woo_update=True).sudo().update({
			'order_line': line_dict,
			})
		return res

	@http.route('/woocommerce/sale/crud/update', type='json', auth='none', csrf=False)
	def woo_order_update(self, **kw):
		# request.session.authenticate('fuson-1205', 'itm@icviet.com', 'a')
		data = request.jsonrequest
		sale_order = request.env['sale.order'].sudo().search([('woo_id','=',data.get('id',False))], limit=1)
		if sale_order:
			if sale_order.state in ('draft', 'sent') and data.get('status', '') == 'processing':
				sale_order.with_context(woo_update=True).sudo().action_confirm()
			if data.get('status', '') in ('failed ', 'cancelled', 'trash', 'refunded'):
				sale_order.with_context(woo_update=True).sudo().action_cancel()
			if data.get('set_paid', False) or data.get('date_paid', False):
				sale_order.with_context(woo_update=True).sudo().update({
					'woo_payment_status': 'paid'
					})
		return sale_order
