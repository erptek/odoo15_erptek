# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from woocommerce import API


class WoocommerceCommunication(models.Model):
	_name = "woocommerce.communication"

	name = fields.Char(string="Name", required=True)
	host_url = fields.Char(string="Host", required=True)
	consumer_key = fields.Char(string="Consumer Key", required=True)
	consumer_secret = fields.Char(string="Consumer Secret", required=True)
	user_name = fields.Char(string="User Name", required=True)
	password = fields.Char(string="Password", required=True)
	state = fields.Selection([('draft', 'Draft'),
		('active', 'Active')], string="Status", default='draft')
	sale_ids = fields.One2many('sale.order', 'woocommerce_id',string="Sale Orders")
	product_ids = fields.Many2many('product.product', string="Products")
	location_id = fields.Many2one('stock.location', domain=[('usage','=','internal')], string="Location", required=True)
	product_count = fields.Integer(string="Product count", compute='_compute_product_count', store=True)
	sale_order_count = fields.Integer(string="Order count", compute='_compute_order_count', store=True)
	picking_count = fields.Integer(string="Picking count", compute='_compute_order_count', store=True)

	@api.depends("sale_ids")
	def _compute_order_count(self):
		for record in self:
			record.sale_order_count = len(record.sale_ids)
			record.picking_count = sum(len(sale.picking_ids) for sale in record.sale_ids)

	@api.depends("product_ids")
	def _compute_product_count(self):
		for record in self:
			record.product_count = len(record.product_ids)

	def get_woocommerce_api(self):
		self.ensure_one()
		return API(
			url=self.host_url,
			consumer_key=self.consumer_key,
			consumer_secret=self.consumer_secret,
			timeout=50,
			version="wc/v3",
			wp_api=True
			)

	def test_api(self):
		self.ensure_one()
		wcapi = API(
			url=self.host_url,
			consumer_key=self.consumer_key,
			consumer_secret=self.consumer_secret,
			timeout=50,
			version="wc/v3",
			wp_api=True
			)
		response = wcapi.get("").json()
		if response.get('namespace', False):
			raise ValidationError(_("Connect Susscess!"))
		else:
			raise ValidationError(_(str(response)))
		return True
