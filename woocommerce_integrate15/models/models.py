# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import requests
from odoo.http import request
import base64
import json
import time
import datetime
from odoo.exceptions import ValidationError
from woocommerce import API
from odoo.tools.misc import formatLang
try:
	from odoo.tools.misc import xlsxwriter
except ImportError:
	import xlsxwriter
import xlwt
import os
import io

# woo_records = request.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)



class ProductAttribute(models.Model):
	_inherit = "product.attribute"

	woo_id = fields.Integer(string="WooCommerce ID")

	@api.model
	def create(self, vals):
		res = super(ProductAttribute, self).create(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			for record in res:
				response = wcapi.post('products/attributes', {'name': record.name,'has_archives': True}).json()
				if response.get('id', False):
					record.update({'woo_id': int(response.get('id', 0))})
				else:
					raise ValidationError(_(str(response)))
		return res

	def write(self, vals):
		res = super(ProductAttribute, self).write(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			if res:
				if 'name' in vals:
					for record in self:
						response = wcapi.put('products/attributes/%s' % (str(record.woo_id)), {'name': vals['name']}).json()
						if not response.get('id', False):
							raise ValidationError(_(str(response)))
		return res

	def unlink(self):
		records = [r.woo_id for r in self]
		res = super(ProductAttribute, self).unlink()
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			if res:
				for record in records:
					response = wcapi.delete('products/attributes/%s' % (str(record)), params={"force": True}).json()
					if not response.get('id', False):
						raise ValidationError(_(str(response)))
		return res


class ProductAttributeValue(models.Model):
	_inherit = "product.attribute.value"

	woo_id = fields.Integer(string="WooCommerce ID")
	woo_description = fields.Char(string="Woocommerce description")

	@api.model
	def create(self, vals):
		res = super(ProductAttributeValue, self).create(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			for record in res:
				response = wcapi.post('products/attributes/%s/terms' % (record.attribute_id.woo_id), {'name': record.name, 'description': record.woo_description or ""}).json()
				if response.get('id', False):
					record.update({'woo_id': int(response.get('id', 0))})
				else:
					raise ValidationError(_(str(response)))
		return res
	
	def write(self, vals):
		res = super(ProductAttributeValue, self).write(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			if res:
				if 'name' in vals or 'woo_description' in vals:
					for record in self:
						response = wcapi.put('products/attributes/%s/terms/%s' % (record.attribute_id.woo_id,record.woo_id), {'description': vals.get('woo_description', "") or (record.woo_description or ""),'name': vals.get('name',False) or record.name}).json()
						if not response.get('id', False):
							raise ValidationError(_(str(response)))
		return res

	def unlink(self):
		records = [{'id': r.woo_id, 'pid': r.attribute_id.woo_id} for r in self]
		res = super(ProductAttributeValue, self).unlink()
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			if res:
				for record in records:
					response = wcapi.delete('products/attributes/%s/terms/%s' % (record['pid'],record['id']), params={"force": True}).json()
					if not response.get('id', False):
						raise ValidationError(_(str(response)))
		return res

class WooCommerceProduct(models.Model):
	_name = "product.woocommerce"

	woocommerce_id = fields.Many2one('woocommerce.communication', string="Woocommerce", required=True)
	woo_id = fields.Integer(string="Woocommerce ID")
	sale_price = fields.Float(string="Price")
	product_tml_id = fields.Many2one('product.template', string="Product")

class ProductTemplate(models.Model):
	_inherit = "product.template"

	woo_id = fields.Integer(string="Woocommerce ID")
	woo_publish = fields.Boolean(string="Publish", default=True)
	detailed_type = fields.Selection(default='product')
	woo_ids = fields.One2many('product.woocommerce', 'product_tml_id', string="Woocommerce")

	def integrate_woocommerce_product(self):
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			for record in self.search([('woo_id','=',False),('type','=','product')]):
				rs = False
				if record.image_1920:
					author = '%s:%s' % (woo_records.user_name, woo_records.password)
					author = bytes(author, 'utf-8')
					headers = {
								'Content-Type': 'image/jpg',
								'Content-Disposition': 'attachment; filename=%s' % "abc.jpg",
								'Authorization': 'Basic %s' % base64.b64encode(author).decode('ascii')
							}
					data = base64.b64decode(record.image_1920)
					rs = requests.post('%s/wp-json/wp/v2/media/' % woo_records.host_url, data=data, headers=headers).json()
				response = wcapi.post('products', {'categories': record.categ_id.woo_id and [{"id": record.categ_id.woo_id}] or [],'manage_stock': True,'attributes': [{'id': a.attribute_id.woo_id,'name': a.attribute_id.name, 'visible': True, 'variation': True, 'options': [av.name for av in a.value_ids]} for a in record.attribute_line_ids],'type': record.attribute_line_ids and 'variable' or 'simple', 'images': rs and [{'id': rs['id']}] or [],'regular_price': str(record.list_price),'name': record.name, 'description': record.description or ""}).json()
				if response.get('id', False):
					record.update({'woo_id': int(response.get('id', 0))})
				else:
					raise ValidationError(_(str(response)))

	def change_woo_publish(self):
		for record in self:
			if record.woo_publish:
				record.woo_publish = False
			else:
				record.woo_publish = True

	@api.model
	def create(self, vals):
		res = super(ProductTemplate, self).create(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')])
		for woo_record in woo_records:
			wcapi = woo_record.get_woocommerce_api()
			if not self._context.get('woo_update', False):
				for record in res:
					rs = False
					if record.image_1920:
						author = '%s:%s' % (woo_record.user_name, woo_record.password)
						author = bytes(author, 'utf-8')
						headers = {
									'Content-Type': 'image/jpg',
									'Content-Disposition': 'attachment; filename=%s' % "abc.jpg",
									'Authorization': 'Basic %s' % base64.b64encode(author).decode('ascii')
								}
						data = base64.b64decode(record.image_1920)
						rs = requests.post('%s/wp-json/wp/v2/media/' % woo_record.host_url, data=data, headers=headers).json()
					response = wcapi.post('products', {'categories': record.categ_id.woo_id and [{"id": record.categ_id.woo_id}] or [],'manage_stock': True,'attributes': [{'id': a.attribute_id.woo_id,'name': a.attribute_id.name, 'visible': True, 'variation': True, 'options': [av.name for av in a.value_ids]} for a in record.attribute_line_ids],'type': record.attribute_line_ids and 'variable' or 'simple', 'images': rs and [{'id': rs['id']}] or [],'regular_price': str(record.list_price),'name': record.name, 'description': record.description or ""}).json()
					if response.get('id', False):
						self.env['product.woocommerce'].create({
							'woocommerce_id': woo_record.id,
							'woo_id': int(response.get('id', 0)),
							'product_tml_id': record.id,
							})
						woo_record.write({'product_ids': [(4, [record.id])] }) 
						record.update({'woo_id': int(response.get('id', 0))})
					else:
						raise ValidationError(_(str(response)))
		return res

	def write(self, vals):
		res = super(ProductTemplate, self).write(vals)
		# woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')])
		if res:
			for record in self:
				for woo_term in record.woo_ids:
					woo_record = woo_term.woocommerce_id
					wcapi = woo_record.get_woocommerce_api()
					if 'categ_id' in vals or 'woo_publish' in vals or 'name' in vals or 'description' in vals or 'list_price' in vals or 'attribute_line_ids' in vals or 'woo_ids' in vals:
						response = wcapi.put('products/%s' % (woo_term.woo_id), {
							'categories': record.categ_id.woo_id and [{"id": record.categ_id.woo_id}] or [],
							'status': record.woo_publish and 'publish' or 'draft',
							'attributes': [{'id': a.attribute_id.woo_id,'name': a.attribute_id.name, 'visible': True, 'variation': True, 'options': [av.name for av in a.value_ids]} for a in record.attribute_line_ids],
							'type': record.attribute_line_ids and 'variable' or 'simple',
							'description': record.description or "",'name': vals.get('name',False) or record.name, 'regular_price': woo_term.sale_price and str(woo_term.sale_price) or str(record.list_price or "")}).json()
						if not response.get('id', False):
							raise ValidationError(_(str(response)))
						if 'attribute_line_ids' in vals and record.product_variant_ids:
							for pv in record.product_variant_ids:
								response = wcapi.put('products/%s/variations/%s' % (woo_term.woo_id,pv.woo_id), {
								"attributes": [{"id": av.attribute_id.woo_id,"option": str(av.name)} for av in pv.product_template_attribute_value_ids]}).json()
					if 'image_1920' in vals:
						author = '%s:%s' % (woo_record.user_name, woo_record.password)
						author = bytes(author, 'utf-8')
						headers = {
								'Content-Type': 'image/jpg',
								'Content-Disposition': 'attachment; filename=%s' % "abc.jpg",
								'Authorization': 'Basic %s' % base64.b64encode(author).decode('ascii')
							}
						data = base64.b64decode(record.image_1920)
						rs = requests.post('%s/wp-json/wp/v2/media/' % woo_record.host_url, data=data, headers=headers).json()
						response = wcapi.put('products/%s' % (woo_term.woo_id), {'images': [{'id': rs['id']}]}).json()
						if not response.get('id', False):
							raise ValidationError(_(str(response)))
		return res

	def unlink(self):
		self.ensure_one()
		# records = [r.woo_id for r in self]
		# woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		woo_ids = self.woo_ids
		res = super(ProductTemplate, self).unlink()
		if woo_ids:
			for woo_term in woo_ids:
				woo_record = woo_term.woocommerce_id
				wcapi = woo_record.get_woocommerce_api()
				if res and not self._context.get('woo_update', False):
					response = wcapi.delete('products/%s' % (woo_term.woo_id), params={"force": True}).json()
					if not response.get('id', False):
						raise ValidationError(_(str(response)))
		return res

class ProductProduct(models.Model):
	_inherit = "product.product"

	woo_id = fields.Integer(string="Woocommerce ID")
	woo_regular_price = fields.Float(string="Regular Price", default=0.0)

	@api.model
	def create(self, vals):
		res = super(ProductProduct, self).create(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			if not self._context.get('woo_update', False):
				for record in res:
					if record.product_template_attribute_value_ids:
						response = wcapi.post('products/%s/variations' % (record.product_tmpl_id.woo_id), {'regular_price': str(record.lst_price),
							'name': record.name, 'description': record.description or "",'manage_stock': True,
							"attributes": [{"id": av.attribute_id.woo_id,"option": str(av.name)} for av in record.product_template_attribute_value_ids]
							}).json()
						if response.get('id', False):
							record.update({'woo_id': int(response.get('id', 0))})
						else:
							raise ValidationError(_(str(response)))
		return res

	def write(self, vals):
		res = super(ProductProduct, self).write(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			if res and not self._context.get('woo_update', False):
				if 'qty_available' in vals or 'name' in vals or 'description' in vals or 'list_price' in vals or 'woo_regular_price' in vals: 
					for record in self:
						if record.woo_id:
							response = wcapi.put('products/%s/variations/%s' % (record.product_tmpl_id.woo_id,record.woo_id), {
								'description': record.description or "",'name': record.name, 'regular_price': str(record.woo_regular_price or "")}).json()
							if not response.get('id', False):
								raise ValidationError(_(str(response)))
				if 'image_1920' in vals:
					for record in self:
						if record.woo_id:
							author = '%s:%s' % (woo_records.user_name, woo_records.password)
							author = bytes(author, 'utf-8')
							headers = {
									'Content-Type': 'image/jpg',
									'Content-Disposition': 'attachment; filename=%s' % "abc.jpg",
									'Authorization': 'Basic %s' % base64.b64encode(author).decode('ascii')
								}
							data = base64.b64decode(record.image_1920)
							rs = requests.post('%s/wp-json/wp/v2/media/' % woo_records.host_url, data=data, headers=headers).json()
							response = wcapi.put('products/%s/variations/%s' % (record.product_tmpl_id.woo_id,record.woo_id), {'images': [{'id': rs['id']}]}).json()
							if not response.get('id', False):
								raise ValidationError(_(str(response)))
		return res

	def unlink(self):
		records = [{'id': r.woo_id, 'pid': r.product_tmpl_id.woo_id} for r in self]
		res = super(ProductProduct, self).unlink()
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			if res and not self._context.get('woo_update', False):
				for record in records:
					if record['id']:
						response = wcapi.delete('products/%s/variations/%s' % (record['pid'], record['id']), params={"force": True}).json()
						if not response.get('id', False):
							raise ValidationError(_(str(response)))
		return res


class StockQuant(models.Model):
	_inherit = "stock.quant"

	@api.model
	def create(self, vals):
		res = super(StockQuant, self).create(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			for record in res:
				woo_terms = record.product_tmpl_id.woo_ids.filtered(lambda l: l.woocommerce_id.location_id.id == record.location_id.id)
				if woo_terms:
					woo_term = woo_terms[0]
					woo_record = woo_term.woocommerce_id
					wcapi = woo_record.get_woocommerce_api()
					# qty_res = record.product_id.with_context(location=record.location_id.id)._compute_quantities_dict(None, None, None, None, None)
					# response = wcapi.put('products/%s/variations/%s' % (record.product_id.product_tmpl_id.woo_id,record.product_id.woo_id), {
					# 	'stock_quantity': int(qty_res[record.product_id.id]['qty_available']),
					# 	'stock_status': int(qty_res[record.product_id.id]['qty_available']) and 'instock' or 'outofstock',}).json()
					# if record.product_tmpl_id.woo_id:
					tmp_qty_res = record.product_tmpl_id.with_context(location=record.location_id.id)._compute_quantities_dict()
					response = wcapi.put('products/%s' % (woo_term.woo_id), {
						'stock_quantity': int(tmp_qty_res[record.product_tmpl_id.id]['qty_available']),
						'stock_status': int(tmp_qty_res[record.product_tmpl_id.id]['qty_available']) and 'instock' or 'outofstock',}).json()
		return res

	def write(self, vals):
		res = super(StockQuant, self).write(vals)
		# woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		# if woo_records:
			
		if 'inventory_quantity' in vals or 'quantity' in vals:
			for record in self:
				woo_terms = record.product_tmpl_id.woo_ids.filtered(lambda l: l.woocommerce_id.location_id.id == record.location_id.id)
				if woo_terms:
					woo_term = woo_terms[0]
					woo_record = woo_term.woocommerce_id
					wcapi = woo_record.get_woocommerce_api()
					
					# if record.product_id.woo_id and record.product_tmpl_id.woo_id:
					# 	qty_res = record.product_id.with_context(location=record.location_id.id)._compute_quantities_dict(None, None, None, None, None)
					# 	response = wcapi.put('products/%s/variations/%s' % (record.product_id.product_tmpl_id.woo_id,record.product_id.woo_id), {
					# 		'stock_quantity': int(qty_res[record.product_id.id]['qty_available']),
					# 		'stock_status': int(qty_res[record.product_id.id]['qty_available']) and 'instock' or 'outofstock',}).json()
					# if record.product_tmpl_id.woo_id:
					tmp_qty_res = record.product_tmpl_id.with_context(location=record.location_id.id)._compute_quantities_dict()
					response = wcapi.put('products/%s' % (woo_term.woo_id), {
						'stock_quantity': int(tmp_qty_res[record.product_tmpl_id.id]['qty_available']),
						'stock_status': int(tmp_qty_res[record.product_tmpl_id.id]['qty_available']) and 'instock' or 'outofstock',}).json()
		return res

class SaleOrder(models.Model):
	_inherit = "sale.order"

	woo_id = fields.Integer(string="Woocommerce ID")
	woo_order_name = fields.Char(string="Woo Order", compute='_compute_woo_order_name', store=True, readonly=True)
	woo_payment_method = fields.Selection([('cod','COD'),('bacs', 'Chuyển khoản'),('onepayus', 'OnePay')], string="Phương thức thanh toán", default='cod')
	woo_payment_status = fields.Selection([('not', 'Chưa thanh toán'),('paid', 'Đã thanh toán')], string="Tình trạng thanh toán", default='not')
	delivered_status = fields.Selection([('not','Chưa giao'),('delivered','Đã giao')], string="TT giao hàng", compute='_compute_delivered_status', default='not')
	order_origin_source = fields.Char(string="Nguồn")
	woo_city_id = fields.Many2one('woo.res.city', name="City")
	woo_district_id = fields.Many2one('woo.res.district', name="District")
	woo_ward_id = fields.Many2one('woo.res.ward', name="Ward")
	woo_ship_address = fields.Char(string="Ship Address", compute='_compute_woo_shipp_address', store=True, readonly=True)
	woocommerce_id = fields.Many2one('woocommerce.communication', string="Woocommerce Store")

	@api.model
	def export_sale_ship_sumary_report(self, payment_status, payment_method, response):
		output = io.BytesIO()
		workbook = xlsxwriter.Workbook(output, {'in_memory': True})
		sheet = workbook.add_worksheet('Ship Infomation')
		city_style = workbook.add_format({'font_name': 'Times New Roman','align': 'left','border': 0,'valign': 'vcenter','font_size': 14,'bold': True,'text_wrap':True})
		district_style = workbook.add_format({'font_name': 'Times New Roman','align': 'left','border': 0,'valign': 'vcenter','font_size': 13,'bold': True,'text_wrap':True})
		
		title_style = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 0,'valign': 'vcenter','font_size': 16,'bold': True,'text_wrap':True})
		date_title_style = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 0,'valign': 'vcenter','font_size': 13,'bold': True,'text_wrap':True})
		header_style = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 1,'valign': 'vcenter','font_size': 10,'bold': True,'text_wrap':True})
		line_style = workbook.add_format({'font_name': 'Times New Roman','align': 'left','border': 1,'valign': 'vcenter','font_size': 10,'text_wrap':True})
		center_line_style = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 1,'valign': 'vcenter','font_size': 10})
		number_style = workbook.add_format({'font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 10})
		number_style.set_num_format('#,##0')
		quantity_style = workbook.add_format({'font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 10})
		total_style = workbook.add_format({'font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 7, 'bold': True})
		
		# formatLang(self.env,mrp_value['pr_quantity'],digits=0)
		domain = [('state','=','sale'),('effective_date','=',False),('woo_city_id','!=',False),('woo_district_id','!=',False),('woo_ward_id','!=',False)]
		if payment_status != 'False':
			domain += [('woo_payment_status','=',payment_status)]
		if payment_method != 'False':
			domain += [('woo_payment_method','=',payment_method)]
		sale_dict = {}
		sales = self.env['sale.order'].search(domain)
		for sale in sales:
			if sale.woo_city_id not in sale_dict:
				sale_dict[sale.woo_city_id] = {}
			if sale.woo_district_id not in sale_dict[sale.woo_city_id]:
				sale_dict[sale.woo_city_id][sale.woo_district_id] = []
			sale_dict[sale.woo_city_id][sale.woo_district_id] += [{
			'order': str(sale.woo_id),
			'partner': sale.partner_id.name,
			'address': (sale.partner_id.street and (sale.partner_id.street + ', ') or '') + sale.woo_ward_id.name + ', ' + sale.woo_district_id.name + ', ' + sale.woo_city_id.name,
			'phone': sale.partner_id.phone or '',
			'payment_method': sale.woo_payment_method == 'cod' and 'COD' or (sale.woo_payment_method == 'onepayus' and 'Onepay' or 'Chuyển khoản'),
			'payment_status': sale.woo_payment_status == 'paid' and 'Đã thanh toán' or 'Chưa thanh toán',
			'total': sale.woo_payment_method == 'cod' and sale.amount_total or 0,
			'note': sale.note,
			}]
		sheet.set_column(2,2,13)
		sheet.set_column(1,1,20)
		sheet.set_column(3,3,40)
		sheet.set_column(4,6,15)
		sheet.set_column(7,7,20)
		sheet.merge_range(0, 0, 0, 7, "Thông tin Ship", title_style)
		sheet.write(2, 0, 'Đơn hàng', header_style)
		sheet.write(2, 1, 'Tên người nhận', header_style)
		sheet.write(2, 2, 'Số điện thoại', header_style)
		sheet.write(2, 3, 'Địa chỉ', header_style)
		sheet.write(2, 4, 'P/T thanh toán', header_style)
		sheet.write(2, 5, 'T/T thanh toán', header_style)
		sheet.write(2, 6, 'Số tiền phải thu', header_style)
		sheet.write(2, 7, 'Ghi chú', header_style)
		row = 3
		for city,record in sale_dict.items():
			sheet.merge_range(row, 0, row, 7, city.name, city_style)
			row += 1
			for district,lines in sale_dict[city].items():
				sheet.merge_range(row, 0, row, 7, '    ' + district.name, district_style)
				row += 1
				for line in lines:
					sheet.write(row, 0, line['order'], center_line_style)
					sheet.write(row, 1, line['partner'], line_style)
					sheet.write(row, 2, line['phone'], quantity_style)
					sheet.write(row, 3, line['address'], line_style)
					sheet.write(row, 4, line['payment_method'], center_line_style)
					sheet.write(row, 5, line['payment_status'], line_style)
					sheet.write(row, 6, formatLang(self.env,line['total'],digits=0), number_style)
					sheet.write(row, 7, line['note'], line_style)
					row += 1
		workbook.close()
		output.seek(0)
		response.stream.write(output.read())
		output.close()

	@api.depends('woo_id')
	def _compute_woo_order_name(self):
		for record in self:
			record.woo_order_name = str(record.woo_id)


	def name_get(self):
		return [(sale.id, "%s - %s" % (sale.name, sale.woo_order_name)) for sale in self]

	@api.model
	def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
		args = args or []
		domain = []
		if name:
			domain = ['|', ('woo_order_name', operator, name), ('name', operator, name)]
			if operator in expression.NEGATIVE_TERM_OPERATORS:
				domain = ['&', '!'] + domain[1:]
		sale_ids = self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)
		return models.lazy_name_get(self.browse(sale_ids).with_user(name_get_uid))

	@api.depends('woo_city_id','woo_district_id','woo_ward_id')
	def _compute_woo_shipp_address(self):
		for record in self:
			record.woo_ship_address = (record.woo_city_id and (record.woo_city_id.name + ', ') or '') + (record.woo_district_id and (record.woo_district_id.name + ', ') or '') + (record.woo_ward_id and record.woo_ward_id.name or '')

	@api.depends('order_line.qty_delivered')
	def _compute_delivered_status(self):
		for order in self:
			if sum(l.product_uom_qty for l in order.order_line) > 0.0:
				order.update({'delivered_status': sum(l.qty_delivered for l in order.order_line) == sum(l.product_uom_qty for l in order.order_line) and 'delivered' or 'not'})
			else:
				order.update({'delivered_status': 'not'})

	# def write(self, vals):
	# 	res = super(SaleOrder, self).write(vals)
	# 	woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
	# 	if woo_records:
	# 		wcapi = woo_records.get_woocommerce_api()
	# 		if res and not self._context.get('woo_update', False):
	# 			if 'state' in vals:
	# 				for record in self:
	# 					if record.woo_id:
	# 						response = wcapi.put('orders/%s' % (record.woo_id), {
	# 							'status': record.state == 'order' and 'processing' or (record.state == 'done' and 'completed' or (record.state == 'cancel' and 'cancelled' or 'pending'))}).json()
	# 			if 'order_line' in vals:
	# 				for record in self:
	# 					if record.woo_id:
	# 						line_dict = []
	# 						ship_dict = []
	# 						for line in record.order_line.filtered(lambda l: l.product_id.type == 'product'):
	# 							item = {}
	# 							if line.woo_id:
	# 								item['id'] = line.woo_id
	# 							if line.product_id.woo_id:
	# 								item['variant_id'] = line.product_id.woo_id
	# 							item['product_id'] = line.product_id.product_tmpl_id.woo_id
	# 							item['quantity'] = int(line.product_uom_qty)
	# 							line_dict.append(item)
	# 						# for line in record.order_line.filtered(lambda l: l.product_id.type == 'service'):
	# 						# 	item = {}
	# 						# 	if line.woo_id:
	# 						# 		item['id'] = line.woo_id
	# 						# 		item['total'] = line.price_subtotal
	# 						# 		line_dict.append(item)
	# 						response = wcapi.put('orders/%s' % (record.woo_id), {
	# 							'line_items': line_dict}).json()
	# 	return res

class SaleOrderLine(models.Model):
	_inherit = "sale.order.line"

	woo_id = fields.Integer(string="Woocommerce ID")

class WooCommerceResCity(models.Model):
	_name = "woo.res.city"

	name = fields.Char(string="Name")

class WooCommerceResDistrict(models.Model):
	_name = "woo.res.district"

	name = fields.Char(string="Name")
	city_id = fields.Many2one('woo.res.city', string="City")

class WooCommerceResWard(models.Model):
	_name = "woo.res.ward"

	name = fields.Char(string="Name")
	district_id = fields.Many2one('woo.res.district', string="District")

class StockPicking(models.Model):
	_inherit = "stock.picking"

	woo_city_id = fields.Many2one('woo.res.city', name="City", related='sale_id.woo_city_id', store=True, readonly=True)
	woo_district_id = fields.Many2one('woo.res.district', name="District", related='sale_id.woo_district_id', store=True, readonly=True)
	woo_ward_id = fields.Many2one('woo.res.ward', name="Ward", related='sale_id.woo_ward_id', store=True, readonly=True)
	woo_ship_address = fields.Char(string="Ship Address", related='sale_id.woo_ship_address', store=True, readonly=True)


class ProductCategory(models.Model):
	_inherit = "product.category"

	woo_id = fields.Integer(string="WooCommerce ID")

	@api.model
	def create(self, vals):
		res = super(ProductCategory, self).create(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			for record in res:
				response = wcapi.post('products/categories', {'name': record.name}).json()
				if response.get('id', False):
					record.update({'woo_id': int(response.get('id', 0))})
				else:
					raise ValidationError(_(str(response)))
		return res

	def write(self, vals):
		res = super(ProductCategory, self).write(vals)
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			if res:
				if 'name' in vals:
					for record in self:
						response = wcapi.put('products/categories/%s' % (str(record.woo_id)), {'name': vals['name']}).json()
						if not response.get('id', False):
							raise ValidationError(_(str(response)))
		return res

	def unlink(self):
		records = [r.woo_id for r in self]
		res = super(ProductCategory, self).unlink()
		woo_records = self.env['woocommerce.communication'].sudo().search([('state','=','active')], limit=1)
		if woo_records:
			wcapi = woo_records.get_woocommerce_api()
			if res:
				for record in records:
					response = wcapi.delete('products/categories/%s' % (str(record)), params={"force": True}).json()
					if not response.get('id', False):
						raise ValidationError(_(str(response)))
		return res