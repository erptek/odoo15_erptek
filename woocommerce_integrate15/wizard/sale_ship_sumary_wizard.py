# -*- coding: utf-8 -*-
from datetime import date

from dateutil.relativedelta import *
from odoo import models, fields

class SaleShipSumaryWizard(models.TransientModel):
    _name = 'sale.ship.sumary.wizard'

    payment_status = fields.Selection([('not','Chưa thanh toán'),('paid','Đã thanh toán')], string="Tình trạng thanh toán")
    payment_method = fields.Selection([('cod','COD'),('bacs', 'Chuyển khoản'),('onepayus', 'OnePay')], string="Phương thức thanh toán")
    city_ids = fields.Many2many('woo.res.city', string="Thành phố")
    district_ids = fields.Many2many('woo.res.district', string="Quận")
    ward_ids = fields.Many2many('woo.res.ward', string="Phường")

    def action_confirm(self):
    	return {
			"type": "ir.actions.act_url",
			"url": "/sale/ship/sumary/export/xls/" + str(self.payment_status) + "/" + str(self.payment_method),
			"target": "current",
		}