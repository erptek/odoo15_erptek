# -*- coding: utf-8 -*-
{
    'name': "WooCommerce Integrate Odoo 15",
    'summary': """This module enables users to connect woocommerce api to odoo modules of sales, partners and inventory""",
    'website': "https://www.erptek.net",
    'category': 'Connectors',
    'version': '0.1',
    'depends': ['product', 'sale', 'stock', 'purchase', 'contacts'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/sale_ship_sumary_wizard_views.xml',
        'views/woocommerce_communication_views.xml',
        'views/product_attribute_value_views.xml',
    ],
    'installable': True,
    'auto_install': False
}
