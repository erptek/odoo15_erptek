# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo.http import content_disposition, request
from odoo.addons.web.controllers.main import ExcelExport
from datetime import date, datetime, timedelta, time
from odoo import http, models, fields, _

class MRPQUANTITYREPORTEXCEL(ExcelExport):

	@http.route('/mrp/quantity/report/export/xls/<string:date_from>/<string:date_to>', type='http', auth="user")
	def action_export_excel_stock_picking(self, date_from, date_to):
		MRPEnv = request.env['mrp.production'].sudo()
		response = request.make_response(
			None,
			headers=[
				('Content-Type', 'application/vnd.ms-excel'),
				('Content-Disposition', content_disposition('MRPQuantity' + '.xlsx'))]
		)
		MRPEnv.export_mrp_quantity_report(date_from, date_to, response)
		return response


class DBCRUD(http.Controller):

	@http.route('/web/crud/update', type='json', auth='none')
	def update(self, db, login, password, update, r_id, model):
		request.session.authenticate(db, login, password)
		record = request.env[model].browse(r_id)
		record.update(update)
		return request.env[model].search_read([('id','=',r_id)],['name'] + [key for key,value in update.items()],limit=1)


	@http.route('/web/crud/create', type='json', auth='none')
	def create(self, db, login, password, create, model):
		request.session.authenticate(db, login, password)
		records = request.env[model].create(create)
		return request.env[model].search_read([('id','in',records.ids)],['name'] + [key for key,value in create.items()])

	@http.route('/web/crud/read', type='json', auth='none')
	def search_read(self, db, login, password, model, fields=None, domain=None):
		request.session.authenticate(db, login, password)
		return request.env[model].search_read(domain or [], fields or [])