# -*- coding: utf-8 -*-
from datetime import date

from dateutil.relativedelta import *

from odoo import models, fields
from odoo.osv import expression

class MRPQUANTITYREPORTWIZARD(models.TransientModel):
    _name = 'mrp.quantity.report.wizard'

    company_id = fields.Many2one("res.company", "Company",default=lambda self: self.env.companies.ids[0],domain=lambda self: [('id', 'in', self.env.companies.ids)])
    date_from = fields.Date("From Date",required=True,default=date.today().replace(day=1))
    date_to = fields.Date("To Date",required=True,default=date.today().replace(day=1) + relativedelta(day=31))


    def action_confirm(self):
    	return {
			"type": "ir.actions.act_url",
			"url": "/mrp/quantity/report/export/xls/" + str(self.date_from) + "/" + str(self.date_to),
			"target": "current",
		}