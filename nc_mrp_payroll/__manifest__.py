# -*- coding: utf-8 -*-
{
    'name': "Nam&Co MRP Payroll",
    'summary': """.
    """,
    'author': "Hoan",
    'category': 'MRP',
    'version': '13.0.1.1',
    'license': 'LGPL-3',
    'depends': ['mrp', 'hr'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/mrp_quantity_report_views.xml',
        'wizard/mrp_routing_workcenter_import_views.xml',
        'views/mrp_routing_workcenter_views.xml',
        'views/mrp_workorder_views.xml',
        'views/mrp_production_views.xml',
        'views/hr_employee_views.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
