# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from odoo.osv import expression


class MRPWORKORDER(models.Model):
	_inherit = "mrp.workorder"

	employee_ids = fields.One2many('mrp.workorder.employee', 'workorder_id', string="Employee")
	step_sequence = fields.Integer(string="Step Sequence", required=True)
	step_id = fields.Many2one('mrp.work.step', string="Work Step")
	unit_price = fields.Float(string="Đơn giá")

	def name_get(self):
		return [(wo.id, "%s - %s" % (wo.step_sequence, wo.name)) for wo in self]

	@api.model
	def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
		args = args or []
		domain = []
		if name:
			if name.isnumeric():
				domain = ['|', ('step_sequence', '=', name), ('name', operator, name)]
			else:
				domain = [('name', operator, name)]
			if operator in expression.NEGATIVE_TERM_OPERATORS:
				domain = ['&', '!'] + domain[1:]
		workorder_ids = self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)
		return models.lazy_name_get(self.browse(workorder_ids).with_user(name_get_uid))


class MRPWORKORDEREMPLOYEE(models.Model):
	_name = "mrp.workorder.employee"

	employee_id = fields.Many2one('hr.employee', string="Employee", required=True)
	quantity = fields.Integer(string="Số lượng")
	workorder_id = fields.Many2one('mrp.workorder', string="Work Order")
	production_id = fields.Many2one('mrp.production', string="MRP")
	sequence = fields.Integer(string="Sequence" ,related='workorder_id.step_sequence', store=True)
	step_price = fields.Float(string="Step Price", related='workorder_id.unit_price', store=True)
	date_record = fields.Date(string="Ngày", required=True, default=fields.Date.today())
	production_line_id = fields.Many2one('hr.department', string="Chuyền sản xuất", related="employee_id.department_id", store=True)

