# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from datetime import date, datetime, timedelta, time
from odoo.tools.misc import formatLang
from odoo.exceptions import ValidationError
try:
	from odoo.tools.misc import xlsxwriter
except ImportError:
	import xlsxwriter
import xlwt
import os
import io
import base64
import requests


class MRPPRODUCTION(models.Model):
	_inherit = "mrp.production"

	def _default_production_line(self):
		line_id = self.env['hr.department'].search([('parent_id.name','=ilike','Sản Xuất'),('manager_id','=',self.env.user.employee_id.id)],limit=1)
		return line_id and line_id.id or False

	def _production_line_domain(self):
		domain = []
		parent_id = self.env['hr.department'].search_read([('name','=ilike','Sản Xuất')],['id'],limit=1)
		if parent_id:
			domain = [('parent_id','=',parent_id[0]['id'])]
		return domain

	order_code = fields.Char(string="Order")
	production_line_id = fields.Many2one('hr.department', string="Chuyền sản xuất", domain=_production_line_domain, default=_default_production_line, required=True)
	order_quantity = fields.Float(string="Order Quantity")
	daily_line_ids = fields.One2many('mrp.production.daily.record', 'production_id', string="Daily Record")
	productivity_expect = fields.Float(string="Năng suất trung bình")
	produced_qty = fields.Float(string="Produced Qty", compute='_compute_produced_qty', store=True)
	iot_currency_produced = fields.Float(string="Produced Qty", readonly=True)

	def button_start_production(self):
		#b827eb0ddf7cffde
		# params = self.env['ir.config_parameter']
		for record in self:
			# response = requests.post(
			# 	'http://18.138.126.175:8080/gwstart',
			# 	json={
			# 		'gwID': record.production_line_id.iot_gw_id or '',
			# 		'OC': record.order_code or '',
			# 		'MO': record.name,
			# 		'quantity': record.product_qty,
			# 		'planStart': str(record.date_planned_start),
			# 		'planStop': str(record.date_planned_finished),
			# 		'startCnt': record.iot_currency_produced,
			# 		'update': {
			# 			'db': self._cr.dbname,
			# 			'login': 'admin',
			# 			'password': 'admin',
			# 			'r_id': record.id,
			# 			'url': params.get_param('web.base.url'),
			# 		}
			# 	},
			# 	headers={'Content-Type': 'application/json'},
			# )
			# json_response = response.json()
			# raise Warning(json_response)
			# if not json_response.get('success',False):
			# 	raise ValidationError(_(json_response.get('errorMsgs','Error!')))
			record.update({'state': 'progress'})

	def button_pause_production(self):
		self.filtered(lambda r: r.state == 'progress').write({'state': 'planned'})

	def button_done_production(self):
		self.filtered(lambda r: r.state != 'done').write({'state': 'done'})


	def refresh_view(self):
		return True

	@api.depends('daily_line_ids')
	def _compute_produced_qty(self):
		for record in self:
			record.update({'produced_qty': sum(l.record_quantity for l in record.daily_line_ids) or 0.0})


	def _prepare_workorder_vals(self, operation, workorders, quantity):
		self.ensure_one()
		todo_uom = self.product_uom_id.id
		if self.product_id.tracking == 'serial' and self.product_uom_id.uom_type != 'reference':
			todo_uom = self.env['uom.uom'].search([('category_id', '=', self.product_uom_id.category_id.id), ('uom_type', '=', 'reference')]).id
		return {
			'name': operation.name,
			'step_sequence': operation.step_sequence,
			'unit_price': operation.unit_price,
			'production_id': self.id,
			'workcenter_id': operation.workcenter_id.id,
			'product_uom_id': todo_uom,
			'operation_id': operation.id,
			'state': len(workorders) == 0 and 'ready' or 'pending',
			'qty_producing': quantity,
			'consumption': self.bom_id.consumption,
		}

	def _do_query_get_max_sequence(self, date_from, date_to):
		params = [date_to, date_from]

		query = '''
			SELECT MAX(mwe.sequence)	AS max_sequence
			FROM mrp_workorder_employee mwe
			WHERE mwe.date_record <= %s
			AND mwe.date_record >= %s
		'''
		self._cr.execute(query, params)
		query_res = self._cr.dictfetchone()
		return query_res

	def _do_query_group_by_employee(self, date_from, date_to):
		params = [date_to, date_from]

		query = '''
			SELECT mwe.employee_id, mwe.sequence, mwe.production_id, mwe.step_price,
				SUM(mwe.quantity)	AS quantity
			FROM mrp_workorder_employee mwe
			INNER JOIN mrp_production mrp ON (mrp.id = mwe.production_id)
			WHERE mwe.date_record <= %s
			AND mwe.date_record >= %s
			GROUP BY mwe.employee_id, mwe.sequence, mwe.production_id, mwe.step_price
		'''
		self._cr.execute(query, params)
		query_res = self._cr.dictfetchall()
		result = {}
		for data in query_res:
			if data['production_id'] not in result:
				mrp_info = self.env['mrp.production'].search_read([('id','=',data['production_id'])], ['name','product_qty','product_id','order_code','production_line_id'], limit = 1)[0]
				# product_name = self.env['product.product'].search_read([('id','=',mrp_info['product_id'][0])],['name'],limit=1)[0]['name']
				result[data['production_id']] = {'product_name': mrp_info['product_id'][1], 'pr_quantity': mrp_info['product_qty'], 'mrp_name': mrp_info['name'],'order_code': mrp_info['order_code'], 'production_line': mrp_info['production_line_id'][1],'data': {}}
			if data['employee_id'] not in result[data['production_id']]['data']:
				result[data['production_id']]['data'][data['employee_id']] = {data['sequence']: {'qty': data['quantity'], 'price': data['step_price']}}
			else:
				result[data['production_id']]['data'][data['employee_id']][data['sequence']] = {'qty': data['quantity'], 'price': data['step_price']}
		return result

	@api.model
	def export_mrp_quantity_report(self, date_from, date_to, response):
		output = io.BytesIO()
		workbook = xlsxwriter.Workbook(output, {'in_memory': True})
		sheet = workbook.add_worksheet('MRP Quantity')
		# date_from_t = datetime.combine(fields.Date.from_string(date_from), time.min)
		# date_to_t = datetime.combine(fields.Date.from_string(date_to), time.max)
		date_from_t = fields.Date.from_string(date_from)
		date_to_t = fields.Date.from_string(date_to)
		result = self._do_query_group_by_employee(date_from_t, date_to_t)
		max_sequence = self._do_query_get_max_sequence(date_from_t, date_to_t)['max_sequence'] or 0.0
		max_sequence = int(max_sequence)
		sheet.set_column(2,2,20)
		sheet.set_column(4,4,12)
		sheet.set_column(5,5,10)
		sheet.set_column(3,3,12)
		sheet.set_column(6,5 + max_sequence,4)
		sheet.set_column(6 + max_sequence,6 + max_sequence,15)
		sheet.freeze_panes(3,6)
		border_format = workbook.add_format({'border': 1})
		title_style = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 0,'valign': 'vcenter','font_size': 16,'bold': True,'text_wrap':True})
		date_title_style = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 0,'valign': 'vcenter','font_size': 13,'bold': True,'text_wrap':True})
		header_style = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 1,'valign': 'vcenter','font_size': 10,'bold': True,'text_wrap':True})
		line_style = workbook.add_format({'font_name': 'Times New Roman','align': 'left','border': 1,'valign': 'vcenter','font_size': 10})
		center_line_style = workbook.add_format({'font_name': 'Times New Roman','align': 'center','border': 1,'valign': 'vcenter','font_size': 10})
		number_style = workbook.add_format({'font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 10})
		number_style.set_num_format('#,##0')
		quantity_style = workbook.add_format({'font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 7})
		total_style = workbook.add_format({'font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 7, 'bold': True})
		total_red_style = workbook.add_format({'font_color': 'red','font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 7, 'bold': True})
		total_gr_style = workbook.add_format({'font_color': 'green','font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 7, 'bold': True})
		line_bg_style = workbook.add_format({'bg_color': '#eaf0f6','font_name': 'Times New Roman','align': 'left','border': 1,'valign': 'vcenter','font_size': 10})
		center_bg_line_style = workbook.add_format({'bg_color': '#eaf0f6','font_name': 'Times New Roman','align': 'center','border': 1,'valign': 'vcenter','font_size': 10})
		number_bg_style = workbook.add_format({'bg_color': '#eaf0f6','font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 10})
		number_bg_style.set_num_format('#,##0')
		quantity_bg_style = workbook.add_format({'bg_color': '#eaf0f6','font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 7})
		total_bg_style = workbook.add_format({'bg_color': '#eaf0f6','font_name': 'Times New Roman','align': 'right','border': 1,'valign': 'vcenter','font_size': 7, 'bold': True})
		production_style = workbook.add_format({'font_name': 'Times New Roman','align': 'left','border': 0,'valign': 'vcenter','font_size': 11, 'bold': True})
		bg_style = workbook.add_format({'bg_color': '#eaf0f6'})
		none_bg_style = workbook.add_format({'bg_color': '#FFFFFF'})
		sheet.merge_range(0, 4, 0, 18, "BÁO NĂNG SUẤT", title_style)
		sheet.merge_range(1, 4, 1, 18, date_from +  " đến " + date_to, date_title_style)
		sheet.write(2, 0, 'No', header_style)
		sheet.write(2, 1, 'Mã nhân viên', header_style)
		sheet.write(2, 2, 'Họ tên', header_style)
		sheet.write(2, 3, 'Chuyền SX', header_style)
		sheet.write(2, 4, 'Style', header_style)
		sheet.write(2, 5, 'Số lượng SP', header_style)
		for i in range(1,1 + max_sequence):
			sheet.write(2, i+5, str(i), header_style)
		sheet.write(2, 6 + max_sequence, 'Tổng lương SP', header_style)
		row = 3
		max_1 = 20
		payroll_data = {}
		total_line = 0
		for mrp_key, mrp_value in result.items():
			total_dict = {}
			total_line += len(mrp_value['data'])
			sheet.merge_range(row, 0, row, 3, mrp_value['order_code'] or mrp_value['mrp_name'], production_style)
			row += 1
			bg_key = 1
			for key, value in mrp_value['data'].items():
				total_income = 0.0
				employ_name = self.env['hr.employee'].search_read([('id','=',key)],['name'],limit=1)[0]['name']
				if len(employ_name) > max_1:
					max_1 = len(employ_name)
					sheet.set_column(2,2,max_1 + 2)
				sheet.write(row, 0, '', line_style if bg_key%2==0 else line_bg_style)
				sheet.write(row, 1, str(key), number_style if bg_key%2==0 else number_bg_style)
				sheet.write(row, 2, employ_name, line_style if bg_key%2==0 else line_bg_style)
				sheet.write(row, 3, mrp_value['production_line'], line_style if bg_key%2==0 else line_bg_style)
				sheet.write(row, 4, mrp_value['product_name'], line_style if bg_key%2==0 else line_bg_style)
				sheet.write(row, 5, str(formatLang(self.env,mrp_value['pr_quantity'],digits=0)), number_style if bg_key%2==0 else number_bg_style)
				for i in range(1,1 + max_sequence):
					if i in value:
						total_income += value[i]['qty']*value[i]['price']
						sheet.write(row, 5 + i, str(value[i]['qty']), quantity_style if bg_key%2==0 else quantity_bg_style)
						if i not in total_dict:
							total_dict[i] = 0
						total_dict[i] += value[i]['qty']
				sheet.write(row, 6 + max_sequence, str(formatLang(self.env,total_income,digits=0)), number_style if bg_key%2==0 else number_bg_style)
				# sheet.set_row(row, cell_format=(none_bg_style if bg_key%2==0 else bg_style))
				sheet.conditional_format(row,0,row ,6 + max_sequence, { 'type' : 'blanks' , 'format' : none_bg_style if bg_key%2==0 else bg_style})
				bg_key += 1
				row += 1
				if key not in payroll_data:
					payroll_data[key] = {'total': total_income, 'name': employ_name}
				else:
					payroll_data[key]['total'] += total_income
			sheet.merge_range(row, 0, row, 5, 'Tổng', header_style)
			for i in range(1,1 + max_sequence):
				if i in total_dict:
					sheet.write(row, i+5, str(total_dict[i]), total_style)
					if total_dict[i] != mrp_value['pr_quantity']:
						sheet.write(row + 1, i+5, str(formatLang(self.env,abs(total_dict[i] - mrp_value['pr_quantity']),digits=0)), total_dict[i] > mrp_value['pr_quantity'] and total_red_style or total_gr_style)
			row += 2
		sheet.conditional_format(2,0,total_line + 2 + len(result)*3,6 + max_sequence, { 'type' : 'blanks' , 'format' : border_format})
		sheet2 = workbook.add_worksheet('Payroll')
		sheet2.merge_range(0, 4, 0, 12, "TỔNG HỢP LƯƠNG", title_style)
		sheet2.merge_range(1, 4, 1, 12, date_from +  " đến " + date_to, date_title_style)
		sheet2.set_column(2,2,22)
		sheet2.set_column(3,3,12)
		sheet2.set_column(4,4,17)
		p_row = 4
		sheet2.write(3, 0, 'STT', header_style)
		sheet2.write(3, 1, 'Mã nhân viên', header_style)
		sheet2.write(3, 2, 'Họ tên', header_style)
		sheet2.write(3, 3, 'Chuyền SX', header_style)
		sheet2.write(3, 4, 'Tổng Lương SP', header_style)
		for key,value in payroll_data.items():
			sheet2.write(p_row, 0, str(p_row - 3), center_line_style)
			sheet2.write(p_row, 1, str(key), center_line_style)
			sheet2.write(p_row, 2, str(value['name']), line_style)
			sheet2.write(p_row, 3, '', center_line_style)
			sheet2.write(p_row, 4, str(formatLang(self.env,value['total'],digits=0)), number_style)
			p_row += 1
		workbook.close()
		output.seek(0)
		response.stream.write(output.read())
		output.close()

class MRPPRODUCTIONDAILYRECORD(models.Model):
	_name = "mrp.production.daily.record"

	production_id = fields.Many2one('mrp.production', string="Production")
	date_record = fields.Date(string="Date Record", default=fields.Date.today(), required=True)
	record_quantity = fields.Float(string="Quantity")


		