# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID


class MRPROUTINGWORKCENTER(models.Model):
	_inherit = "mrp.routing.workcenter"

	work_step_id = fields.Many2one('mrp.work.step', string="Work Step")
	step_sequence = fields.Integer(string="Step Sequence", required=True)
	unit_price = fields.Float(string="Unit Price")
	time_cycle = fields.Float(compute=False)
