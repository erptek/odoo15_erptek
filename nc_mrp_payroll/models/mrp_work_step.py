# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID


class MRPWORKSTEP(models.Model):
	_name = "mrp.work.step"

	name = fields.Char(string="Name", required=True)
	sequence = fields.Integer(string="sequence", required=True)
	line_ids = fields.One2many('mrp.work.step.line', 'work_id', string="Salary rules")

class MRPWORKSTEPLINE(models.Model):
	_name = "mrp.work.step.line"

	unit_price = fields.Float(string="Đơn giá")
	product_id = fields.Many2one('product.product', string="Style", required=True)
	work_id = fields.Many2one('mrp.work.step', string="Work Step")