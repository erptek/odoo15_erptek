# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from odoo.osv import expression


class HREMPLOYEE(models.Model):
	_inherit = "hr.employee"

	worker_code = fields.Char(string="Mã nhân viên")

	_sql_constraints = [('worker_code_uniq', 'UNIQUE (worker_code)',  'Không thể tạo 2 nhân viên có cùng mã nhân viên!')]

	def name_get(self):
		return [(hr.id, "%s - %s" % (hr.worker_code, hr.name)) for hr in self]

	@api.model
	def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
		args = args or []
		domain = []
		if name:
			domain = ['|', ('worker_code', operator, name), ('name', operator, name)]
			if operator in expression.NEGATIVE_TERM_OPERATORS:
				domain = ['&', '!'] + domain[1:]
		hr_ids = self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)
		return models.lazy_name_get(self.browse(hr_ids).with_user(name_get_uid))

class HREMPLOYEE(models.Model):
	_inherit = "hr.department"
	_rec_name = 'name'

	iot_gw_id = fields.Char(string="IOT gwID")