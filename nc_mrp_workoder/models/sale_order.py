# -*- coding: utf-8 -*-

import itertools
import logging
from collections import defaultdict

from odoo import api, fields, models, SUPERUSER_ID, _
from collections import defaultdict
from odoo.tools import float_compare, OrderedSet
from odoo.addons.stock.models.stock_rule import ProcurementException


class NAMCOSTOCKRULE(models.Model):
	_inherit = "stock.rule"

	def _prepare_mo_vals(self, product_id, product_qty, product_uom, location_id, name, origin, company_id, values, bom):
		res = super(NAMCOSTOCKRULE, self)._prepare_mo_vals(product_id, product_qty, product_uom, location_id, name, origin, company_id, values, bom)
		res['department_line_id'] = values.get('department_line_id', False) or self._context.get('department_line_id',False)
		res['order_code'] = self._context.get('order_code', False)
		res['product_template_id'] = self._context.get('product_template_id', '')
		return res


class NAMCOSALEORDER(models.Model):
	_inherit = "sale.order"


	order_code = fields.Many2one('namco.order.code', string="OC No.", required=True)
	department_line_id = fields.Many2one('hr.department', string="Chuyền", required=True)
	product_template_id = fields.Many2one('product.template', string="Style", required=True)

	def action_confirm(self):
		context = self._context.copy()
		context['department_line_id'] = self.department_line_id and self.department_line_id.id or False
		context['order_code'] = self.order_code.id
		context['product_template_id'] = self.product_template_id.id
		res = super(NAMCOSALEORDER, self.with_context(context)).action_confirm()
		return res


class NAMCOSALEORDERLINE(models.Model):
	_inherit = "sale.order.line"


	def _prepare_procurement_values(self, group_id=False):
		""" Prepare specific key for moves or other components that will be created from a stock rule
		comming from a sale order line. This method could be override in order to add other custom key that could
		be used in move/po creation.
		"""
		values = super(NAMCOSALEORDERLINE, self)._prepare_procurement_values(group_id)
		self.ensure_one()
		values.update({
			'department_line_id': self.order_id.department_line_id and self.order_id.department_line_id.id or False, 
		})
		return values


class NAMCOPRODUCTTEMPLATE(models.Model):
	_inherit = "product.template"

	def _get_routes(self):
		return self.env['stock.location.route'].search([('name', 'in', ['Replenish on Order (MTO)','Manufacture','Bổ sung cho Đơn hàng (MTO)','Sản xuất'])]).ids

	product_add_mode = fields.Selection(default='matrix')
	route_ids = fields.Many2many(default=_get_routes)
	purchase_ok = fields.Boolean(default=False)
	component_type = fields.Selection([('product', 'Thành phẩm'),
		('main_fabric', 'Vải chính'),
		('lining', 'Vải lót'),
		('semi_scraper', 'Semi mí cạp'),
		('scraper','Mí cạp')], default='product', string="Phân loại sản phẩm")


	# def write(self, vals):
	# 	res = super(NAMCOPRODUCTTEMPLATE, self).write(vals)
	# 	if self._context.get('auto_update', True) and 'attribute_line_ids' in vals:
	# 		main_fabric = self.search([('name','=','Vải chính ' + self.name),('component_type','=','main_fabric')],limit=1)
	# 		if main_fabric:
	# 			main_fabric.with_context(auto_update=False).write({'attribute_line_ids': vals.get('attribute_line_ids',[])})
	# 		lining = self.search([('name','=','Vải chính ' + self.name),('component_type','=','lining')],limit=1)
	# 		if lining:
	# 			lining.with_context(auto_update=False).write({'attribute_line_ids': vals.get('attribute_line_ids',[])})
	# 		scraper = self.search([('name','=','Vải chính ' + self.name),('component_type','=','scraper')],limit=1)
	# 		if scraper:
	# 			scraper.with_context(auto_update=False).write({'attribute_line_ids': vals.get('attribute_line_ids',[])})

	# @api.model_create_multi
	# def create(self, vals):
	# 	# OVERRIDE
	# 	if self._context.get('auto_create', True) and len(vals) == 1:
	# 		component_product = [{
	# 			'name': 'Vải chính ' + vals[0].get('name'),
	# 			'attribute_line_ids': [(0,0,a[2]) for a in vals[0].get('attribute_line_ids',[])],
	# 			'component_type': 'main_fabric',
	# 		},
	# 		{
	# 			'name': 'Vải lót ' + vals[0].get('name'),
	# 			'attribute_line_ids': [(0,0,a[2]) for a in vals[0].get('attribute_line_ids',[])],
	# 			'component_type': 'lining',
	# 		},
	# 		{
	# 			'name': 'Mí cạp ' + vals[0].get('name'),
	# 			'attribute_line_ids': [(0,0,a[2]) for a in vals[0].get('attribute_line_ids',[])],
	# 			'component_type': 'scraper',
	# 		}]
	# 		self.env['product.template'].with_context(auto_create=False).create(component_product)
	# 	res = super(NAMCOPRODUCTTEMPLATE, self).create(vals)
	# 	return res


# class NAMCOPRODUCTPRODUCT(models.Model):
# 	_inherit = "product.product"

	
	# @api.model_create_multi
	# def create(self, vals):
	# 	res = super(NAMCOPRODUCTPRODUCT, self).create(vals)
	# 	for record in res:
	# 		if record.component_type == 'product':
	# 			line_ids = []
	# 			main_fabrics = self.env['product.product'].search([('name','=','Vải chính ' + record.name),('component_type','=','main_fabric')])
				
	# 			main_fabric = main_fabrics.filtered(lambda l: [v.name for v in l.product_template_attribute_value_ids] == [v.name for v in record.product_template_attribute_value_ids])
	# 			if main_fabric:
	# 				line_ids.append((0,0,{
	# 					'product_id': main_fabric[0].id
	# 					}))
	# 			linings = self.env['product.product'].search([('name','=','Vải lót ' + record.name),('component_type','=','lining')])
	# 			lining = linings.filtered(lambda l: [v.name for v in l.product_template_attribute_value_ids] == [v.name for v in record.product_template_attribute_value_ids])
	# 			if lining:
	# 				line_ids.append((0,0,{
	# 					'product_id': lining[0].id
	# 					}))
	# 			scrapers = self.env['product.product'].search([('name','=','Mí cạp ' + record.name),('component_type','=','scraper')])
	# 			scraper = scrapers.filtered(lambda l: [v.name for v in l.product_template_attribute_value_ids] == [v.name for v in record.product_template_attribute_value_ids])
				
	# 			if scraper:
	# 				line_ids.append((0,0,{
	# 					'product_id': scraper[0].id
	# 					}))
	# 			bom_id = self.env['mrp.bom'].create({
	# 				'product_tmpl_id': record.product_tmpl_id.id,
	# 				'product_id': record.id,
	# 				'bom_line_ids': line_ids,
	# 				'ready_to_produce': 'asap',
	# 				'consumption': 'flexible',
	# 				'operation_ids': [
	# 					(0,0,{
	# 						'name': 'Nhận BTP',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Chuyền may')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Chuyền may')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Chuyền may'}).id, 
	# 						'sequence': 1,
	# 					}),
	# 					(0,0,{
	# 						'name': 'Lên xích',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Chuyền may')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Chuyền may')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Chuyền may'}).id, 
	# 						'sequence': 2,
	# 						}),
	# 					(0,0,{
	# 						'name': 'QC pass',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Chuyền may')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Chuyền may')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Chuyền may'}).id, 
	# 						'sequence': 3,
	# 						})]})
	# 		if record.component_type == 'main_fabric':
	# 			bom_id = self.env['mrp.bom'].create({
	# 				'product_tmpl_id': record.product_tmpl_id.id,
	# 				'product_id': record.id,
	# 				'operation_ids': [
	# 					(0,0,{
	# 						'name': 'Cắt vải chính',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Phòng cắt')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Phòng cắt')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Phòng cắt'}).id, 
	# 						'sequence': 1,
	# 					}),
	# 					(0,0,{
	# 						'name': 'Đồng bộ BTP - Vải chính',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Đồng bộ BTP')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Đồng bộ BTP')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Đồng bộ BTP'}).id, 
	# 						'sequence': 2,
	# 						})]
	# 				})
	# 		if record.component_type == 'lining':
	# 			bom_id = self.env['mrp.bom'].create({
	# 				'product_tmpl_id': record.product_tmpl_id.id,
	# 				'product_id': record.id,
	# 				'operation_ids': [
	# 					(0,0,{
	# 						'name': 'Cắt vải lót',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Phòng cắt')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Phòng cắt')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Phòng cắt'}).id, 
	# 						'sequence': 1,
	# 					}),
	# 					(0,0,{
	# 						'name': 'Đồng bộ BTP - Vải lót',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Đồng bộ BTP')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Đồng bộ BTP')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Đồng bộ BTP'}).id, 
	# 						'sequence': 2,
	# 						})]
	# 				})
	# 		if record.component_type == 'scraper':
	# 			bom_id = self.env['mrp.bom'].create({
	# 				'product_tmpl_id': record.product_tmpl_id.id,
	# 				'product_id': record.id,
	# 				'operation_ids': [
	# 					(0,0,{
	# 						'name': 'Cắt cạp',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Phòng cắt')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Phòng cắt')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Phòng cắt'}).id, 
	# 						'sequence': 1,
	# 					}),
	# 					(0,0,{
	# 						'name': 'Đồng bộ BTP - Cạp',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Đồng bộ BTP')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Đồng bộ BTP')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Đồng bộ BTP'}).id, 
	# 						'sequence': 2,
	# 						}),
	# 					(0,0,{
	# 						'name': 'Hoàn thiện cạp',
	# 						'workcenter_id': self.env['mrp.workcenter'].search([('name','=','Chuyên dùng')], limit=1) and self.env['mrp.workcenter'].search([('name','=','Chuyên dùng')], limit=1).id or self.env['mrp.workcenter'].create({'name': 'Chuyên dùng'}).id, 
	# 						'sequence': 3,
	# 						})]
	# 				})

