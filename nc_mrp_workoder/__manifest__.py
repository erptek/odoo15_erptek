# -*- coding: utf-8 -*-
{
    'name': "Nam&Co MRP Workorder",
    'summary': """.
    """,
    'author': "Hoan",
    'category': 'MRP',
    'version': '15.0.1.1',
    'license': 'LGPL-3',
    'depends': ['mrp', 'sale_stock', 'sale_product_matrix', 'product', 'nc_mrp_dashboard'],
    'data': [
        # 'security/ir.model.access.csv',
        # 'security/mrp_security.xml',
        'views/sale_order_views.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
